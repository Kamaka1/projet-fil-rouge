package controller;

import controller.moteur.GestionMoteur;
import controller.moteur.Moteur;
import model.PropertyName;
import model.exception.RechercheImpossibleException;
import model.recherche.Recherche;
import model.recherche.TypeComparaison;
import model.recherche.requete.*;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;

/**
 * Classe permettant de gérer les différentes recherches.
 *
 * @see thread.ThreadRecherche
 */
public class ControllerRecherche {

    /**
     * Attribut qui va stocker la liste des moteurs.
     * @see Moteur
     */
    private final Moteur[] moteurs;

    /**
     * Ajout d'un PropertyChangeSupport pour envoyer les résultats aux vues.
     */
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    /**
     * Constructeur qui récupère la liste des moteurs disponibles.
     */
    public ControllerRecherche() {
        this.moteurs = GestionMoteur.getMoteurs();
    }

    /**
     * Méthode permettant d'effectuer une recherche sur l'ensemble des moteurs activés.
     * @param parametres Liste contenant les différents paramètres désirés pour la recherche.
     * @param tr Type de la requête (son, image ou texte).
     * @param tc Type de comparaison.
     * @see ParametreRequete
     * @see TypeRequete
     * @see TypeComparaison
     */
    public void effectuerRechercheMultiMoteurs(ArrayList<ParametreRequete> parametres, TypeRequete tr, TypeComparaison tc) {
        Requete req = new Requete(tr, tc);
        for (ParametreRequete p : parametres) {
            req.ajoutParametre(p);
        }
        Recherche rechercheFinale = new Recherche(req);
        Recherche rechercheTemp;
        for (Moteur m : moteurs) {
            if (m.isUtilise()) {
                try {
                    rechercheTemp = m.comparaison(tr, req);
                } catch (RechercheImpossibleException e) {
                    rechercheFinale = null;
                    break;
                }
                rechercheFinale = m.assemblageRecherches(rechercheFinale, rechercheTemp);
            }
        }
        // Envoi des résultats aux vues
        PropertyName propertyName = null;
        switch (tr) {
            case SON:
                propertyName = PropertyName.RESULTAT_AUDIO;
                break;
            case TEXTE:
                propertyName = PropertyName.RESULTAT_TEXTE;
                break;
            case IMAGE_NB:
            case IMAGE_RGB:
                propertyName = PropertyName.RESULTAT_IMAGE;
                break;
        }
        propertyChangeSupport.firePropertyChange(propertyName.toString(), null, rechercheFinale);
    }

    /**
     * Ajout du listener sur le support.
     * @param propertyName Nom de la propriété qui varie en fonction du type de recherche.
     * @param propertyChangeListener Listener qui va écouter la propriété.
     */
    public void setListener(String propertyName, PropertyChangeListener propertyChangeListener) {
        propertyChangeSupport.addPropertyChangeListener(propertyName, propertyChangeListener);
    }
}
