package controller;

import model.PropertyName;
import model.recherche.Recherche;
import model.recherche.requete.TypeRequete;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * Controlleur permetant d'afficher les détails d'une recherche lors d'un clic sur une parmi l'historique.
 *
 * @author Rémi
 *
 * @see vuegraphique.PanHistorique
 * @see vuegraphique.PanResultatAudio
 * @see vuegraphique.PanResultatImage
 * @see vuegraphique.PanResultatTexte
 */
public class ControllerAfficheDetail {


    /**
     * Support utile pour envoyer les détails de la recherche demandé aux vues.
     */
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    /**
     * Méthode à appeller pour afficher les détails d'une recherche.
     * @param recherche La recherche dont on veut afficher les détails.
     * @see Recherche
     */
    public void afficheDetails(Recherche recherche) {

        TypeRequete tr = recherche.getRequete().getType();

        PropertyName propertyName = null;
        switch (tr) {
            case SON:
                propertyName = PropertyName.RESULTAT_AUDIO;
                break;
            case TEXTE:
                propertyName = PropertyName.RESULTAT_TEXTE;
                break;
            case IMAGE_NB:
            case IMAGE_RGB:
                propertyName = PropertyName.RESULTAT_IMAGE;
                break;
        }
        propertyChangeSupport.firePropertyChange(propertyName.toString(), null, recherche);
    }

    /**
     * Ajout du listener sur le support.
     * @param propertyName Nom de la propriété qui varie en fonction du type de recherche.
     * @param propertyChangeListener Listener qui va écouter la propriété.
     */
    public void setListener(String propertyName, PropertyChangeListener propertyChangeListener) {
        propertyChangeSupport.addPropertyChangeListener(propertyName, propertyChangeListener);
    }
}
