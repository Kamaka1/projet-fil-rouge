package controller;

import controller.moteur.GestionMoteur;
import controller.moteur.Moteur;
import model.recherche.requete.TypeRequete;

/**
 * Classe permettant de gérer la connexion administrateur.
 *
 * @see Moteur
 */
public class ControllerAdmin {

    /**
     * Constructeur qui récupère la liste des moteurs disponibles
     */
    public ControllerAdmin(){
        Moteur[] moteurs = GestionMoteur.getMoteurs();
    }

    /**
     * Méthode permettant d'afficher des descripteurs
     * @param type Type des descripteurs à afficher.
     * @param moteur Choix du moteur pour lequel on va afficher les descripteurs.
     * @see TypeRequete
     * @see Moteur
     * @return String qui représente les dexcripteurs demandés
     */
    public String afficherDescripteur(TypeRequete type, Moteur moteur){
        return moteur.visualiserDescripteur(type);
    }

    /**
     *
     * @param motDePasse Mot de passe de l'application
     * @return true si l'utilisateur a saisi le bon mot de passe
     */
    public boolean seConnecter(String motDePasse) {
    	if (motDePasse.equals(GestionMoteur.getMotdePasse())) {
    		GestionMoteur.setConnexionAdmin(true);
    		return true;
    	}
    	return false;
    }

    /**
     * Permet de se déconnecter du mode administrateur
     */
    public void seDeconnecter() {
    	GestionMoteur.setConnexionAdmin(false);
    }

    /**
     * Getter de la connexion administrateur
     * @return true si l'utilisateur est connecté en administrateur
     */
    public boolean verificationConnexion() {
        return GestionMoteur.getConnexionAdmin();
    }
}
