package controller.moteur;

import model.Indexation;
import model.moteur_natif.MoteurKamakaNatif;
import model.parametre.Parametre;
import model.parametre.ValBornee;
import model.parametre.ValChoix;
import model.parametre.ValeurParametre;
import model.recherche.Recherche;
import model.recherche.Resultat;
import model.recherche.TypeIndexation;
import model.recherche.TypeValeur;
import model.recherche.requete.*;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Une implémentation de la classe Moteur
 *
 * @author Rémi et Matéo
 * @see Moteur
 */
public class MoteurKamaka extends Moteur {

    private static final Moteur instance = new MoteurKamaka();

    public static Moteur getInstance() {
        return instance;
    }

    protected MoteurKamaka() {
        this.nom = "Kamaka";
        this.utilise = true;
        this.moteurNatif = new MoteurKamakaNatif();

        this.cheminDossierTextes = "./src/c/moteurKamaka/DATA/textes/";
        this.cheminDossierImages = "./src/c/moteurKamaka/DATA/images/";
        this.cheminDossierAudios = "./src/c/moteurKamaka/DATA/audios/";
        this.dateModifDossierTextes = new File(this.cheminDossierTextes).lastModified();
        this.dateModifDossierImages = new File(this.cheminDossierImages).lastModified();
        this.dateModifDossierAudios = new File(this.cheminDossierAudios).lastModified();

        this.cheminDescripteurTexteBase = "./src/c/moteurKamaka/descripteur/descripteur_texte/base_descripteur_texte";
        this.cheminDescripteurTexteListe = "./src/c/moteurKamaka/descripteur/descripteur_texte/liste_base_texte";
        this.cheminDescripteurImageBase = "./src/c/moteurKamaka/descripteur/descripteur_image/base_descripteur_image";
        this.cheminDescripteurImageListe = "./src/c/moteurKamaka/descripteur/descripteur_image/liste_base_image";
        this.cheminDescripteurAudioBase = "./src/c/moteurKamaka/descripteur/descripteur_audio/base_descripteur_audio";
        this.cheminDescripteurAudioListe = "./src/c/moteurKamaka/descripteur/descripteur_audio/liste_base_audio";

        this.extensionTexte = "xml";
        this.extensionImage = "txt";
        this.extensionAudio = "bin";
        creerParametres();
    }

    /**
     * Permet de créer les paramètres du moteur
     */
    private void creerParametres() {
        ValBornee v1 = new ValBornee(2, 15);
        Parametre p1 = creerParametre("seuil_mot_config", "le nombre d'occurrences que doit avoir un mot", v1, TypeRequete.TEXTE);

        ValBornee v2 = new ValBornee(1, 30);
        Parametre p2 = creerParametre("nbr_fichiers_config", "le nombre de fichier affichés", v2, null);

        ValBornee v3 = new ValBornee(30, 50);
        Parametre p3 = creerParametre("nbr_intervalles_config", "le nombre d'intervalles", v3, TypeRequete.SON);

        ValBornee v4 = new ValBornee(2048, 8192);
        Parametre p4 = creerParametre("nbr_points_config", "le nombre de points", v4, TypeRequete.SON);

        ValChoix<Integer> v5 = new ValChoix<>();
        v5.addChoix(2);
        v5.addChoix(3);
        v5.setChoix(moteurNatif.recupereConfig("nbr_bits_quantification"));
        Parametre p5 = creerParametre("nbr_bits_quantification", "le nombre de bits de quantification", v5, TypeRequete.IMAGE_RGB);

        this.parametres = new Parametre[]{p1, p2, p3, p4, p5};
    }

    @Override
    public void modifConfig(Parametre parametre, int valeur, boolean force) {
        ValeurParametre valeurParametre = parametre.getValeur();
        boolean skipReindex = false;

        switch (valeurParametre.getType()) {
            case BORNEE:
                ValBornee valBornee = (ValBornee) valeurParametre;
                if (valeur >= valBornee.getMin() && valeur <= valBornee.getMax()) {
                    if (valeur != valBornee.getValeur()) {
                        valBornee.setValeur(valeur);
                        this.moteurNatif.modifConfig(parametre.getNom(), valBornee.getValeur());
                    } else {
                        skipReindex = true;
                    }
                } else {
                    throw new IllegalArgumentException("Valeur en dehors des bornes");
                }
                break;
            case CHOIX:
                @SuppressWarnings("unchecked") ValChoix<Integer> valChoix = (ValChoix<Integer>) valeurParametre;
                valChoix.setChoix(valeur);
                Object choix = valChoix.getChoix();
                if (choix != null) {
                    this.moteurNatif.modifConfig(parametre.getNom(), (int) choix);
                } else {
                    this.moteurNatif.modifConfig(parametre.getNom(), valChoix.getChoixIndice());
                }
                break;
            default:
                throw new IllegalStateException("Type inattendu : " + valeurParametre.getType());
        }

        if (parametre.getTypeRequete() != null && !skipReindex) {
            switch (parametre.getTypeRequete()) {
                case TEXTE:
                    if (force) {
                        reindexationDirecte(new Indexation(this, TypeIndexation.TEXTE));
                    } else {
                        reindexationTexte();
                    }
                    break;
                case IMAGE_RGB:
                case IMAGE_NB:
                    if (force) {
                        reindexationDirecte(new Indexation(this, TypeIndexation.IMAGE));
                    } else {
                        reindexationImage();
                    }
                    break;
                case SON:
                    if (force) {
                        reindexationDirecte(new Indexation(this, TypeIndexation.AUDIO));
                    } else {
                        reindexationAudio();
                    }
                    break;
                default:
                    break;
            }
        }

    }

    @Override
    public int getValeur(Parametre parametre) {
        ValeurParametre valeurParametre = parametre.getValeur();
        int valeur;

        switch (valeurParametre.getType()) {
            case BORNEE:
                valeur = ((ValBornee) valeurParametre).getValeur();
                break;
            case CHOIX:
                ValChoix<?> valChoix = (ValChoix<?>) valeurParametre;
                Object choix = valChoix.getChoix();
                if (choix instanceof Integer) {
                    valeur = (int) choix;
                } else {
                    valeur = valChoix.getChoixIndice();
                }
                break;
            default:
                throw new IllegalStateException("Type inattendu : " + valeurParametre.getType());
        }
        return valeur;
    }

    @Override
    public Recherche comparaisonCritere(Requete requete) {

        ArrayList<ParametreRequete> listeParam = new ArrayList<>();
        for (ParametreRequete paramReq : requete) {
            listeParam.add(paramReq);
        }
        if (listeParam.size() != 1) {
            throw new IllegalArgumentException("Erreur nombre de paramètre");
        }

        Recherche recherche = new Recherche(requete);
        recherche.ajoutMoteur(this);
        ParametreRequete param = listeParam.get(0);
        String paramString;
        int typeComp, typeIndex;

        switch (requete.getType()) {
            case TEXTE:
                typeComp = 0;
                typeIndex = 0;

                if (param instanceof ParametreRequeteMot) {
                    paramString = ((ParametreRequeteMot) param).getNom();
                } else {
                    throw new IllegalArgumentException("Erreur type paramètre comparaisonCritière");
                }

                Parametre[] params = getParametres();
                Parametre paramSeuilMotConfig = null;
                for (Parametre p : params) {
                    if (p.getNom().equals("seuil_mot_config")) {
                        paramSeuilMotConfig = p;
                    }
                }
                if (paramSeuilMotConfig == null) {
                    throw new IllegalArgumentException("Erreur algo");
                }
                try {
                    modifConfig(paramSeuilMotConfig, ((ParametreRequeteMot) param).getOccurrence(), true);
                } catch (IllegalArgumentException e) {
                    ValeurParametre val = paramSeuilMotConfig.getValeur();
                    ValBornee valBornee = (ValBornee) val;
                    if (((ParametreRequeteMot) param).getOccurrence() > valBornee.getMin()) {
                        modifConfig(paramSeuilMotConfig, valBornee.getMax(), true);
                    } else {
                        modifConfig(paramSeuilMotConfig, valBornee.getMin(), true);
                    }
                }
                break;
            case IMAGE_NB:
                typeComp = 2;
                typeIndex = 1;

                if (param instanceof ParametreRequeteCouleur) {
                    Color color = ((ParametreRequeteCouleur) param).getColor();
                    int couleurNB = color.getRed();
                    if (couleurNB < 255 / 4) {
                        paramString = "0";
                    } else if (couleurNB < (255 / 4) * 2) {
                        paramString = "1";
                    } else if (couleurNB < (255 / 4) * 3) {
                        paramString = "2";
                    } else {
                        paramString = "3";
                    }
                } else {
                    throw new IllegalArgumentException("Erreur type paramètre comparaisonCritère");
                }
                break;
            case IMAGE_RGB:
                typeComp = 1;
                typeIndex = 1;
                if (param instanceof ParametreRequeteCouleur) {
                    Color color = ((ParametreRequeteCouleur) param).getColor();
                    paramString = color.getRed() + "," + color.getGreen() + "," + color.getBlue();
                } else {
                    throw new IllegalArgumentException("Erreur type paramètre comparaisonCritère");
                }
                break;
            default:
                throw new IllegalStateException("Type inattendu : " + requete.getType());
        }

        String[] res = moteurNatif.comparaisonCritereNative(typeComp, typeIndex, paramString);

        for (String s : res) {
            Pattern pattern = Pattern.compile("([0-9]+)=(.+)");
            Matcher matcher = pattern.matcher(s);
            if (matcher.find()) {
                String strValeur = matcher.group(1);
                String nomFic = matcher.group(2);

                int valeur = Integer.parseInt(strValeur);
                recherche.ajoutResultat(new Resultat(nomFic, valeur, TypeValeur.RESSEMBLANCE));
            } else {
                throw new IllegalArgumentException("Erreur resultat mal formaté");
            }
        }
        return recherche;
    }

    @Override
    public Recherche comparaisonFichierAudio(Requete requete) {
        TypeRequete typeRequete = requete.getType();

        if (typeRequete != TypeRequete.SON) {
            throw new IllegalArgumentException("Erreur type de requête comparaison fichier audio");
        }

        ArrayList<ParametreRequete> listeParam = new ArrayList<>();
        for (ParametreRequete paramReq : requete) {
            listeParam.add(paramReq);
        }
        if (listeParam.size() != 1) {
            throw new IllegalArgumentException("Erreur nombre de paramètre");
        }

        Recherche recherche = new Recherche(requete);
        recherche.ajoutMoteur(this);
        ParametreRequete param = listeParam.get(0);
        String paramString;

        if (param instanceof ParametreRequeteFichier) {
            paramString = ((ParametreRequeteFichier) param).getFichier().getName();
        } else {
            throw new IllegalArgumentException("Erreur type paramètre comparaison fichier audio");
        }

        String[] res = moteurNatif.comparaisonFichierAudioNative(paramString);

        for (String s : res) {
            Pattern pattern = Pattern.compile("([0-9]+)=(.+)");
            Matcher matcher = pattern.matcher(s);
            if (matcher.find()) {
                String strValeur = matcher.group(1);
                String nomFic = matcher.group(2);

                int valeur = Integer.parseInt(strValeur);
                recherche.ajoutResultat(new Resultat(nomFic, valeur, TypeValeur.TEMPS));
            } else {
                throw new IllegalArgumentException("Erreur resultat mal formaté");
            }
        }
        return recherche;
    }

    @Override
    public Recherche comparaisonFichierTexte(Requete requete) {
        TypeRequete typeRequete = requete.getType();

        if (typeRequete != TypeRequete.TEXTE) {
            throw new IllegalArgumentException("Erreur type de requète comparaison fichier texte");
        }

        ArrayList<ParametreRequete> listeParam = new ArrayList<>();
        for (ParametreRequete paramReq : requete) {
            listeParam.add(paramReq);
        }
        if (listeParam.size() != 1) {
            throw new IllegalArgumentException("Erreur nombre de paramètre");
        }

        Recherche recherche = new Recherche(requete);
        recherche.ajoutMoteur(this);
        ParametreRequete param = listeParam.get(0);
        String paramString;

        if (param instanceof ParametreRequeteFichier) {
            paramString = ((ParametreRequeteFichier) param).getFichier().getName();
        } else {
            throw new IllegalArgumentException("Erreur type paramètre comparaison fichier texte");
        }

        String[] res = moteurNatif.comparaisonFichierTexteNative(paramString);

        for (String s : res) {
            Pattern pattern = Pattern.compile("([0-9]+)=(.+)");
            Matcher matcher = pattern.matcher(s);
            if (matcher.find()) {
                String strValeur = matcher.group(1);
                String nomFic = matcher.group(2);

                int valeur = Integer.parseInt(strValeur);
                recherche.ajoutResultat(new Resultat(nomFic, valeur, TypeValeur.RESSEMBLANCE));
            } else {
                throw new IllegalArgumentException("Erreur resultat mal formaté");
            }
        }
        return recherche;
    }

    @Override
    public Recherche comparaisonFichierImage(Requete requete) {
        TypeRequete typeRequete = requete.getType();

        if (typeRequete != TypeRequete.IMAGE_RGB && typeRequete != TypeRequete.IMAGE_NB) {
            throw new IllegalArgumentException("Erreur type de requête comparaison fichier image");
        }

        ArrayList<ParametreRequete> listeParam = new ArrayList<>();
        for (ParametreRequete paramReq : requete) {
            listeParam.add(paramReq);
        }
        if (listeParam.size() != 1) {
            throw new IllegalArgumentException("Erreur nombre de paramètre");
        }

        Recherche recherche = new Recherche(requete);
        recherche.ajoutMoteur(this);
        ParametreRequete param = listeParam.get(0);
        String paramString;

        if (param instanceof ParametreRequeteFichier) {
            paramString = ((ParametreRequeteFichier) param).getFichier().getName();
        } else {
            throw new IllegalArgumentException("Erreur type paramètre comparaison fichier image");
        }

        String[] res = moteurNatif.comparaisonFichierImageNative(paramString);

        for (String s : res) {
            Pattern pattern = Pattern.compile("([0-9]+)=(.+)");
            Matcher matcher = pattern.matcher(s);
            if (matcher.find()) {
                String strValeur = matcher.group(1);
                String nomFic = matcher.group(2);

                int valeur = Integer.parseInt(strValeur);
                recherche.ajoutResultat(new Resultat(nomFic, valeur, TypeValeur.RESSEMBLANCE));
            } else {
                throw new IllegalArgumentException("Erreur resultat mal formaté");
            }
        }
        return recherche;
    }
}



