package controller.moteur;

import model.moteur_natif.MoteurTamataNatif;
import model.parametre.Parametre;
import model.parametre.ValBornee;
import model.recherche.requete.*;

import java.io.*;

/**
 * Une implémentation de la classe Moteur
 * @see Moteur
 * @author Rémi et Matéo
 */
public class MoteurTamata extends MoteurKamaka {

    private static final Moteur instance = new MoteurTamata();

    public static Moteur getInstance() {
        return instance;
    }

    private MoteurTamata() {
        this.nom = "Tamata";
        this.utilise = false;
        this.moteurNatif = new MoteurTamataNatif();

        this.cheminDossierTextes = "./src/c/moteurTamata/DATA/textes/";
        this.cheminDossierImages = "./src/c/moteurTamata/DATA/images/";
        this.cheminDossierAudios = "./src/c/moteurTamata/DATA/audios/";
        this.dateModifDossierTextes = new File(this.cheminDossierTextes).lastModified();
        this.dateModifDossierImages = new File(this.cheminDossierImages).lastModified();
        this.dateModifDossierAudios = new File(this.cheminDossierAudios).lastModified();

        this.cheminDescripteurTexteBase = "./src/c/moteurTamata/descripteur/descripteur_texte/base_descripteur_texte";
        this.cheminDescripteurTexteListe = "./src/c/moteurTamata/descripteur/descripteur_texte/liste_base_texte";
        this.cheminDescripteurImageBase = "./src/c/moteurTamata/descripteur/descripteur_image/base_descripteur_image";
        this.cheminDescripteurImageListe = "./src/c/moteurTamata/descripteur/descripteur_image/liste_base_image";
        this.cheminDescripteurAudioBase = "./src/c/moteurTamata/descripteur/descripteur_audio/base_descripteur_audio";
        this.cheminDescripteurAudioListe = "./src/c/moteurTamata/descripteur/descripteur_audio/liste_base_audio";

        this.extensionTexte = "xml";
        this.extensionImage = "txt";
        this.extensionAudio = "bin";
        creerParametres();
    }

    /**
     * Permet de créer les paramètres du moteur
     */
    private void creerParametres() {
        ValBornee v1 = new ValBornee(2, 15);
        Parametre p1 = creerParametre("seuil_mot_config", "le nombre d'occurrences que doit avoir un mot", v1, TypeRequete.TEXTE);

        ValBornee v2 = new ValBornee(1, 30);
        Parametre p2 = creerParametre("nbr_fichiers_config", "le nombre de fichier affichés", v2, null);

        ValBornee v3 = new ValBornee(30, 50);
        Parametre p3 = creerParametre("nbr_intervalles_config", "le nombre d'intervalles", v3, TypeRequete.SON);
        this.parametres = new Parametre[]{p1, p2, p3};
    }
}



