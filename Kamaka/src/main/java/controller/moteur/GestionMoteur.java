package controller.moteur;

/**
 * Classe qui permet de faire la gestion des Moteur.
 *
 * @see Moteur
 * @author Rémi et Matéo
 */
public class GestionMoteur {
	
	/**
     * Mot de passe pour l'administrateur du moteur
     */
    protected final static String MotdePasse="kamakadmin";
    /**
     * Variable permettant de verifier l'etat de connexion
     */
    private static boolean connexionAdmin = false;
    
    private GestionMoteur() {}

    /**
     * Contient toutes les instances des moteurs de l'application Kamaka.
     */
    private static final Moteur[] moteurs = new Moteur[]{
            MoteurKamaka.getInstance(),
            MoteurTamata.getInstance()
    };

    /**
     * Fonction servant à récupérer tous les moteurs de l'application
     * @return Tableau des Moteur
     * @see Moteur
     */
    public static Moteur[] getMoteurs() {
        return moteurs;
    }

    /**
     * Getter du mot de passe
     * @return Mot du passe du moteur
     */
    public static String getMotdePasse() {
        return MotdePasse;
    }

    /**
     * Getter de la connexion admin
     * @return true si l'utilisateur est connecté en admin
     */
    public static boolean getConnexionAdmin() {
    	return connexionAdmin;
    }

    /**
     * Setter de la connexion admin
     * @param b = True pour dire que l'utilisation s'est connecté, false pour indiquer que l'admin s'est déconnecté
     */
    public static void setConnexionAdmin(boolean b) {
    	connexionAdmin = b;
    }

}
