package controller.moteur;

import model.Indexation;
import model.exception.FileDeleteException;
import model.exception.FileIsNotAFile;
import model.exception.RechercheImpossibleException;
import model.moteur_natif.MoteurNatif;
import model.parametre.Parametre;
import model.parametre.ValBornee;
import model.parametre.ValeurParametre;
import model.recherche.Recherche;
import model.recherche.Resultat;
import model.recherche.TypeComparaison;
import model.recherche.requete.*;

import model.recherche.TypeIndexation;
import thread.GestionThread;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * Implémentation des fonctions en java pour les recherches complexes.<br>
 * Classe abstraite pour les différents moteurs utilisés.
 *
 * @author Rémi et Matéo
 * @see GestionMoteur
 */
public abstract class Moteur {

    /**
     * Moteur natif associé au moteur.
     *
     * @see MoteurNatif
     */
    protected MoteurNatif moteurNatif;
    /**
     * Paramètres du moteur.
     *
     * @see Parametre
     */
    protected Parametre[] parametres;
    /**
     * Nombre de paramètre dans le moteur.<br>
     * Utilisé pour la créaction des paramètres.
     */
    protected int nbParametres = 0;
    /**
     * Indique si le moteur est utilisé dans les recherches (true) ou non (false).
     */
    protected boolean utilise;
    /**
     * Nom du moteur. Utilisé lors de l'affichage.
     */
    protected String nom;
    /**
     * Chemin jusqu'à l'intérieur du dossier contenant les fichiers textes (DATA)
     */
    protected String cheminDossierTextes;
    /**
     * Chemin jusqu'à l'intérieur du dossier contenant les fichiers images (DATA)
     */
    protected String cheminDossierImages;
    /**
     * Chemin jusqu'à l'intérieur du dossier contenant les fichiers audios (DATA)
     */
    protected String cheminDossierAudios;

    /**
     * Date de modification du dossier contenant les fichiers textes (permet de détecter un ajout ou une suppression)
     */
    protected long dateModifDossierTextes;
    /**
     * Date de modification du dossier contenant les fichiers images (permet de détecter un ajout ou une suppression)
     */
    protected long dateModifDossierImages;
    /**
     * Date de modification du dossier contenant les fichiers audios (permet de détecter un ajout ou une suppression)
     */
    protected long dateModifDossierAudios;
    /**
     * Chemins jusqu'au descripteur de fichiers textes : base = descripteur
     */
    protected String cheminDescripteurTexteBase;
    /**
     * Chemins jusqu'au descripteur de fichiers textes : liste = lien descripteur fichier
     */
    protected String cheminDescripteurTexteListe;
    /**
     * Chemins jusqu'au descripteur de fichiers images : base = descripteur
     */
    protected String cheminDescripteurImageBase;
    /**
     * Chemins jusqu'au descripteur de fichiers images : liste = lien descripteur fichier
     */
    protected String cheminDescripteurImageListe;
    /**
     * Chemins jusqu'au descripteur de fichiers audios : base = descripteur
     */
    protected String cheminDescripteurAudioBase;
    /**
     * Chemins jusqu'au descripteur de fichiers audios: liste = lien descripteur fichier
     */
    protected String cheminDescripteurAudioListe;

    /**
     * Extension acceptée pour les fichiers textes
     */
    protected String extensionTexte;
    /**
     * Extension acceptée pour les fichiers images
     */
    protected String extensionImage;
    /**
     * Extension acceptée pour les fichiers audio
     */
    protected String extensionAudio;

    /**
     * Permet de modifier un paramètre d'un moteur
     * @param parametre : Le paramètre à modifier
     * @param valeurParametre : La nouvelle valeur que l'on souhaite lui donner
     * @param force : Réalisation de l'indexation sur le thread courant si true. Sinon file d'attente des threads
     * @see Parametre
     */
    public abstract void modifConfig(Parametre parametre, int valeurParametre, boolean force);

    /**
     * Permet d'obtenir la valeur d'un paramètre
     * @param parametre : Paramètre dont on veut la valeur
     * @return la valeur du paramètre
     * @see Parametre
     */
    public abstract int getValeur(Parametre parametre);

    /**
     * Permet de lancer l'indexation des fichiers audios
     */
    public void indexationAudio() {
        moteurNatif.indexationAudioNative();
    }

    /**
     * Permet de lancer l'indexation des fichiers textes
     */
    public void indexationTexte() {
        moteurNatif.indexationTexteNative();
    }

    /**
     * Permet de lancer l'indexation des fichiers images
     */
    public void indexationImage() {
        moteurNatif.indexationImageNative();
    }

    /**
     * Permet d'obtenir le descripteur du type demandé
     * @param typeDesc : Type du descripteur que l'on souhaite obtenir
     * @return String contenant le descripteur
     * @see TypeRequete
     */
    public String visualiserDescripteur(TypeRequete typeDesc) {
        verifierIndexation(typeDesc);
        String chemin;
        BufferedReader br;
        switch (typeDesc) {
            case TEXTE:
                chemin = this.cheminDescripteurTexteBase;
                break;
            case IMAGE_NB:
            case IMAGE_RGB:
                chemin = this.cheminDescripteurImageBase;
                break;
            case SON:
                chemin = this.cheminDescripteurAudioBase;
                break;
            default:
                throw new IllegalArgumentException();
        }
        File f = new File(chemin);
        StringBuilder content = new StringBuilder();
        String line;
        try {
            br = new BufferedReader(new FileReader(f.getAbsoluteFile()));
            while ((line = br.readLine()) != null) {
                content.append(line).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return content.toString();
    }

    /**
     * Retourne la recherche associé à une comparaison par critères faite avec ce moteur.
     *
     * @param requete Type et paramètres que l'on recherche, stocké dans un objet requete
     * @return Objet Recherche, contant toutes les informations nécessaires.
     * @see Recherche
     * @see Requete
     */
    protected abstract Recherche comparaisonCritere(Requete requete);

    /**
     * Retourne la recherche associé à une comparaison de fichier audio faite avec ce moteur.
     *
     * @param requete Type et paramètres (nom du fichier) que l'on recherche, stocké dans un objet requete
     * @return Objet Recherche, contant toutes les informations nécessaires.
     * @see Recherche
     * @see Requete
     */
    protected abstract Recherche comparaisonFichierAudio(Requete requete);

    /**
     * Retourne la recherche associé à une comparaison de fichier texte faite avec ce moteur.
     *
     * @param requete Type et paramètres (nom du fichier) que l'on recherche, stocké dans un objet requete
     * @return Objet Recherche, contant toutes les informations nécessaires.
     * @see Recherche
     * @see Requete
     */
    protected abstract Recherche comparaisonFichierTexte(Requete requete);

    /**
     * Retourne la recherche associé à une comparaison de fichier image faite avec ce moteur.
     *
     * @param requete Type et paramètres (nom du fichier) que l'on recherche, stocké dans un objet requete
     * @return Objet Recherche, contant toutes les informations nécessaires.
     * @see Recherche
     * @see Requete
     */
    protected abstract Recherche comparaisonFichierImage(Requete requete);

    /**
     * Permet de récupérer tous les paramètres du moteur.
     *
     * @return Les paramètres du moteur
     * @see Parametre
     */
    public Parametre[] getParametres() {
        return parametres;
    }

    /**
     * Vérifie que moteur est utilisé pour la recherche ou non.
     *
     * @return true si le moteur est utilisé, false s'il ne l'est pas.
     */
    public boolean isUtilise() {
        return utilise;
    }

    /**
     * Permet de définir si le moteur est utilisé pour la recherche ou non.
     *
     * @param utilise true si le moteur est utilisé, false s'il ne l'est pas.
     */
    public void setUtilise(boolean utilise) {
        this.utilise = utilise;
    }

    /**
     * Permet de récupérer le nom du moteur
     *
     * @return Nom du moteur
     */
    public String getNom() {
        return nom;
    }

    /**
     * Permet de générer un paramètre.<br>
     * Utilisé pour le remplissage du tableau des paramètres.
     *
     * @param nom             Nom du paramètre.
     * @param description     Description du paramètre.
     * @param valeurParametre Valeur du paramètre.
     * @param tr Le type de la requete.
     * @return Le paramètre généré.
     * @see Parametre
     * @see ValeurParametre
     * @see TypeRequete
     */
    protected Parametre creerParametre(String nom, String description, ValeurParametre valeurParametre, TypeRequete tr) {
        int valeur = moteurNatif.recupereConfig(nom);

        switch (valeurParametre.getType()) {
            case BORNEE:
                ((ValBornee) valeurParametre).setValeur(valeur);
                break;
            case CHOIX:

                break;
            default:
                throw new IllegalStateException("Unexpected value: " + valeurParametre.getType());
        }

        Parametre p = new Parametre(nom, description, valeurParametre, tr);
        nbParametres++;
        return p;
    }

    /**
     * Permet de vérifier si les fichiers du corpus sont bien indexés.
     * @param typeReq : Type de requete pour ne réindexer que les fichiers nécessaires
     * @see TypeRequete
     */
    public void verifierIndexation(TypeRequete typeReq) {
        long lastModifTexte, lastModifImage, lastModifAudio;

        lastModifTexte = new File(this.cheminDossierTextes).lastModified();
        lastModifImage = new File(this.cheminDossierImages).lastModified();
        lastModifAudio = new File(this.cheminDossierAudios).lastModified();

        if (this.dateModifDossierTextes != lastModifTexte && typeReq.equals(TypeRequete.TEXTE)) {
            reindexationDirecte(new Indexation(this, TypeIndexation.TEXTE));
        }
        if (this.dateModifDossierImages != lastModifImage && (typeReq.equals(TypeRequete.IMAGE_NB) || typeReq.equals(TypeRequete.IMAGE_RGB))) {
            reindexationDirecte(new Indexation(this, TypeIndexation.IMAGE));
        }
        if (this.dateModifDossierAudios != lastModifAudio && typeReq.equals(TypeRequete.SON)) {
            reindexationDirecte(new Indexation(this, TypeIndexation.AUDIO));
        }
    }

    /**
     * Permet de relancer l'indexation texte.
     */
    public void reindexationTexte() {
        GestionThread.reIndexer(new Indexation(this, TypeIndexation.TEXTE));
    }

    /**
     * Permet de relancer l'indexation image.
     */
    public void reindexationImage() {
        GestionThread.reIndexer(new Indexation(this, TypeIndexation.IMAGE));
    }

    /**
     * Permet de relancer l'indexation audio.
     */
    public void reindexationAudio() {
        GestionThread.reIndexer(new Indexation(this, TypeIndexation.AUDIO));
    }

    /**
     * Vide les fichiers de descripteurs et réindexe. Seulement du type demandé
     * @param indexation : Indexation que l'on souhaite réaliser
     * @see Indexation
     */
    public void reindexationDirecte(Indexation indexation) {
        switch (indexation.getTypeIndexation()) {
            case TEXTE:
                viderFichier(this.cheminDescripteurTexteBase);
                viderFichier(this.cheminDescripteurTexteListe);
                this.indexationTexte();
                break;
            case IMAGE:
                viderFichier(this.cheminDescripteurImageBase);
                viderFichier(this.cheminDescripteurImageListe);
                this.indexationImage();
                break;
            case AUDIO:
                viderFichier(this.cheminDescripteurAudioBase);
                viderFichier(this.cheminDescripteurAudioListe);
                this.indexationAudio();
                break;
        }
    }

    /**
     * Permet de vider le contenu d'un fichier
     * @param nomFichier : Nom du fichier que l'on souhaite vider
     */
    private void viderFichier(String nomFichier) {
        BufferedWriter bw;
        try {
            bw = new BufferedWriter(new FileWriter(new File(nomFichier).getAbsoluteFile()));
            bw.write("");
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Permet d'ajouter un fichier au corpus de documents. Attention aux extensions acceptées par le moteur
     * @param file : Fichier à ajouter
     * @throws IOException S'il y a une erreur lors de la lectude de fichier.
     * @see File
     */
    public void ajouterFichier(File file) throws IOException {
        if (!Files.exists(Paths.get(file.getAbsolutePath()))) {
            throw new FileNotFoundException();
        }
        if (!file.isFile()) {
            throw new FileIsNotAFile();
        }
        String nomFichier = file.getName();
        String[] fragments = nomFichier.split("[.]");
        String extension;
        StringBuilder nom;
        String cheminUpload;
        String pathFichier;
        int i = 1;
        if (fragments.length < 2) {
            throw new FileNotFoundException("Non de fichier incorrecte");
        }
        extension = fragments[fragments.length - 1];
        nom = new StringBuilder(fragments[0]);
        for (int j = 1; j < fragments.length - 1; j++) {
            nom.append(".").append(fragments[j]);
        }
        if (extension.equals(this.extensionTexte)) {
            cheminUpload = this.cheminDossierTextes;
        } else if (extension.equals(this.extensionImage)) {
            cheminUpload = this.cheminDossierImages;
        } else if (extension.equals(this.extensionAudio)) {
            cheminUpload = this.cheminDossierAudios;
        } else {
            throw new FileNotFoundException("Extension non acceptée");
        }
        File dest = new File(cheminUpload + nom + "." + extension);
        while (Files.exists(Paths.get(dest.getAbsolutePath()))) {
            dest = new File(cheminUpload + nom + "-" + i + "." + extension);
            i++;
        }
        pathFichier = dest.getAbsolutePath();
        Files.createFile(Paths.get(pathFichier));
        Files.copy(Paths.get(file.getAbsolutePath()), Paths.get(pathFichier), StandardCopyOption.REPLACE_EXISTING);
    }

    /**
     * Supprime un fichier du corpus de document
     * @param file : Fichier à supprimer
     * @throws FileDeleteException Si le fichier n'a pas pu être supprimé
     * @throws FileNotFoundException Si le fichier n'existe pas.
     * @see File
     */
    public void supprimerFichier(File file) throws FileDeleteException, FileNotFoundException {
        String nomFichier = file.getName();
        String[] fragments = nomFichier.split("[.]");
        String extension;
        StringBuilder nom;
        String cheminUpload;
        if (fragments.length < 2) {
            throw new FileNotFoundException("Nom de fichier incorrect");
        }
        extension = fragments[fragments.length - 1];
        nom = new StringBuilder(fragments[0]);
        for (int j = 1; j < fragments.length - 1; j++) {
            nom.append(".").append(fragments[j]);
        }
        if (extension.equals(this.extensionTexte)) {
            cheminUpload = this.cheminDossierTextes;
        } else if (extension.equals(this.extensionImage)) {
            cheminUpload = this.cheminDossierImages;
        } else if (extension.equals(this.extensionAudio)) {
            cheminUpload = this.cheminDossierAudios;
        } else {
            throw new FileNotFoundException("Extension non acceptée");
        }
        File dest = new File(cheminUpload + nom + "." + extension);
        if (!Files.exists(Paths.get(dest.getAbsolutePath()))) {
            throw new FileNotFoundException();
        }
        if (!dest.isFile()) {
            throw new FileIsNotAFile();
        }
        try {
            Files.delete(Paths.get(dest.getAbsolutePath()));
        } catch (IOException e) {
            throw new FileDeleteException();
        }
    }

    /**
     * Recherche à réaliser.
     * @param tr : Type de la requête que l'on souhaite faire
     * @param requete : Requete contenant toutes les informations nécessaire à la recherche
     * @return la recherche avec ses paramètres et ses résultats
     * @throws RechercheImpossibleException Si la recherche est impossible
     * @see Recherche
     * @see TypeRequete
     * @see Requete
     * @see RechercheImpossibleException
     */
    public Recherche comparaison(TypeRequete tr, Requete requete) throws RechercheImpossibleException {
        TypeComparaison tc = requete.getTypeComparaison();
        Recherche rechercheFinale = new Recherche(requete);
        Recherche rechercheTemp;
        for (ParametreRequete param : requete) {
            Requete r = new Requete(requete.getType(), tc);
            r.ajoutParametre(param);
            if (tc.equals(TypeComparaison.CRITERE)) {
                verifierIndexation(r.getType());
                rechercheTemp = comparaisonCritere(r);
            } else {

                ParametreRequeteFichier prf = (ParametreRequeteFichier) param;
                boolean inCorpus;
                try {
                    inCorpus = fichierDansCorpus(prf.getFichier(), tr);
                } catch (FileNotFoundException e) {
                    throw new RechercheImpossibleException("Fichier inexistant");
                }

                if (!inCorpus) {
                    try {
                        ajouterFichier(prf.getFichier());
                    } catch (FileNotFoundException e) {
                        throw new RechercheImpossibleException("Erreur ajout fichier");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                switch (tr) {
                    case TEXTE:
                        rechercheTemp = comparaisonFichierTexte(r);
                        break;
                    case IMAGE_NB:
                    case IMAGE_RGB:
                        rechercheTemp = comparaisonFichierImage(r);
                        break;
                    case SON:
                        rechercheTemp = comparaisonFichierAudio(r);
                        break;
                    default:
                        throw new IllegalArgumentException();
                }

                if (!inCorpus) {
                    try {
                        supprimerFichier(prf.getFichier());
                    } catch (Exception e) {
                        throw new RechercheImpossibleException("Erreur suppression fichier");
                    }
                }
            }
            if (!rechercheFinale.haveMoteur()) {
                if (param.isObligatoire()) {
                    rechercheFinale = rechercheTemp;
                } else {
                    throw new RechercheImpossibleException("Impossible de faire une recherche avec un seul param (non obligatoire)");
                }
            } else {
                if (param.isObligatoire()) {
                    rechercheFinale = intersectionRecherches(rechercheFinale, rechercheTemp);
                } else {
                    rechercheFinale = differenceRecherches(rechercheFinale, rechercheTemp);
                }
            }

        }
        return rechercheFinale;
    }

    /**
     * Permet de savoir si un fichier est dans le corpus
     * @param fichier Fichier dont on souhaite savoir s'il est dans le corpus
     * @param tr Type de requete permettant de savoir dans quel dossier chercher
     * @return true si le fichier est dans le corpus, false sinon
     * @throws FileNotFoundException Si le fichier n'existe pas.
     * @see TypeRequete
     * @see File
     * @see FileNotFoundException
     */
    protected boolean fichierDansCorpus(File fichier, TypeRequete tr) throws FileNotFoundException {

        if (!(Files.exists(Paths.get(fichier.getAbsolutePath())))) {
            throw new FileNotFoundException();
        }
        if (!fichier.isFile()) {
            throw new FileIsNotAFile();
        }
        String extensionAttendue;

        String nomFichier = fichier.getName();
        String[] fragments = nomFichier.split("[.]");
        String extension;
        StringBuilder nom;
        if (fragments.length < 2) {
            throw new FileNotFoundException("Nom de fichier incorrect");
        }
        extension = fragments[fragments.length - 1];
        nom = new StringBuilder(fragments[0]);
        for (int j = 1; j < fragments.length - 1; j++) {
            nom.append(".").append(fragments[j]);
        }

        String chemin;
        switch (tr) {
            case TEXTE:
                chemin = cheminDossierTextes;
                extensionAttendue = extensionTexte;
                break;
            case IMAGE_RGB:
            case IMAGE_NB:
                chemin = cheminDossierImages;
                extensionAttendue = extensionImage;
                break;
            case SON:
                chemin = cheminDossierAudios;
                extensionAttendue = extensionAudio;
                break;
            default:
                throw new FileNotFoundException();
        }
        if (!extension.equals(extensionAttendue)) {
            throw new RechercheImpossibleException("Extension non acceptée");
        }
        return Files.exists(Paths.get(chemin + nom + "." + extension));
    }

    /**
     * Permet d'assembler deux recherches lorsqu'utilisation du multi moteur
     * @param rech1 Première recherche à combiner
     * @param rech2 Deuxième recherche à combiner
     * @return Une recherche combinaison des deux passées en paramètre
     * @see Recherche
     */
    public Recherche assemblageRecherches(Recherche rech1, Recherche rech2) {
        Recherche rechercheFinale;
        if (rech1.haveMoteur()) {
            rechercheFinale = intersectionRecherches(rech1, rech2);
        } else {
            rechercheFinale = rech2;
            rechercheFinale.ajoutMoteur(rech2.getMoteurs());
        }
        return rechercheFinale;
    }

    /**
     * Réalise l'intersection de deux recherches, on ne garde que les résultats en commun et on combine les paramètres.
     * @param rech1 Première recherche à intersectionner
     * @param rech2 Deuxième recherche à intersectionner
     * @return Une recherche résultat de l'intersection des deux passées en paramètre
     * @see Recherche
     */
    protected Recherche intersectionRecherches(Recherche rech1, Recherche rech2) {
        Recherche rechercheFusion;

        Requete req = new Requete(rech1.getRequete().getType(), rech1.getRequete().getTypeComparaison());
        for (ParametreRequete p : rech1.getRequete()) {
            req.ajoutParametre(p);
        }
        for (ParametreRequete p : rech2.getRequete()) {
            if (!req.getParametres().contains(p))
                req.ajoutParametre(p);
        }
        rechercheFusion = new Recherche(req);
        rechercheFusion.ajoutMoteur(rech1.getMoteurs());
        rechercheFusion.ajoutMoteur(rech2.getMoteurs());
        for (Resultat r1 : rech1) {
            for (Resultat r2 : rech2) {
                if (r1.equals(r2)) {
                    rechercheFusion.ajoutResultat(r1);
                }
            }
        }
        return rechercheFusion;
    }

    /**
     * Réalise la différence entre deux recherches. On retire à la première les résultats de la seconde
     * @param rech1 Recherche pour laquelle on va retirer des résultats
     * @param rech2 Recherche que l'on va utiliser pour retirer des résultats
     * @return Une recherche résultat de la différence de la recherche 1 par la recherche 2
     * @see Recherche
     */
    protected Recherche differenceRecherches(Recherche rech1, Recherche rech2) {
        Recherche rechercheFusion;
        Requete req = new Requete(rech1.getRequete().getType(), rech1.getRequete().getTypeComparaison());
        for (ParametreRequete p : rech1.getRequete()) {
            req.ajoutParametre(p);
        }
        for (ParametreRequete p : rech2.getRequete()) {
            req.ajoutParametre(p);
        }
        rechercheFusion = new Recherche(req);
        rechercheFusion.ajoutMoteur(rech1.getMoteurs());
        rechercheFusion.ajoutMoteur(rech2.getMoteurs());

        boolean present;
        for (Resultat r1 : rech1) {
            present = false;
            for (Resultat r2 : rech2) {
                if (r1.equals(r2)) {
                    present = true;
                    break;
                }
            }
            if (!present) {
                rechercheFusion.ajoutResultat(r1);
            }
        }
        return rechercheFusion;
    }

    /**
     * Getter de l'extension acceptée pour les fichiers textes
     * @return l'extension acceptée
     */
    public String getExtensionTexte() {
        return extensionTexte;
    }

    /**
     * Getter de l'extension acceptée pour les fichiers images
     * @return l'extension acceptée
     */
    public String getExtensionImage() {
        return extensionImage;
    }

    /**
     * Getter de l'extension acceptée pour les fichiers audios
     * @return l'extension acceptée
     */
    public String getExtensionAudio() {
        return extensionAudio;
    }

    /**
     * Permet d'obtenir le nom du moteur
     * @return le nom du moteur
     */
    @Override
    public String toString() {
        return this.nom;
    }
}
