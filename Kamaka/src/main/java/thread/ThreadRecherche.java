package thread;

import controller.ControllerRecherche;
import model.recherche.TypeComparaison;
import model.recherche.requete.ParametreRequete;
import model.recherche.requete.TypeRequete;

import java.util.ArrayList;

/**
 * Thread à créer pour faire une recherche.<br>
 * Il faut utiliser GestionsThread pour pouvoir le lancer.
 *
 * @author Rémi
 * @see GestionThread
 */
public class ThreadRecherche extends Thread {

    /**
     * Le controlleur utilisé pour la recherche.
     * @see ControllerRecherche
     */
    private final ControllerRecherche controllerRecherche;

    /**
     * Les paramètres de la requete.
     * @see ParametreRequete
     * @see model.recherche.requete.Requete
     */
    private final ArrayList<ParametreRequete> parametreRequetes;

    /**
     * Le type de la requete.
     * @see TypeRequete
     */
    private final TypeRequete typeRequete;

    /**
     * Le type de comparaison.
     * @see TypeComparaison
     */
    private final TypeComparaison typeComparaison;


    /**
     * Création du ThreadRecherche.<br>
     *
     * @param controllerRecherche Le controlleur utilisé pour la recherche.
     * @param parametres Les paramètres de la requete.
     * @param typeRequete Le type de la requete.
     * @param typeComparaison Le type de comparaison.
     *
     * @see GestionThread
     * @see ControllerRecherche
     * @see ParametreRequete
     * @see TypeRequete
     * @see TypeComparaison
     */
    public ThreadRecherche(ControllerRecherche controllerRecherche, ArrayList<ParametreRequete> parametres, TypeRequete typeRequete, TypeComparaison typeComparaison) {
        super();
        this.parametreRequetes = parametres;
        this.typeComparaison = typeComparaison;
        this.typeRequete = typeRequete;
        this.controllerRecherche = controllerRecherche;
    }

    /**
     * Lancement de la requete.<br>
     * <strong>NE PAS UTILISER EN DEHORS DE GestionThread !</strong><br>
     * Pour l'utiliser, il faut appeller la méthode start().
     * @see GestionThread
     */
    @Override
    public void run() {
        controllerRecherche.effectuerRechercheMultiMoteurs(parametreRequetes, typeRequete, typeComparaison);
    }
}
