package thread;

import model.Indexation;
import model.Messages;
import model.PropertyName;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Permet de gérer les thread.<br>
 * <strong>Classe utilitaire</strong>, méthodes static.
 *
 * @author Rémi
 * @see ThreadRecherche
 */
public class GestionThread {

    /**
     * C'est une classe utilitaire, toutes les méthodes à appeller sont static.
     */
    private GestionThread() {
    }

    /**
     * Support utile pour envoyer des informations de message à afficher (ex : indexation en cours...).
     */
    @SuppressWarnings("InstantiationOfUtilityClass")
    private static final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(new GestionThread());

    /**
     * Liste d'attente des indexation à faire.
     */
    private static final ArrayList<Indexation> indexations = new ArrayList<>();

    /**
     * Indique si une indexation est en cours.
     */
    private static boolean indexationEnCours = false;

    /**
     * Indique si une recherche est en cours.
     */
    private static boolean rechercheEnCours = false;

    /**
     * Indique si un Thread attend pour faire une indexation (ex : car une indexation déjà en cours).
     */
    private static boolean attenteIndexation = false;

    /**
     * Permet de lancer l'indexation donnée en paramètre au bon moment.<br>
     * Envoir également un message aux vues abonnées.
     * @param indexation L'indexation à faire.
     * @see Indexation
     */
    public static synchronized void reIndexer(Indexation indexation) {
        if (!indexations.contains(indexation)) {
            indexations.add(indexation);
            if (!attenteIndexation) {
                attenteIndexation = true;
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (!indexationEnCours) {
                            indexationEnCours = true;
                            propertyChangeSupport.firePropertyChange(PropertyName.MESSAGE.toString(), null, Messages.INDEXATION.getMessage());
                            Indexation i = indexations.remove(0);
                            i.getMoteur().reindexationDirecte(i);
                            indexationEnCours = false;
                        }
                        synchronized (indexations) {
                            if (indexations.isEmpty()) {
                                attenteIndexation = false;
                                propertyChangeSupport.firePropertyChange(PropertyName.MESSAGE.toString(), null, Messages.A_JOUR.getMessage());
                                timer.cancel();
                            }
                        }
                    }
                }, 0, 1000);
            }
        }
    }

    /**
     * Permet de thread de recherche au bon moment (quand il n'y a plus d'indexation ou de recherche en cours).
     * @param threadRecherche Le thread de recherche à lancer.
     * @see ThreadRecherche
     */
    public static void rechercher(ThreadRecherche threadRecherche) {
        if (indexationsEnCours() || rechercheEnCours()) {
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (!indexationsEnCours() && !rechercheEnCours()) {
                        recherche(threadRecherche);
                        timer.cancel();
                    }
                }
            }, 0, 1000);
        } else {
            threadRecherche.start();
        }
    }

    /**
     * Permet de lancer le thread de recherche.
     * @param threadRecherche Le thread de recherche à lancer.
     * @see ThreadRecherche
     */
    private static synchronized void recherche(ThreadRecherche threadRecherche) {
        rechercheEnCours = true;
        threadRecherche.start();
        rechercheEnCours = false;
    }

    /**
     * Indique si une indexation est en cours.
     * @return <ul><li><strong>true</strong> si une indexation est en cours ou en attente.</li>
     * <li><strong>false</strong> sinon</li></ul>
     */
    public static boolean indexationsEnCours() {
        return !indexations.isEmpty() || indexationEnCours;
    }

    /**
     * Indique si une recherche est en cours.
     * @return <ul><li><strong>true</strong> si une recherche est en cours ou en attente.</li>
     * <li><strong>false</strong> sinon</li></ul>
     */
    public static boolean rechercheEnCours() {
        return rechercheEnCours;
    }

    /**
     * Ajout du listener sur le support.
     * @param propertyName Nom de la propriété à écouter.
     * @param propertyChangeListener Listener qui va écouter la propriété.
     */
    public static void setListener(String propertyName, PropertyChangeListener propertyChangeListener) {
        propertyChangeSupport.addPropertyChangeListener(propertyName, propertyChangeListener);
    }
}
