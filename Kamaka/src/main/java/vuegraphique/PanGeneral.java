package vuegraphique;

import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.*;

import model.Messages;
import model.NomPannels;
import model.PropertyName;
import model.recherche.Historique;
import model.recherche.Recherche;
import thread.GestionThread;

public class PanGeneral extends JPanel implements PropertyChangeListener {

	/**
	 * Auto-Generated Serial ID
	 */
	private static final long serialVersionUID = 1L;
	protected final FrameUtilisateur frame;
	protected final PanGeneral instance;
	
	// private JButton boutonTest = new JButton("bonjour");
	protected final JLabel labelTest = new JLabel(Messages.A_JOUR.getMessage());
	protected final JButton boutonRetour = new JButton("Retour");
	protected final JButton boutonHistorique = new JButton("Historique");
	protected final JButton sauvegarderHistorique = new JButton("Sauvegarder dans l'historique");
	protected Recherche recherche;
	
	// controleurs du cas + panel des cas inclus ou etendus en lien avec un acteur

	// les attributs metiers (ex : numClient)
	// Les elements graphiques :
	// Declaration et creation des polices d'ecritures
	private final Font policeParagraphe = new Font("Calibri", Font.PLAIN, 16);
	// Declaration et creation des ComboBox
	// Declaration et creation des Button
	// Declaration et creation des TextArea
	// Declaration et creation des Labels
	// Mise en page : les Box

	public PanGeneral(
	// parametres pour l�initialisation des attributs m�tiers
			FrameUtilisateur frame
	// parametres correspondants au controleur du cas + panel des cas inclus ou
	// etendus
	// en relation avec un acteur
			
	) {
		// initialisation des attributs metiers
		this.frame = frame;
		this.instance = this;

		// initilaisation du controleur du cas + panels
		// des cas inclus ou etendus en lien avec un acteur

	}

	// M�thode d�initialisation du panel
	public void initialisation() {
		GestionThread.setListener(PropertyName.MESSAGE.toString(), this);
		// mise en forme du panel (couleur, �)
		// creation des diff�rents elements graphiques (JLabel, Combobox, Button,
		// TextAera �)
		// mise en page : placements des differents elements graphiques dans des Box
		// mise en page : placements des differentes box dans une box principale
		// mise en page : ajout de la box principale dans le panel
		setLayout(new BorderLayout(0, 0));
		labelTest.setFont(this.policeParagraphe);
		this.add(labelTest, BorderLayout.SOUTH);
		
		boutonRetour.addActionListener(event -> frame.changerPannel(NomPannels.ACCUEIL.getNomPannel()));
		
		boutonHistorique.addActionListener(event -> {
			frame.changerPannel(NomPannels.HISTORIQUE.getNomPannel());
			PanHistorique.getHistorique();
		});
		sauvegarderHistorique.addActionListener(e -> {
			Historique.ajoutRecherche(recherche);
			JOptionPane.showMessageDialog(this, "Résultat ajouté !", "Historique", JOptionPane.INFORMATION_MESSAGE);
		});

	}

	@Override
	public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
		PropertyName propertyName = PropertyName.valueOf(propertyChangeEvent.getPropertyName());
		if (propertyName.equals(PropertyName.MESSAGE)) {
			labelTest.setText((String) propertyChangeEvent.getNewValue());
		}
	}
	// Methodes priv�es pour le bon deroulement du cas
}