package vuegraphique;

import controller.ControllerRecherche;
import controller.ControllerAfficheDetail;
import model.Chemins;
import model.NomPannels;
import model.PropertyName;
import model.recherche.Recherche;
import model.recherche.Resultat;
import model.recherche.requete.ParametreRequeteFichier;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;

import javax.sound.sampled.*;
import javax.swing.*;

public class PanResultatAudio extends PanGeneral implements PropertyChangeListener {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	// controleurs du cas + panel des cas inclus ou etendus en lien avec un acteur
	// les attributs metiers (ex : numClient)
	// Les elements graphiques :
	// Declaration et creation des polices d'ecritures
	private final Font policeParagraphe = new Font("Calibri", Font.PLAIN, 18);
	private final Font policeTitre = new Font("Calibri", Font.BOLD, 20);

	// Declaration et creation des ComboBox
	// Declaration et creation des Button
	private final JButton nouvelleRecherche = new JButton("Nouvelle Recherche");

	// Declaration et creation des TextArea
	// Declaration et creation des Labels
	private final JLabel requete = new JLabel("Requete :");// A initialiser plus tard quand on pasera le type de
	// requ�te en param�tre
	// Mise en page : les Box
	private final Box boxMiseEnPage = Box.createVerticalBox();
	private final Box boxMiseEnPageHaute = Box.createHorizontalBox();
	final Box boxBoutonsAudios = Box.createHorizontalBox();

	private final Box boxMiseEnPageBoutons = Box.createHorizontalBox();
	private final Box boxMiseEnPageRequete = Box.createVerticalBox();
	private final Box boxMiseEnPageResultat = Box.createVerticalBox();

	public PanResultatAudio(FrameUtilisateur frame, ControllerRecherche controllerRecherche,
			ControllerAfficheDetail controllerAfficheDetail
	// parametres pour l�initialisation des attributs m�tiers
	// parametres correspondants au controleur du cas + panel des cas inclus ou
	// etendus
	// en relation avec un acteur
	) {
		super(frame);
		controllerRecherche.setListener(PropertyName.RESULTAT_AUDIO.toString(), this);
		controllerAfficheDetail.setListener(PropertyName.RESULTAT_AUDIO.toString(), this);
		// initialisation des attributs metiers
		// initilaisation du controleur du cas + panels
		// des cas inclus ou etendus en lien avec un acteur
	}

	// M�thode d�initialisation du panel
	public void initialisation() {
		super.initialisation();
		// mise en forme du panel (couleur, �)
		setBackground(Color.WHITE);

		requete.setFont(policeParagraphe);

		boxMiseEnPageRequete.setAlignmentX(LEFT_ALIGNMENT);
		boxMiseEnPageResultat.setAlignmentX(CENTER_ALIGNMENT);

		nouvelleRecherche.addActionListener(event -> frame.changerPannel(NomPannels.RECHERCHE_AUDIO.getNomPannel()));

		// creation des diff�rents elements graphiques (JLabel, Combobox, Button,
		// TextAera �)

		// mise en page : placements des differents elements graphiques dans des Box
		boxMiseEnPageRequete.add(Box.createRigidArea(new Dimension(0, 30)));
		boxMiseEnPageRequete.add(requete);

		boxMiseEnPageResultat.add(Box.createRigidArea(new Dimension(0, 200))); // Espace bordure haute
		boxMiseEnPageResultat.add(boxBoutonsAudios);

		// recherche
		boxMiseEnPageHaute.add(Box.createRigidArea(new Dimension(30, 0)));
		boxMiseEnPageHaute.add(boxMiseEnPageRequete);
		boxMiseEnPageHaute.add(Box.createRigidArea(new Dimension(300, 0))); // Espace entre espace requete et resultat
		boxMiseEnPageHaute.add(boxMiseEnPageResultat);
		boxMiseEnPageHaute.add(Box.createRigidArea(new Dimension(500, 0))); // Espace entre espace resultat et bordure
		
		boxMiseEnPageBoutons.add(boutonRetour);
		boxMiseEnPageBoutons.add(Box.createRigidArea(new Dimension(150, 0))); // Espace entre bouton retour et																		// sauvegarde
		boxMiseEnPageBoutons.add(sauvegarderHistorique);
		boxMiseEnPageBoutons.add(Box.createRigidArea(new Dimension(150, 0))); // Espace entre bouton sauvegarder et																	// historique
		boxMiseEnPageBoutons.add(boutonHistorique);
		boxMiseEnPageBoutons.add(Box.createRigidArea(new Dimension(150, 0))); // Espace entre bouton historique et																		// nouvelle recherche
		boxMiseEnPageBoutons.add(nouvelleRecherche);

																			// droite
		// mise en page : placements des differentes box dans une box principale
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 50))); // Espace entre la bordure haute et le reste
		boxMiseEnPage.add(boxMiseEnPageHaute);
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 200))); // Espace entre r�sultats et boutons
		boxMiseEnPage.add(boxMiseEnPageBoutons);

		// mise en page : ajout de la box principale dans le panel
		this.add(boxMiseEnPage);
	}

	@Override
	public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
		String propertyName = propertyChangeEvent.getPropertyName();
		PropertyName choix = PropertyName.valueOf(propertyName);
		if (choix.equals(PropertyName.MESSAGE)) {
			labelTest.setText((String) propertyChangeEvent.getNewValue());
		}
		if (choix == PropertyName.RESULTAT_AUDIO) {
			this.recherche = (Recherche) propertyChangeEvent.getNewValue();
			if (recherche != null) {
				requete.setText("Requete : " + ((ParametreRequeteFichier) recherche.getRequete().getParametres().get(0))
						.getFichier().getName());
			}
			afficheResultat(recherche);
		}
	}

	// Methodes priv�es pour le bon deroulement du cas

	private void lireResultat(String chemin) {
		Clip clip;
		File file = new File(chemin);
		AudioInputStream sound;
		try {
			sound = AudioSystem.getAudioInputStream(file);
			clip = AudioSystem.getClip();
			clip.open(sound);
			clip.setFramePosition(0);
			clip.start();
		} catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
			e.printStackTrace();
		}
	}

	private void afficheResultat(Recherche recherche) {
		boxBoutonsAudios.removeAll();
		boolean rOk = false;
		if (recherche != null) {
			for (Resultat r : recherche) {
				rOk = true;
				JButton boutonPlay = new JButton("Play");
				String nouveauNom = r.getNomFichier().split("[.]")[0] + ".wav";
				String chemin = (Chemins.KAMAKA_AUDIO.getChemin() + nouveauNom);
				boutonPlay.addActionListener(event -> lireResultat(chemin));
				JLabel nomFichier = new JLabel(
						"Dans " + nouveauNom + ", le fichier apparait à " + r.getValeur().getValeur() + "s.");
				boxBoutonsAudios.add(boutonPlay);
				boxBoutonsAudios.add(Box.createRigidArea(new Dimension(30, 0)));
				boxBoutonsAudios.add(nomFichier);
			}
			if (!rOk) {
				JLabel nomFichier = new JLabel("Aucun résultat");
				nomFichier.setFont(policeTitre);
				boxBoutonsAudios.add(nomFichier);
			}
		}
	}
}