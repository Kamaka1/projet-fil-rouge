package vuegraphique;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

import controller.ControllerRecherche;
import controller.moteur.GestionMoteur;
import controller.moteur.Moteur;
import model.Chemins;
import model.NomPannels;
import model.PropertyName;
import model.recherche.TypeComparaison;
import model.recherche.requete.ParametreRequete;
import model.recherche.requete.ParametreRequeteFichier;
import model.recherche.requete.ParametreRequeteMot;
import model.recherche.requete.TypeRequete;
import thread.GestionThread;
import thread.ThreadRecherche;

public class PanRechercheTexte extends PanGeneral implements PropertyChangeListener {
	/**
	 * Auto-Generated Serial ID
	 */
	private static final long serialVersionUID = -7234462906411391738L;

	// controleurs du cas + panel des cas inclus ou etendus en lien avec un acteur
	private final ControllerRecherche controllerRecherche;

	// les attributs metiers (ex : numClient)
	private File nomFichier = null;
	private final DefaultListModel<ParametreRequeteMot> dataObligatoire = new DefaultListModel<>();
	private final DefaultListModel<ParametreRequeteMot> dataEliminee = new DefaultListModel<>();

	// Declaration JSlide
	private final JSlider sliderOccurence = new JSlider();

	// Les elements graphiques :
	private final String imgUrl = Chemins.IMAGEFICHIER2.getChemin(); // Chemin dans l'explorateur de fichier
	private final ImageIcon imageIconFichier = new ImageIcon(imgUrl); // Cr?ation de l'image
	// Declaration et creation des polices d'ecritures
	private final Font policeTitre = new Font("Calibri", Font.BOLD, 20);
	private final Font policeParagraphe = new Font("Calibri", Font.PLAIN, 18);

	// Declaration et creation des ComboBox

	// Declaration et creation des Button
	private final JButton boutonObligatoire = new JButton("Obligatoire");
	private final JButton boutonEliminer = new JButton("Eliminé");
	private final JButton boutonRechercheMotClef = new JButton("Recherche");
	private final JButton boutonRechercheComparaison = new JButton("Recherche");

	// Declaration et creation des TextArea
	private final JTextField zoneSaisie = new JTextField();
	private final JLabel labelOccurence = new JLabel("Occurrence : 1");

	private final JLabel titreRecherche = new JLabel("Recherche en cours...");
	private final JLabel fichierChoisit = new JLabel();

	private final JLabel texteComparaisonMotClef = new JLabel("Recherche par mot clef");
	private final JLabel texteMotsObligatoires = new JLabel("Mots obligatoires");
	private final JLabel texteMotsElimines = new JLabel("Mots éliminés");
	private final JLabel texteComparaisonFichier = new JLabel("Recherche par comparaison de fichier");

	// Mise en page : les Box
	private final Box boxMiseEnPage = Box.createVerticalBox();

	private final Box boxTitre = Box.createVerticalBox();
	private final Box boxContenu = Box.createHorizontalBox();
	private final Box boxBoutons = Box.createHorizontalBox();

	private final Box boxMiseEnPageGauche = Box.createVerticalBox();
	private final Box boxMiseEnPageSaisie = Box.createHorizontalBox();
	private final Box boxMiseEnPageTableaux = Box.createHorizontalBox();
	private final Box boxPartieObligatoire = Box.createVerticalBox();
	private final Box boxPartieEliminee = Box.createVerticalBox();

	private final Box boxMiseEnPageDroite = Box.createVerticalBox();

	public PanRechercheTexte(
			// parametres pour l?initialisation des attributs m?tiers
			FrameUtilisateur frame, ControllerRecherche controllerRecherche
	// parametres correspondants au controleur du cas + panel des cas inclus ou
	// etendus
	// en relation avec un acteur
	) {
		// initialisation des attributs metiers
		super(frame);
		// initilaisation du controleur du cas + panels
		this.controllerRecherche = controllerRecherche;
		this.controllerRecherche.setListener(PropertyName.RESULTAT_TEXTE.toString(), this);

		// des cas inclus ou etendus en lien avec un acteur
	}

	// M?thode d?initialisation du panel
	public void initialisation() {
		super.initialisation();


		// mise en forme du panel (couleur, ?)
		setBackground(Color.WHITE);

		Image imageFichier = imageIconFichier.getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH);
		ImageIcon scaledFichier = new ImageIcon(imageFichier);
		// Declaration et creation des Labels
		JLabel labelFichier = new JLabel(scaledFichier);
		labelFichier.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent mouseEvent) {
				JFileChooser fileChooser = new JFileChooser(Chemins.KAMAKA_TEXTE.getChemin());
				Moteur moteur = GestionMoteur.getMoteurs()[0];
				FileNameExtensionFilter fileNameExtensionFilter = new FileNameExtensionFilter("Fichiers textes",
						moteur.getExtensionTexte());
				fileChooser.setFileFilter(fileNameExtensionFilter);
				int valeurRetour = fileChooser.showOpenDialog(instance);
				if (valeurRetour == JFileChooser.APPROVE_OPTION) {
					if (!fileNameExtensionFilter.accept(fileChooser.getSelectedFile().getAbsoluteFile())) {
						JOptionPane.showMessageDialog(instance, "Tu n'a pas le droit de choisir ce fichier", "Erreur",
								JOptionPane.WARNING_MESSAGE);
					} else {
						nomFichier = fileChooser.getSelectedFile();
						fichierChoisit.setText(nomFichier.getName());
					}
				}
			}
		});
		sliderOccurence.setMaximumSize(new Dimension(100, 30));
		sliderOccurence.setMinimum(1);
		sliderOccurence.setMaximum(15);
		sliderOccurence.setValue(1);
		sliderOccurence.addChangeListener(
				event -> labelOccurence.setText("Occurrence : " + ((JSlider) event.getSource()).getValue()));

		JList<ParametreRequeteMot> listeObligatoire = new JList<>(dataObligatoire);
		listeObligatoire.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listeObligatoire.setLayoutOrientation(JList.VERTICAL);
		listeObligatoire.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent mouseEvent) {
				if (SwingUtilities.isRightMouseButton(mouseEvent) && listeObligatoire
						.locationToIndex(mouseEvent.getPoint()) == listeObligatoire.getSelectedIndex()) {
					if (!listeObligatoire.isSelectionEmpty()) {
						int input = JOptionPane.showConfirmDialog(instance, "Voulez-vous supprimer ce paramètre ?",
								"Suppression de paramètre", JOptionPane.YES_NO_OPTION);
						if (input == 0) {
							dataObligatoire.remove(listeObligatoire.getSelectedIndex());
						}
					}
				}
			}
		});
		JScrollPane scrollObligatoire = new JScrollPane(listeObligatoire);
		scrollObligatoire.setPreferredSize(new Dimension(40, 100));

		JList<ParametreRequeteMot> listeEliminee = new JList<>(dataEliminee);
		listeEliminee.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listeEliminee.setLayoutOrientation(JList.VERTICAL);
		listeEliminee.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent mouseEvent) {
				if (SwingUtilities.isRightMouseButton(mouseEvent)
						&& listeEliminee.locationToIndex(mouseEvent.getPoint()) == listeEliminee.getSelectedIndex()) {
					if (!listeEliminee.isSelectionEmpty()) {
						int input = JOptionPane.showConfirmDialog(instance,
								"Voulez-vous supprimer le param�tre ?" + listeEliminee.getSelectedValue(),
								"Suppression de param�tre", JOptionPane.YES_NO_OPTION);
						if (input == 0) {
							dataEliminee.remove(listeEliminee.getSelectedIndex());
						}
					}
				}
			}
		});
		JScrollPane scrollEliminee = new JScrollPane(listeEliminee);
		scrollEliminee.setPreferredSize(new Dimension(40, 100));

		titreRecherche.setFont(policeTitre);
		titreRecherche.setAlignmentX(CENTER_ALIGNMENT);

		texteComparaisonMotClef.setFont(policeTitre);
		texteComparaisonMotClef.setAlignmentX(CENTER_ALIGNMENT);
		boutonRechercheMotClef.setAlignmentX(CENTER_ALIGNMENT);
		boutonRechercheMotClef.addActionListener(e -> {
			ArrayList<ParametreRequete> parametresMotCle = new ArrayList<>();

			for (int i = 0; i < dataObligatoire.size(); i++) {
				parametresMotCle.add(dataObligatoire.get(i));
			}
			for (int i = 0; i < dataEliminee.size(); i++) {
				parametresMotCle.add(dataEliminee.get(i));
			}
			if (!parametresMotCle.isEmpty()) {
				dataObligatoire.clear();
				dataEliminee.clear();
				zoneSaisie.setText("");
				boxContenu.setVisible(false);
				boxTitre.setVisible(true);
				boxBoutons.setVisible(false);
				ThreadRecherche threadRecherche = new ThreadRecherche(controllerRecherche, parametresMotCle,
						TypeRequete.TEXTE, TypeComparaison.CRITERE);
				GestionThread.rechercher(threadRecherche);
			}
		});

		boutonRetour.setAlignmentX(LEFT_ALIGNMENT);

		texteMotsObligatoires.setFont(policeParagraphe);
		texteMotsElimines.setFont(policeParagraphe);

		texteComparaisonFichier.setFont(policeTitre);
		texteComparaisonFichier.setAlignmentX(CENTER_ALIGNMENT);
		labelFichier.setAlignmentX(CENTER_ALIGNMENT);
		fichierChoisit.setAlignmentX(CENTER_ALIGNMENT);
		boutonRechercheComparaison.setAlignmentX(CENTER_ALIGNMENT);
		boutonRechercheComparaison.addActionListener(e -> {
			if (nomFichier != null) {
				boxContenu.setVisible(false);
				boxTitre.setVisible(true);
				boxBoutons.setVisible(false);
				ArrayList<ParametreRequete> parametres = new ArrayList<>();
				parametres.add(new ParametreRequeteFichier(nomFichier));
				ThreadRecherche threadRecherche = new ThreadRecherche(controllerRecherche, parametres,
						TypeRequete.TEXTE, TypeComparaison.FICHIER);
				GestionThread.rechercher(threadRecherche);
			}
		});

		boutonHistorique.setAlignmentX(RIGHT_ALIGNMENT);

		zoneSaisie.setColumns(10);
		zoneSaisie.setMaximumSize(zoneSaisie.getPreferredSize());

		boutonObligatoire.addActionListener(event -> ajoutObligatoire());

		boutonEliminer.addActionListener(event -> ajoutEliminer());

		// creation des diff?rents elements graphiques (JLabel, Combobox, Button,
		// TextAera ?)

		// mise en page : placements des differents elements graphiques dans des Box

		boxTitre.add(titreRecherche);

		boxMiseEnPageSaisie.add(zoneSaisie);
		boxMiseEnPageSaisie.add(Box.createRigidArea(new Dimension(75, 0))); // Espace entre barre de saisie et bouton
		// obligatoire
		boxMiseEnPageSaisie.add(boutonObligatoire);
		boxMiseEnPageSaisie.add(Box.createRigidArea(new Dimension(75, 0))); // Espace entre bouton obligatoire et bouton
		// ?liminer
		boxMiseEnPageSaisie.add(boutonEliminer);
		boxMiseEnPageSaisie.add(Box.createRigidArea(new Dimension(75, 0))); // Espace entre bouton obligatoire et bouton
		// ?liminer
		boxMiseEnPageSaisie.add(sliderOccurence);
		boxMiseEnPageSaisie.add(Box.createRigidArea(new Dimension(20, 0))); // Espace entre bouton obligatoire et bouton
		// ?liminer
		boxMiseEnPageSaisie.add(labelOccurence);

		boxPartieObligatoire.add(texteMotsObligatoires);
		boxPartieObligatoire.add(scrollObligatoire);
		boxPartieEliminee.add(texteMotsElimines);
		boxPartieEliminee.add(scrollEliminee);

		boxMiseEnPageTableaux.add(boxPartieObligatoire);
		boxMiseEnPageTableaux.add(Box.createRigidArea(new Dimension(150, 0))); // Espace entre la partie obligatoire et
		// la partie ?limin?e
		boxMiseEnPageTableaux.add(boxPartieEliminee);

		boxMiseEnPageGauche.setAlignmentY(TOP_ALIGNMENT);
		boxMiseEnPageGauche.add(Box.createRigidArea(new Dimension(0, 50))); // Distance entre le haut et texte
		boxMiseEnPageGauche.add(texteComparaisonMotClef);
		boxMiseEnPageGauche.add(Box.createRigidArea(new Dimension(0, 100))); // Distance entre le texte et la barre
		// dessous
		boxMiseEnPageGauche.add(boxMiseEnPageSaisie);
		boxMiseEnPageGauche.add(Box.createRigidArea(new Dimension(0, 50))); // Distance entre la barre et les tableaux
		boxMiseEnPageGauche.add(boxMiseEnPageTableaux);
		boxMiseEnPageGauche.add(Box.createRigidArea(new Dimension(0, 50))); // Distance entre les tableaux et le bouton
		// recherche
		boxMiseEnPageGauche.add(boutonRechercheMotClef);

		boxMiseEnPageDroite.setAlignmentY(TOP_ALIGNMENT);
		boxMiseEnPageDroite.add(Box.createRigidArea(new Dimension(0, 50))); // Distance entre le haut et texte
		boxMiseEnPageDroite.add(texteComparaisonFichier);
		boxMiseEnPageDroite.add(Box.createRigidArea(new Dimension(0, 200))); // Distance entre le texte et l'image de
		// fichier
		boxMiseEnPageDroite.add(labelFichier);
		boxMiseEnPageDroite.add(fichierChoisit);
		boxMiseEnPageDroite.add(Box.createRigidArea(new Dimension(0, 200))); // Distance entre l'image de fichier et le
		// bouton recherche
		boxMiseEnPageDroite.add(boutonRechercheComparaison);

		// bouton retour
		boxBoutons.add(boutonRetour);
		boxBoutons.add(Box.createRigidArea(new Dimension(150, 0)));// Espace entre boutonr retour et historique
		// bouton historique
		boxBoutons.add(boutonHistorique);

		// mise en page : placements des differentes box dans une box principale
		boxContenu.add(Box.createRigidArea(new Dimension(50, 0))); // Distance entre la bordure gauche et la partie
		// gauche
		boxContenu.add(boxMiseEnPageGauche);
		boxContenu.add(Box.createRigidArea(new Dimension(100, 0))); // Distance entre la partie gauche et la partie
		// droite
		boxContenu.add(boxMiseEnPageDroite);
		boxContenu.add(Box.createRigidArea(new Dimension(100, 0))); // Distance avec la bordure droite
		boxMiseEnPage.add(boxTitre);
		boxMiseEnPage.add(boxContenu);
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 50))); // Distance avec la bordure droite
		boxMiseEnPage.add(boxBoutons);

		// mise en page : ajout de la box principale dans le panel
		this.add(boxMiseEnPage);
		boxTitre.setVisible(false);
	}

	private void ajoutObligatoire() {
		String motSaisi = zoneSaisie.getText().toLowerCase();
		ParametreRequeteMot paramRequeteMot = new ParametreRequeteMot(motSaisi, sliderOccurence.getValue(), true);
		if (zoneSaisie.getCaretPosition() != 0 && !dataObligatoire.contains(paramRequeteMot)
				&& !dataEliminee.contains(paramRequeteMot)) {
			dataObligatoire.addElement(paramRequeteMot);

		} else {
			JOptionPane.showMessageDialog(instance, "Ajout impossible", "Erreur", JOptionPane.WARNING_MESSAGE);
		}
	}

	private void ajoutEliminer() {
		String motSaisi = zoneSaisie.getText().toLowerCase();
		ParametreRequeteMot paramRequeteMot = new ParametreRequeteMot(motSaisi, sliderOccurence.getValue(), false);
		if (zoneSaisie.getCaretPosition() != 0 && !dataEliminee.contains(paramRequeteMot)
				&& !dataObligatoire.contains(paramRequeteMot)) {
			dataEliminee.addElement(paramRequeteMot);
		} else {
			JOptionPane.showMessageDialog(instance, "Ajout impossible", "Erreur", JOptionPane.WARNING_MESSAGE);
		}

	}

	@Override
	public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
		String propertyName = propertyChangeEvent.getPropertyName();
		PropertyName choix = PropertyName.valueOf(propertyName);
		if (choix.equals(PropertyName.MESSAGE)) {
			labelTest.setText((String) propertyChangeEvent.getNewValue());
		}
		if (choix.equals(PropertyName.RESULTAT_TEXTE)) {
			boxContenu.setVisible(true);
			boxTitre.setVisible(false);
			boxBoutons.setVisible(true);
			if (propertyChangeEvent.getNewValue() != null) {
				frame.changerPannel(NomPannels.RESULTAT_TEXTE.getNomPannel());
			} else {
				JOptionPane.showMessageDialog(instance, "Recherche impossible", "Erreur", JOptionPane.WARNING_MESSAGE);
			}
		}
	}
}