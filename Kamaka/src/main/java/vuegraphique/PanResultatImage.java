package vuegraphique;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import java.awt.*;

import controller.ControllerAfficheDetail;
import controller.ControllerRecherche;
import model.Chemins;
import model.NomPannels;
import model.PropertyName;
import model.recherche.Recherche;
import model.recherche.Resultat;
import model.recherche.TypeComparaison;
import model.recherche.requete.ParametreRequete;
import model.recherche.requete.TypeRequete;

public class PanResultatImage extends PanGeneral implements PropertyChangeListener {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    // les attributs metiers (ex : numClient)
    // Les elements graphiques :
    // Declaration et creation des polices d'ecritures
    private final Font policeParagraphe = new Font("Calibri", Font.PLAIN, 18);
    private final Font policeTitre = new Font("Calibri", Font.BOLD, 20);

    // Declaration et creation des ComboBox
    // Declaration et creation des Button
    private final JButton nouvelleRecherche = new JButton("Nouvelle Recherche");
    // Declaration et creation des TextArea
    // Declaration et creation des Labels
    private final JLabel resultat = new JLabel("Résultats :");
    private final JLabel requete = new JLabel("Requete :");// A initialiser plus tard quand on pasera le type de
    // requ�te en param�tre
    // Mise en page : les Box
    private final Box boxMiseEnPage = Box.createVerticalBox();
    private final JPanel panelResultatImage = new JPanel();
    private final Box boxMiseEnPageHaute = Box.createHorizontalBox();
    private final Box boxMiseEnPageRequete = Box.createVerticalBox();
    private final Box boxMiseEnPageResultat = Box.createVerticalBox();
    private final Box boxMiseEnPageBoutons = Box.createHorizontalBox();

    public PanResultatImage(FrameUtilisateur frame, ControllerRecherche controllerRecherche,
                            ControllerAfficheDetail controllerAfficheDetail
                            // parametres pour l�initialisation des attributs m�tiers
                            // parametres correspondants au controleur du cas + panel des cas inclus ou
                            // etendus
                            // en relation avec un acteur
    ) {
        super(frame);
        // controleurs du cas + panel des cas inclus ou etendus en lien avec un acteur
        controllerRecherche.setListener(PropertyName.RESULTAT_IMAGE.toString(), this);
        controllerAfficheDetail.setListener(PropertyName.RESULTAT_IMAGE.toString(), this);
        // initialisation des attributs metiers
        // initilaisation du controleur du cas + panels
        // des cas inclus ou etendus en lien avec un acteur
    }

    // M�thode d�initialisation du panel
    public void initialisation() {
        super.initialisation();

        // mise en forme du panel (couleur, �)
        setBackground(Color.WHITE);
        panelResultatImage.setBackground(Color.white);
        requete.setFont(policeParagraphe);
        resultat.setFont(policeParagraphe);
        resultat.setAlignmentX(RIGHT_ALIGNMENT);
        nouvelleRecherche.addActionListener(event -> frame.changerPannel(NomPannels.RECHERCHE_IMAGE.getNomPannel()));
        // boxMiseEnPageRequete.setAlignmentY(TOP_ALIGNMENT);
        boxMiseEnPageRequete.setAlignmentX(LEFT_ALIGNMENT);
        boxMiseEnPageRequete.setMaximumSize(new Dimension(500, 500));
        boxMiseEnPageResultat.setAlignmentX(CENTER_ALIGNMENT);
        // boxMiseEnPageRequete.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        // boxMiseEnPageResultat.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        // boxMiseEnPageResultat.setMinimumSize(new Dimension(100, 100));
        // boxMiseEnPageResultat.setMaximumSize(new Dimension(500, 500));

        // boxMiseEnPageResultat.setAlignmentX(CENTER_ALIGNMENT);
        // creation des diff�rents elements graphiques (JLabel, Combobox, Button,
        // TextAera �)
        // mise en page : placements des differents elements graphiques dans des Box
        boxMiseEnPageRequete.add(Box.createRigidArea(new Dimension(0, 30)));
        boxMiseEnPageRequete.add(requete);

        boxMiseEnPageResultat.add(resultat);
        boxMiseEnPageResultat.add(Box.createRigidArea(new Dimension(0, 30))); // Espace entre resultat et pannel
        boxMiseEnPageResultat.add(panelResultatImage);

        boxMiseEnPageHaute.add(Box.createRigidArea(new Dimension(30, 0)));
        boxMiseEnPageHaute.add(boxMiseEnPageRequete);
        boxMiseEnPageHaute.add(Box.createRigidArea(new Dimension(150, 0))); // Espace entre espace requete et resultat
        boxMiseEnPageHaute.add(boxMiseEnPageResultat);
        boxMiseEnPageHaute.add(Box.createRigidArea(new Dimension(100, 0))); // Espace entre espace resultat et bordure
        // droite

        boxMiseEnPageBoutons.add(boutonRetour);
        boxMiseEnPageBoutons.add(Box.createRigidArea(new Dimension(150, 0))); // Espace entre bouton retour et
        // sauvegarde
        boxMiseEnPageBoutons.add(sauvegarderHistorique);
        boxMiseEnPageBoutons.add(Box.createRigidArea(new Dimension(150, 0))); // Espace entre bouton sauvegarder et
        // historique
        boxMiseEnPageBoutons.add(boutonHistorique);
        boxMiseEnPageBoutons.add(Box.createRigidArea(new Dimension(150, 0))); // Espace entre bouton historique et
        // nouvelle recherche
        boxMiseEnPageBoutons.add(nouvelleRecherche);
        // mise en page : placements des differentes box dans une box principale
        boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 50))); // Espace entre bordure et mise en page haute
        boxMiseEnPage.add(boxMiseEnPageHaute);
        boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 200))); // Espace entre mise en page haute et boutons
        boxMiseEnPage.add(boxMiseEnPageBoutons);

        // mise en page : ajout de la box principale dans le panel
        this.add(boxMiseEnPage);
    }

    // Methodes priv�es pour le bon deroulement du cas

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        String propertyName = propertyChangeEvent.getPropertyName();
        PropertyName choix = PropertyName.valueOf(propertyName);
        if (choix.equals(PropertyName.MESSAGE)) {
            labelTest.setText((String) propertyChangeEvent.getNewValue());
        }
        if (choix == PropertyName.RESULTAT_IMAGE) {
            this.recherche = (Recherche) propertyChangeEvent.getNewValue();
            if (recherche != null) {
                StringBuilder sReq = new StringBuilder("<html>Requete : ");
                TypeComparaison tc = recherche.getRequete().getTypeComparaison();
                switch (tc) {
                    case CRITERE:
                        sReq.append("Recherche par couleur<br>");
                        for (ParametreRequete p : recherche.getRequete()) {
                            if (p.isObligatoire()) {
                                sReq.append("+ ").append(p).append("<br>");
                            } else {
                                sReq.append("- ").append(p).append("<br>");
                            }
                        }
                        break;
                    case FICHIER:
                        sReq.append("Recherche par fichier<br>");
                        sReq.append(recherche.getRequete().getParametres().get(0));
                        break;
                }
                sReq.append("</html>");
                requete.setText(sReq.toString());
            }
            afficheResultat(recherche);
        }
    }

    private void afficheResultat(Recherche recherche) {
        panelResultatImage.removeAll();
        if (recherche != null) {
            int nbImages = recherche.getResultats().size();
            int q = nbImages / 5;
            ImageIcon imageIconImagette;
            panelResultatImage.setLayout(new GridLayout(q, 5));
            // boxResultatImage.add(Box.createRigidArea(new Dimension(0, 100))); // Espace
            // entre bordure haute et reste
            // JLabel titree = new JLabel("Résultats :");
            // titree.setFont(policeParagraphe);
            // panelResultatImage.add(titree);
            boolean rOk = false;
            for (Resultat r : recherche) {
                rOk = true;
                String ex;
                if (recherche.getRequete().getType().equals(TypeRequete.IMAGE_NB)) {
                    ex = ".bmp";
                } else {
                    ex = ".jpg";
                }
                String nouveauNom = r.getNomFichier().split("[.]")[0] + ex;
                String chemin = (Chemins.KAMAKA_IMAGE.getChemin() + nouveauNom);
                if (recherche.getRequete().getType().equals(TypeRequete.IMAGE_NB)) {
                    File f = new File(chemin);
                    Image imgTest = null;
                    try {
                        imgTest = ImageIO.read(f);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    imageIconImagette = new ImageIcon(imgTest);
                } else {
                    imageIconImagette = new ImageIcon(chemin);
                }
                Image imageImagette = imageIconImagette.getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH);
                ImageIcon scaledImagette = new ImageIcon(imageImagette);
                JLabel labelImagette = new JLabel(scaledImagette);
                JLabel nomImage = new JLabel(nouveauNom);
                Box boxResultatImage = Box.createVerticalBox();
                boxResultatImage.add(labelImagette);
                boxResultatImage.add(nomImage);
                panelResultatImage.add(boxResultatImage);
                labelImagette.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent mouseEvent) {
                        if (SwingUtilities.isRightMouseButton(mouseEvent)) {
                            int input = JOptionPane.showConfirmDialog(instance,
                                    "Voulez-vous ouvrir " + nouveauNom + "?", "Ouverture de résultat",
                                    JOptionPane.YES_NO_OPTION);
                            if (input == 0) {
                                System.out.println("le fichier s'ouvre");
                                ouvrirImage(chemin, nouveauNom);
                            }
                        }
                    }
                });
            }
            if (!rOk) {
                JLabel nomFichier = new JLabel("Aucun résultat");
                nomFichier.setFont(policeTitre);
                Box boxResultatImage = Box.createVerticalBox();
                boxResultatImage.add(nomFichier);
                panelResultatImage.add(boxResultatImage);
            }
            this.invalidate();
            this.validate();
            this.repaint();
        }

    }

    private void ouvrirImage(String chemin, String nom) {
        JDialog dialog = new JDialog();
        dialog.setTitle(nom);
        dialog.setResizable(false);
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        BufferedImage img;

        try {
            img = ImageIO.read(new File(chemin));

            JLabel label = new JLabel();
            label.setIcon(new ImageIcon(img));
            dialog.getContentPane().add(label, BorderLayout.CENTER);
            dialog.pack();
            dialog.setLocationRelativeTo(null);
            dialog.setVisible(true);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
