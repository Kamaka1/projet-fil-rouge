package vuegraphique;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import controller.ControllerAdmin;

public class DialogueConnexion extends JDialog {

	/**
	 * Auto-Generated Serial ID
	 */
	private static final long serialVersionUID = 6852558332293447132L;
	// Declaration des controlleurs
	private final ControllerAdmin controlAdmin;
	// Declaration et creation des differents panels
	private final JPanel panConnexion = new JPanel();
	// Declaration et creation des polices d'ecritures
	private final Font policeTitre = new Font("Calibri", Font.BOLD, 20);
	// Declaration et creation des Button
	private final JButton butAnnuler = new JButton();
	private final JButton butConnexion = new JButton();
	// Declaration des TextFields et TextAreas
	private final JPasswordField champMdp = new JPasswordField();
	// Declaration et creation des Box
	private final Box boxTitre = Box.createHorizontalBox();
	private final Box boxMdpLabel = Box.createHorizontalBox();
	private final Box boxMdpSaisie = Box.createHorizontalBox();
	private final Box boxMiseEnPage = Box.createVerticalBox();
	private final Box boxBoutons = Box.createHorizontalBox();

	/**
	 * Contructeur, met en place le Dialog
	 * @param frame La Frame parent
	 * @param controlAdmin Le controlleur utilisé pour l'administration
	 */
	public DialogueConnexion(FrameUtilisateur frame, ControllerAdmin controlAdmin) {
		super(frame, ModalityType.DOCUMENT_MODAL);
		this.controlAdmin = controlAdmin;
		// mise en forme du Dialog (titre, dimension, ...)
		setTitle("Connexion Administrateur");
		setSize(500, 325);
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		// initialisation de panFichier
		initialisationConnexion();
		// ajout des pannels dans le ContentPane de la Frame
		getContentPane().add(panConnexion);
		
		setVisible(true);
	}
	
	/**
	 * M?thode d'initialisation du Panel panConnexion
	 */
	public void initialisationConnexion() {
		// mise en forme du panel (couleur, ...)
		panConnexion.setBackground(Color.WHITE);
		
		// creation des diff?rents elements graphiques (JLabel, Combobox, Button, TextAera ?)
		JLabel titre = new JLabel("Connectez vous !");
		titre.setFont(policeTitre);
		JLabel mdp = new JLabel("Mot de passe :");
		
		butAnnuler.setText("Annuler");
		butAnnuler.addActionListener(event -> dispose());
		
		butConnexion.setText("Connexion");
		butConnexion.addActionListener(event -> {
			boolean test;
			String password = new String(champMdp.getPassword());
			test = controlAdmin.seConnecter(password);
			if (test) {
				JOptionPane.showMessageDialog(this, "Mode administrateur activé !", "Connexion", JOptionPane.INFORMATION_MESSAGE);
				dispose();
			}
			else {
				JOptionPane.showMessageDialog(this, "Veuillez ressaisir votre mot de passe", "Mot de passe incorrect !", JOptionPane.WARNING_MESSAGE);
			}
		});
		
		// mise en page : placements des differents elements graphiques dans des Box
		boxTitre.add(titre);
		boxMdpLabel.add(mdp);
		boxMdpSaisie.add(champMdp);
		boxBoutons.add(butAnnuler);
		boxBoutons.add(Box.createRigidArea(new Dimension(65, 0)));
		boxBoutons.add(butConnexion);
		
		// mise en page : placements des differentes box dans une box principale
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 50)));
		boxMiseEnPage.add(boxTitre);
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 35)));
		boxMiseEnPage.add(boxMdpLabel);
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 10)));
		boxMiseEnPage.add(boxMdpSaisie);
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 45)));
		boxMiseEnPage.add(boxBoutons);
		
		// mise en page : ajout de la box principale dans le panel
		panConnexion.add(boxMiseEnPage);
	}
}
