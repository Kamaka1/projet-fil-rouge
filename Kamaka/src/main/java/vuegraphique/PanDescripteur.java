package vuegraphique;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import controller.ControllerAdmin;
import controller.moteur.GestionMoteur;
import controller.moteur.Moteur;
import model.recherche.requete.TypeRequete;

public class PanDescripteur extends PanGeneral {
	/**
	 * Auto-Generated Serial ID
	 */
	private static final long serialVersionUID = 1L;

	// controleurs du cas + panel des cas inclus ou etendus en lien avec un acteur
	private final ControllerAdmin control;
	// les attributs metiers (ex : numClient)
	private int numeroMoteur = 0;
	TypeRequete typeDescripteur;
	// Les elements graphiques :
	// Declaration et creation des polices d'ecritures
	private final Font policeTitre = new Font("Calibri", Font.BOLD, 18);
	// Declaration et creation des ComboBox
	private final JComboBox<String> comboBoxMoteur = new JComboBox<>();
	// Declaration et creation des Button
	final JButton butValider = new JButton();
	// Declaration et creation des TextArea
	JTextArea texteDescripteur;
	// Declaration et creation des Labels
	JLabel labelTitre;
	// Mise en page : les Box
	private final Box boxTitre = Box.createHorizontalBox();
	private final Box boxTexte = Box.createHorizontalBox();
	private final Box boxMiseEnPage = Box.createVerticalBox();
	private final Box boxBoutons = Box.createHorizontalBox();
	private final Box boxChoixMoteur = Box.createHorizontalBox();

	public PanDescripteur(FrameUtilisateur frame, ControllerAdmin control) {
		super(frame);
		// initilaisation du controleur du cas + panels
		this.control = control;
	}

	// Méthode dinitialisation du panel
	public void initialisation() {
		super.initialisation();
		// mise en forme du panel (couleur, )
		setBackground(Color.WHITE);
		// creation des différents elements graphiques (JLabel, Combobox, Button, TextAera )
		labelTitre = new JLabel();
		labelTitre.setFont(policeTitre);
		texteDescripteur = new JTextArea(50, 100);
		JScrollPane scrollPane = new JScrollPane(texteDescripteur, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		// mise en page : placements des differents elements graphiques dans des Box
		boxTitre.add(labelTitre);
		boxTexte.add(scrollPane);
		boxBoutons.add(boutonRetour);
		moteurs();
		boxChoixMoteur.add(comboBoxMoteur);
		
		comboBoxMoteur.addActionListener(e -> numeroMoteur = comboBoxMoteur.getSelectedIndex());
		
		butValider.setText("Valider");
		butValider.addActionListener(e -> afficherDescripteur(typeDescripteur, numeroMoteur));
		boxChoixMoteur.add(butValider);
		// mise en page : placements des differentes box dans une box principale
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 50)));
		boxMiseEnPage.add(boxTitre);
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 20)));
		boxMiseEnPage.add(boxChoixMoteur);
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 20)));
		boxMiseEnPage.add(boxTexte);
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 20)));
		boxMiseEnPage.add(boxBoutons);
		// mise en page : ajout de la box principale dans le panel
		this.add(boxMiseEnPage);
	}

	// Methode correspondante au nom du cas
	public void descripteurTexte(TypeRequete typeDescripteur) {
		this.typeDescripteur = typeDescripteur;
		afficherTitre(typeDescripteur);
		texteDescripteur.setText("");
	}

	// Methodes privées pour le bon deroulement du cas
	private void afficherDescripteur(TypeRequete typeDescripteur, int numeroMoteur) {
		String descripteur = control.afficherDescripteur(typeDescripteur, GestionMoteur.getMoteurs()[numeroMoteur]);
		texteDescripteur.setText(descripteur);
	}
	
	private void afficherTitre(TypeRequete typeDescripteur) {
		switch (typeDescripteur) {
		case TEXTE:
			labelTitre.setText("Descripteur de fichiers textes");
			break;
		case SON:
			labelTitre.setText("Descripteur de fichiers audios");
			break;
		default:
			labelTitre.setText("Descripteur de fichiers images");
		}
	}
	
	private void moteurs() {
		comboBoxMoteur.removeAllItems();
		Moteur[] moteurs = GestionMoteur.getMoteurs();
		for (Moteur moteur : moteurs) {
			comboBoxMoteur.addItem(moteur.getNom());
		}
	}
}