package vuegraphique;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

import model.Chemins;
import model.NomPannels;

public class PanAccueil extends PanGeneral {
	/**
	 * Auto-Generated Serial ID
	 */
	private static final long serialVersionUID = -8683747855758789171L;
	
	// controleurs du cas + panel des cas inclus ou etendus en lien avec un acteur
	// les attributs metiers (ex : numClient)

	// Les elements graphiques :
	private final String imgUrl = Chemins.IMAGEKAMAKA.getChemin(); // Chemin dans l'explorateur de fichier
    private final ImageIcon kamaka = new ImageIcon(imgUrl); // Cr	éation de l'image

	// Declaration et creation des polices d'ecritures
	private final Font policeTitre = new Font("Calibri", Font.BOLD, 28);

	// Declaration et creation des ComboBox

	// Declaration et creation des Button
	private final JButton boutonTexte = new JButton("Texte");
	private final JButton boutonImage = new JButton("Image");
	private final JButton boutonAudio = new JButton("Audio");
	
	// Declaration et creation des TextArea

	// Declaration et creation des Labels
	private final JLabel titre = new JLabel("Bonjour, que cherchez vous ?");
	private final JLabel imageKamaka = new JLabel(kamaka);

	// Mise en page : les Box
	private final Box boxTitre = Box.createHorizontalBox();
	private final Box boxHistorique = Box.createHorizontalBox();
	private final Box boxMiseEnPageBoutons = Box.createHorizontalBox();
	private final Box boxMiseEnPageDroite = Box.createVerticalBox();
	private final Box boxMiseEnPage = Box.createHorizontalBox();

	public PanAccueil(
	// parametres pour linitialisation des attributs métiers
			FrameUtilisateur frame
	// parametres correspondants au controleur du cas + panel des cas inclus ou
	// etendus
	// en relation avec un acteur
	) {
		// initialisation des attributs metiers
		// initilaisation du controleur du cas + panels
		super(frame);
		// des cas inclus ou etendus en lien avec un acteur
	}

	// Méthode dinitialisation du panel
	public void initialisation() {
		super.initialisation();
		
		// mise en forme du panel (couleur, )
		setBackground(Color.WHITE);
		titre.setFont(policeTitre);
		
		// creation des différents elements graphiques (JLabel, Combobox, Button,
		// TextAera )
		boutonTexte.addActionListener(event -> frame.changerPannel(NomPannels.RECHERCHE_TEXTE.getNomPannel()));
		
		boutonImage.addActionListener(event -> frame.changerPannel(NomPannels.RECHERCHE_IMAGE.getNomPannel()));

		boutonAudio.addActionListener(event -> frame.changerPannel(NomPannels.RECHERCHE_AUDIO.getNomPannel()));
		
		
		

		// mise en page : placements des differents elements graphiques dans des Box
		boxTitre.add(titre);
		boxHistorique.add(boutonHistorique);
		
		boxMiseEnPageBoutons.add(boutonTexte);
		boxMiseEnPageBoutons.add(Box.createRigidArea(new Dimension(50, 0))); // Distance entre bouton texte et bouton image
		boxMiseEnPageBoutons.add(boutonImage);
		boxMiseEnPageBoutons.add(Box.createRigidArea(new Dimension(50, 0))); // Distance entre bouton image et bouton audio
		boxMiseEnPageBoutons.add(boutonAudio);
		
		boxMiseEnPageDroite.add(boxTitre);
		boxMiseEnPageDroite.add(Box.createRigidArea(new Dimension(0, 100))); // Distance entre le titre et les boutons texte/image/audio
		boxMiseEnPageDroite.add(boxMiseEnPageBoutons);
		boxMiseEnPageDroite.add(Box.createRigidArea(new Dimension(0, 100))); // Distance entre les boutons texte/image/audio et le bouton historique
		boxMiseEnPageDroite.add(boxHistorique);
		
		// mise en page : placements des differentes box dans une box principale
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(100, 0))); // Distance entre la bordure gauche et l'image
		boxMiseEnPage.add(imageKamaka);
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(100, 0))); // Distance entre l'image et le reste à droite
		boxMiseEnPage.add(boxMiseEnPageDroite);
		
		// mise en page : ajout de la box principale dans le panel
		this.add(boxMiseEnPage);

	}

}
