package vuegraphique;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import controller.ControllerRecherche;
import controller.moteur.GestionMoteur;
import controller.moteur.Moteur;
import model.Chemins;
import model.NomPannels;
import model.PropertyName;
import model.recherche.TypeComparaison;
import model.recherche.requete.ParametreRequete;
import model.recherche.requete.ParametreRequeteFichier;
import model.recherche.requete.TypeRequete;
import thread.GestionThread;
import thread.ThreadRecherche;

public class PanRechercheAudio extends PanGeneral implements PropertyChangeListener {
    /**
     * Auto-Generated Serial ID
     */
    private static final long serialVersionUID = 1L;
    // controleurs du cas + panel des cas inclus ou etendus en lien avec un acteur
    final ControllerRecherche controllerRecherche;
    // les attributs metiers (ex : numClient)
    private File nomFichier = null;
    // Les elements graphiques :
    private final String imgUrl = Chemins.IMAGEFICHIER2.getChemin(); // Chemin dans l'explorateur de fichier
    private final ImageIcon imageIconFichier = new ImageIcon(imgUrl); // Cr�ation de l'image

    // Declaration et creation des polices d'ecritures
    private final Font policeTitre = new Font("Calibri", Font.BOLD, 20);

    // Declaration et creation des ComboBox
    // Declaration et creation des Button
    private final JButton boutonRecherche = new JButton("Recherche");

    private final JLabel texteComparaisonFichier = new JLabel("Recherche par comparaison de fichier");
    private final JLabel fichierChoisit = new JLabel();

    // Mise en page : les Box
    private final Box boxMiseEnPage = Box.createVerticalBox();
    private final Box boxTitre = Box.createVerticalBox();
    private final Box boxContenu = Box.createVerticalBox();
    private final Box boxBoutons = Box.createHorizontalBox();


    public PanRechercheAudio(
            // parametres pour l�initialisation des attributs m�tiers
            FrameUtilisateur frame,
            ControllerRecherche controllerRecherche
            // parametres correspondants au controleur du cas + panel des cas inclus ou
            // etendus
            // en relation avec un acteur
    ) {
        // initialisation des attributs metiers
        super(frame);
        // initilaisation du controleur du cas + panels
        // des cas inclus ou etendus en lien avec un acteur
        this.controllerRecherche = controllerRecherche;
    }

    // M�thode d�initialisation du panel
    public void initialisation() {
        super.initialisation();

        this.controllerRecherche.setListener(PropertyName.RESULTAT_AUDIO.toString(), this);

        // mise en forme du panel (couleur, �)

        setBackground(Color.WHITE);

        Image imageFichier = imageIconFichier.getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH);
        ImageIcon scaledFichier = new ImageIcon(imageFichier);
        // Declaration et creation des TextArea
        // Declaration et creation des Labels
        JLabel labelFichier = new JLabel(scaledFichier);
        labelFichier.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                JFileChooser fileChooser = new JFileChooser(Chemins.KAMAKA_AUDIO.getChemin());
                Moteur moteur = GestionMoteur.getMoteurs()[0];
                FileNameExtensionFilter fileNameExtensionFilter = new FileNameExtensionFilter("Fichiers audios", moteur.getExtensionAudio());
                fileChooser.setFileFilter(fileNameExtensionFilter);
                int valeurRetour = fileChooser.showOpenDialog(null);
                if (valeurRetour == JFileChooser.APPROVE_OPTION) {
                    if (!fileNameExtensionFilter.accept(fileChooser.getSelectedFile().getAbsoluteFile())) {
                        JOptionPane.showMessageDialog(instance, "Tu n'a pas le droit de choisir ce fichier", "Erreur", JOptionPane.WARNING_MESSAGE);
                    } else {
                        nomFichier = fileChooser.getSelectedFile();
                        fichierChoisit.setText(nomFichier.getName());
                    }
                }
            }
        });

        texteComparaisonFichier.setFont(policeTitre);
        texteComparaisonFichier.setAlignmentX(CENTER_ALIGNMENT);
        labelFichier.setAlignmentX(CENTER_ALIGNMENT);
        fichierChoisit.setAlignmentX(CENTER_ALIGNMENT);
        boutonRecherche.setAlignmentX(CENTER_ALIGNMENT);
        boutonRecherche.addActionListener(e -> {
            if (nomFichier != null) {
                boxContenu.setVisible(false);
                texteComparaisonFichier.setText("Recherche en cours...");
                ArrayList<ParametreRequete> parametres = new ArrayList<>();
                parametres.add(new ParametreRequeteFichier(nomFichier));
                ThreadRecherche threadRecherche = new ThreadRecherche(controllerRecherche, parametres, TypeRequete.SON, TypeComparaison.FICHIER);
                GestionThread.rechercher(threadRecherche);
            }
        });

        boxMiseEnPage.setAlignmentX(CENTER_ALIGNMENT);


        // creation des diff�rents elements graphiques (JLabel, Combobox, Button,
        // TextAera �)
        // mise en page : placements des differents elements graphiques dans des Box
        boxTitre.add(Box.createRigidArea(new Dimension(0, 50))); // Espace entre bordure haute et titre
        boxTitre.add(texteComparaisonFichier);
        boxContenu.add(Box.createRigidArea(new Dimension(0, 100))); // Espace entre titre et image
        boxContenu.add(labelFichier);
        boxContenu.add(fichierChoisit);
        boxContenu.add(Box.createRigidArea(new Dimension(0, 100))); // Espace entre image et bouton recherche
        boxContenu.add(boutonRecherche);
        boxContenu.add(Box.createRigidArea(new Dimension(0, 100))); // Espace entre bouton recherche et autres
        boxBoutons.add(boutonRetour);
        boxBoutons.add(Box.createRigidArea(new Dimension(200, 0))); // Espace entre bouton retour et historique
        boxBoutons.add(boutonHistorique);

        boxContenu.add(boxBoutons);
        boxMiseEnPage.add(boxTitre);
        boxMiseEnPage.add(boxContenu);


        // mise en page : placements des differentes box dans une box principale
        // mise en page : ajout de la box principale dans le panel
        this.add(boxMiseEnPage);
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        String propertyName = propertyChangeEvent.getPropertyName();
        PropertyName choix = PropertyName.valueOf(propertyName);
        if (choix.equals(PropertyName.MESSAGE)) {
            labelTest.setText((String) propertyChangeEvent.getNewValue());
        }
        if (choix == PropertyName.RESULTAT_AUDIO) {
            boxContenu.setVisible(true);
            texteComparaisonFichier.setText("Recherche par comparaison de fichier");
            if (propertyChangeEvent.getNewValue() != null) {
                frame.changerPannel(NomPannels.RESULTAT_AUDIO.getNomPannel());
            } else {
                JOptionPane.showMessageDialog(instance, "Recherche impossible", "Erreur", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    // Methodes priv�es pour le bon deroulement du cas
}