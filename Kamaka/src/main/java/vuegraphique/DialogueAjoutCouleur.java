package vuegraphique;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;

public class DialogueAjoutCouleur extends JDialog implements ActionListener {

	/**
	 * Auto-Generated Serial ID
	 */
	private static final long serialVersionUID = -8333486601100097083L;

	// les attributs metiers (ex : numClient)
	private Color couleur = Color.BLUE;
	private boolean valider = false;
	private String nom = "";
	private int pourcentage;
	// Declaration et creation des differents panels
	private final JPanel panCouleur = new JPanel();
	// Les elements graphiques :
	private final JSlider slider = new JSlider(0, 100, 50);
	// Declaration et creation des Button
	private final JButton butCouleur = new JButton();
	private final JButton butAnnuler = new JButton();
	private final JButton butAjouter = new JButton();
	// Déclaration des TextFields et TextAreas
	private final JTextField champNom = new JTextField();

	// Declaration et creation des Box
	private final Box boxAjoutCouleur = Box.createHorizontalBox();
	private final Box boxNom = Box.createHorizontalBox();
	private final Box boxPourcent = Box.createHorizontalBox();
	private final Box boxMiseEnPage = Box.createVerticalBox();
	private final Box boxBoutons = Box.createHorizontalBox();
	private final Box boxValActuelle = Box.createHorizontalBox();

	/**
	 * Contructeur, met en place le Dialog
	 * @param frame La Frame parent
	 */
	public DialogueAjoutCouleur(FrameUtilisateur frame) {
		super(frame, ModalityType.DOCUMENT_MODAL);
		// mise en forme du Dialog (titre, dimension, ...)
		setTitle("Ajouter une Couleur");
		setSize(500, 375);
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		// initialisation de panFichier
		initialisationAjoutCouleur();
		// ajout des pannels dans le ContentPane de la Frame
		getContentPane().add(panCouleur);

		setVisible(true);
	}

	/**
	 * Méthode d'initialisation du Panel panConnexion
	 */
	public void initialisationAjoutCouleur() {
		// mise en forme du panel (couleur, ...)
		panCouleur.setBackground(Color.WHITE);

		// creation des différents elements graphiques (JLabel, Combobox, Button,
		// TextAera )
		JLabel labelAjoutCouleur = new JLabel("Ajout d'une couleur : ");
		JLabel labelNom = new JLabel("Nom : ");
		JLabel labelPourcent = new JLabel("Pourcentage de dominance : ");
		butAjouter.setText("Ajouter");
		butAnnuler.addActionListener(event -> dispose());
		butAnnuler.setText("Annuler");
		butCouleur.setBackground(couleur);
		butCouleur.addActionListener(this);
		butCouleur.setPreferredSize(new Dimension(30, 30));
		JLabel labelValActuelle = new JLabel("Valeur : " + slider.getValue() + "%");
		slider.setPaintTicks(true);
		slider.addChangeListener(event -> labelValActuelle.setText("Valeur : " + ((JSlider) event.getSource()).getValue() + "%"));
		butAjouter.addActionListener(event -> {
			valider = true;
			pourcentage = slider.getValue();
			nom = champNom.getText();
			dispose();
		});
		// mise en page : placements des differents elements graphiques dans des Box
		boxAjoutCouleur.add(labelAjoutCouleur);
		boxAjoutCouleur.add(butCouleur);
		boxNom.add(labelNom);
		boxNom.add(champNom);
		boxPourcent.add(labelPourcent);
		boxPourcent.add(slider);
		boxValActuelle.add(labelValActuelle);
		boxBoutons.add(butAnnuler);
		boxBoutons.add(Box.createRigidArea(new Dimension(65, 0)));
		boxBoutons.add(butAjouter);

		// mise en page : placements des differentes box dans une box principale
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 30)));
		boxMiseEnPage.add(boxAjoutCouleur);
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 40)));
		boxMiseEnPage.add(boxNom);
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 40)));
		boxMiseEnPage.add(boxPourcent);
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 5)));
		boxMiseEnPage.add(boxValActuelle);
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 40)));
		boxMiseEnPage.add(boxBoutons);

		// mise en page : ajout de la box principale dans le panel
		panCouleur.add(boxMiseEnPage);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource().equals(butCouleur)) {
			couleur = JColorChooser.showDialog(this,"Choissez la couleur",butCouleur.getBackground());
			butCouleur.setBackground(couleur);
		}
	}
	
	public boolean getValider() {
		return valider;
	}
	
	public Color getCouleur() {
		return couleur;
	}
	
	public String getNom() {
		return nom;
	}
	
	public int getPourcentage() {
		return pourcentage;
	}
}
