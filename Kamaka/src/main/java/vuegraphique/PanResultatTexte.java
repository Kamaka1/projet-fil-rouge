package vuegraphique;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Dialog.ModalityType;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

import controller.ControllerAfficheDetail;
import controller.ControllerRecherche;
import model.Chemins;
import model.NomPannels;
import model.PropertyName;
import model.recherche.Recherche;
import model.recherche.Resultat;
import model.recherche.TypeComparaison;
import model.recherche.requete.ParametreRequete;
import model.recherche.requete.ParametreRequeteMot;

public class PanResultatTexte extends PanGeneral implements PropertyChangeListener {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    // les attributs metiers (ex : numClient)
    private final DefaultListModel<Resultat> dataResultats = new DefaultListModel<>();
    // Les elements graphiques :
    // Declaration et creation des polices d'ecritures
    private final Font policeParagraphe = new Font("Calibri", Font.PLAIN, 18);
    private final Font policeTitre = new Font("Calibri", Font.BOLD, 20);

    // Declaration et creation des ComboBox
    // Declaration et creation des Button
    private final JButton nouvelleRecherche = new JButton("Nouvelle Recherche");
    // Declaration et creation des TextArea
    // Declaration et creation des Labels
    private final JLabel requete = new JLabel("Requete :");//A initialiser plus tard quand on pasera le type de requete en parametre
    private final JLabel resultat = new JLabel();
    // Mise en page : les Box
    JScrollPane scrollResultats;
         
    private final Box boxMiseEnPage = Box.createVerticalBox();

    private final Box boxMiseEnPageHaute = Box.createHorizontalBox();
    private final Box boxMiseEnPageRequete = Box.createVerticalBox();
    private final Box boxMiseEnPageResultat = Box.createVerticalBox();

    private final Box boxMiseEnPageBoutons = Box.createHorizontalBox();

    public PanResultatTexte(
            FrameUtilisateur frame,
            ControllerRecherche controllerRecherche, ControllerAfficheDetail controllerAfficheDetail
            // parametres pour l'initialisation des attributs metiers
            // parametres correspondants au controleur du cas + panel des cas inclus ou
            // etendus
            // en relation avec un acteur
    ) {
        super(frame);
        // controleurs du cas + panel des cas inclus ou etendus en lien avec un acteur
        controllerRecherche.setListener(PropertyName.RESULTAT_TEXTE.toString(), this);
        controllerAfficheDetail.setListener(PropertyName.RESULTAT_TEXTE.toString(), this);
        // initialisation des attributs metiers
        // initilaisation du controleur du cas + panels
        // des cas inclus ou etendus en lien avec un acteur
    }

    // Methode d'initialisation du panel
    public void initialisation() {
        super.initialisation();
        // mise en forme du panel (couleur, etc..)
        setBackground(Color.WHITE);

        JList<Resultat> listeResultats = new JList<>(dataResultats);
        listeResultats.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listeResultats.setLayoutOrientation(JList.VERTICAL);
        listeResultats.setFont(policeParagraphe);
        listeResultats.addMouseListener(new MouseAdapter() {
        	@Override
			public void mouseClicked(MouseEvent mouseEvent) {
				if (SwingUtilities.isRightMouseButton(mouseEvent) && 
						listeResultats.locationToIndex(mouseEvent.getPoint()) == listeResultats.getSelectedIndex()) {
					if (!listeResultats.isSelectionEmpty()) {
						int input = JOptionPane.showConfirmDialog(instance, "Voulez-vous ouvrir "+dataResultats.get(listeResultats.getSelectedIndex()) +"?",
								"Ouverture de résultat", JOptionPane.YES_NO_OPTION);
						if (input == 0) {
							String nomFichier = Chemins.KAMAKA_TEXTE.getChemin().toString() + listeResultats.getSelectedValue();
							String commande = "gedit " + nomFichier;
                            try {
                                Runtime.getRuntime().exec(commande);
                            } catch (IOException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
					    }
					}
				}
			}
		});
        
        scrollResultats = new JScrollPane(listeResultats);
        scrollResultats.setPreferredSize(new Dimension(1000, 500));
        
        requete.setFont(policeParagraphe);
        resultat.setFont(policeParagraphe);
        resultat.setText("Résultats : ");
        nouvelleRecherche.addActionListener(event -> frame.changerPannel(NomPannels.RECHERCHE_TEXTE.getNomPannel()));
        boxMiseEnPageRequete.setAlignmentX(LEFT_ALIGNMENT);
        boxMiseEnPageResultat.setAlignmentX(CENTER_ALIGNMENT);
        
        
        // creation des differents elements graphiques (JLabel, Combobox, Button, TextAera...)

        // mise en page : placements des differents elements graphiques dans des Box
        boxMiseEnPageRequete.add(Box.createRigidArea(new Dimension(0, 30)));
        boxMiseEnPageRequete.add(requete);
        //boxMiseEnPageRequete.setBorder(BorderFactory.createEmptyBorder(0, 30, 0, 0));

        boxMiseEnPageResultat.add(Box.createRigidArea(new Dimension(0, 100))); // Espace entre bordure haute et reste
        boxMiseEnPageResultat.add(resultat);
        boxMiseEnPageResultat.add(Box.createRigidArea(new Dimension(0, 15)));
        boxMiseEnPageResultat.add(scrollResultats);

        boxMiseEnPageHaute.add(Box.createRigidArea(new Dimension(30, 0)));
        boxMiseEnPageHaute.add(boxMiseEnPageRequete);
        boxMiseEnPageHaute.add(Box.createRigidArea(new Dimension(200, 0))); // Espace entre espace requete et resultat
        boxMiseEnPageHaute.add(boxMiseEnPageResultat);
        boxMiseEnPageHaute.add(Box.createRigidArea(new Dimension(250, 0))); // Espace entre espace resultat et bordure droite

        boxMiseEnPageBoutons.add(boutonRetour);
        boxMiseEnPageBoutons.add(Box.createRigidArea(new Dimension(150, 0))); // Espace entre bouton retour et sauvegarde
        boxMiseEnPageBoutons.add(sauvegarderHistorique);
        boxMiseEnPageBoutons.add(Box.createRigidArea(new Dimension(150, 0))); // Espace entre bouton sauvegarder et historique
        boxMiseEnPageBoutons.add(boutonHistorique);
        boxMiseEnPageBoutons.add(Box.createRigidArea(new Dimension(150, 0))); // Espace entre bouton historique et nouvelle recherche
        boxMiseEnPageBoutons.add(nouvelleRecherche);
        // mise en page : placements des differentes box dans une box principale

        boxMiseEnPage.add(boxMiseEnPageHaute);
        boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 200))); // Espace entre mise en page haute et boutons
        boxMiseEnPage.add(boxMiseEnPageBoutons);
        // mise en page : ajout de la box principale dans le panel
        this.add(boxMiseEnPage);
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        dataResultats.removeAllElements();
        String propertyName = propertyChangeEvent.getPropertyName();
        PropertyName choix = PropertyName.valueOf(propertyName);
        if (choix.equals(PropertyName.MESSAGE)) {
            labelTest.setText((String) propertyChangeEvent.getNewValue());
        }
        if (choix.equals(PropertyName.RESULTAT_TEXTE)) {
            this.recherche = (Recherche) propertyChangeEvent.getNewValue();
            if (recherche != null) {
                StringBuilder sReq = new StringBuilder("<html>Requete : ");
                TypeComparaison tc = recherche.getRequete().getTypeComparaison();
                switch (tc) {
                    case CRITERE:
                        sReq.append("Recherche par mot cle<br>");
                        for (ParametreRequete p : recherche.getRequete()) {
                            if (p.isObligatoire()) {
                                sReq.append("+ ").append(p).append("<br>");
                            } else {
                                sReq.append("- ").append(p).append("<br>");
                            }
                        }
                        break;
                    case FICHIER:
                        sReq.append("Recherche par fichier<br>");
                        sReq.append(recherche.getRequete().getParametres().get(0));
                        break;
                }
                sReq.append("</html>");
                requete.setText(sReq.toString());
            }

            boolean rOk = false;
            //StringBuilder sRes = new StringBuilder("<html>Resultats :<br>");
            if (recherche != null) {
                //int nbRes = 1;
                for (Resultat s : recherche) {
                    rOk = true;
                    //sRes.append(nbRes).append(" - ").append(s).append("<br>");
                    //nbRes++;
                    dataResultats.addElement(s);
                }
                //sRes.append("</html>");
            }
            if (!rOk) {
                resultat.setFont(policeTitre);
                resultat.setText("Aucun résultat");
            } else {
                resultat.setFont(policeParagraphe);
                //resultat.setText(sRes.toString());
                resultat.setText("Résultats :");
            }
        }

    }
/*
    // Methodes privees pour le bon deroulement du cas
    private void ouvrirTexte(String nomFichier){
 
    	JDialog dialog = new JDialog(frame, ModalityType.DOCUMENT_MODAL);
		// mise en forme du Dialog (titre, dimension, ...)
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	
        JTextArea tarea = new JTextArea(10, 10);
            try {
            	String chemin = (Chemins.KAMAKA_IMAGE.getChemin() + nomFichier);
            	File file = new File(chemin);
              BufferedReader input = new BufferedReader(new InputStreamReader(
                  new FileInputStream(file)));
              tarea.read(input, "READING FILE :-)");
            } catch (Exception e) {
              e.printStackTrace();
            }
         JPanel pan = new JPanel();
         pan.add(tarea);
         pan.setVisible(true);
            dialog.getContentPane().add(pan);
            
          }
        
*/
      }


