package vuegraphique;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

import controller.ControllerRecherche;
import controller.moteur.GestionMoteur;
import controller.moteur.Moteur;
import model.Chemins;
import model.NomPannels;
import model.PropertyName;
import model.recherche.TypeComparaison;
import model.recherche.requete.ParametreRequete;
import model.recherche.requete.ParametreRequeteCouleur;
import model.recherche.requete.ParametreRequeteFichier;
import model.recherche.requete.TypeRequete;
import thread.GestionThread;
import thread.ThreadRecherche;

public class PanRechercheImage extends PanGeneral implements ActionListener, PropertyChangeListener {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	// controleurs du cas + panel des cas inclus ou etendus en lien avec un acteur
	final ControllerRecherche controllerRecherche;
	// les attributs metiers (ex : numClient)
	private File nomFichier = null;
	final DefaultListModel<ParametreRequeteCouleur> dataObligatoire = new DefaultListModel<>();
	final DefaultListModel<ParametreRequeteCouleur> dataEliminee = new DefaultListModel<>();

	// Les elements graphiques :
	private final String imgUrl = Chemins.IMAGEFICHIER2.getChemin(); // Chemin dans l'explorateur de fichier
	private final ImageIcon imageIconFichier = new ImageIcon(imgUrl); // Creation de l'image
	private final String imgSablierUrl = Chemins.IMAGESABLIER.getChemin();
	private final ImageIcon imageIconSablier = new ImageIcon(imgSablierUrl); // Creation de l'image

	// Declaration et creation des polices d'ecritures
	private final Font policeTitre = new Font("Calibri", Font.BOLD, 20);
	private final Font policeParagraphe = new Font("Calibri", Font.PLAIN, 18);

	// Declaration et creation des ComboBox
	// Declaration et creation des Button
	private final JButton boutonRechercheCouleur = new JButton("Recherche");
	private final JButton boutonRechercheGris = new JButton("Recherche");
	private final JButton boutonRechercheFichier = new JButton("Recherche");
	private final JRadioButton boutonBlanc = new JRadioButton("Blanc");
	private final JRadioButton boutonGrisClair = new JRadioButton("Gris clair");
	private final JRadioButton boutonGrisFonce = new JRadioButton("Gris foncé");
	private final JRadioButton boutonNoir = new JRadioButton("Noir");
	private final ButtonGroup groupeBouton = new ButtonGroup();
	private final JButton butObligatoire = new JButton("+");
	private final JButton butElimine = new JButton("+");

	private final JLabel titreRecherche = new JLabel("Recherche en cours...");
	private final JLabel fichierChoisit = new JLabel();

	private final JLabel texteRechercheCouleur = new JLabel("Recherche par couleurs");
	private final JLabel texteRechercheGris = new JLabel("Recherche par niveau de gris");
	private final JLabel texteComparaisonFichier = new JLabel("Recherche par comparaison de fichier");
	private final JLabel texteCouleursObligatoires = new JLabel("Couleurs obligatoires");
	private final JLabel texteCouleursEliminees = new JLabel("Couleurs éliminées");

	// Mise en page : les Box
	private final Box boxTrio = Box.createHorizontalBox();

	private final Box boxTitre = Box.createVerticalBox();
	private final Box boxPartieGauche = Box.createVerticalBox();
	private final Box boxPartieCentrale = Box.createVerticalBox();
	private final Box boxChoixGris = Box.createVerticalBox();
	private final Box boxPartieDroite = Box.createVerticalBox();

	private final Box boxTableaux = Box.createHorizontalBox();
	private final Box boxPartieObligatoire = Box.createVerticalBox();
	private final Box boxPartieEliminee = Box.createVerticalBox();
	private final Box boxBoutons = Box.createHorizontalBox();
	private final Box boxMiseEnPage = Box.createVerticalBox();

	public PanRechercheImage(
			// parametres pour l?initialisation des attributs m?tiers
			FrameUtilisateur frame, ControllerRecherche controllerRecherche
	// parametres correspondants au controleur du cas + panel des cas inclus ou
	// etendus
	// en relation avec un acteur
	) {
		// initialisation des attributs metiers
		super(frame);
		this.controllerRecherche = controllerRecherche;
		// initilaisation du controleur du cas + panels
		// des cas inclus ou etendus en lien avec un acteur
	}

	// M?thode d?initialisation du panel
	public void initialisation() {
		super.initialisation();

		this.controllerRecherche.setListener(PropertyName.RESULTAT_IMAGE.toString(), this);

		// mise en forme du panel (couleur, ?)
		setBackground(Color.WHITE);

		Image imageFichier = imageIconFichier.getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH);
		ImageIcon scaledFichier = new ImageIcon(imageFichier);
		Image imageSablier = imageIconSablier.getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH);
		ImageIcon scaledSablier = new ImageIcon(imageSablier);
		// Declaration et creation des TextArea
		// Declaration et creation des Labels
		JLabel labelFichier = new JLabel(scaledFichier);
		JLabel labelSablier = new JLabel(scaledSablier);

		labelFichier.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent mouseEvent) {
				JFileChooser fileChooser = new JFileChooser(Chemins.KAMAKA_IMAGE.getChemin());
				Moteur moteur = GestionMoteur.getMoteurs()[0];
				FileNameExtensionFilter fileNameExtensionFilter = new FileNameExtensionFilter("Fichiers images",
						moteur.getExtensionImage());
				fileChooser.setFileFilter(fileNameExtensionFilter);
				int valeurRetour = fileChooser.showOpenDialog(null);
				if (valeurRetour == JFileChooser.APPROVE_OPTION) {
					if (!fileNameExtensionFilter.accept(fileChooser.getSelectedFile().getAbsoluteFile())) {
						JOptionPane.showMessageDialog(instance, "Tu n'a pas le droit de choisir ce fichier", "Erreur",
								JOptionPane.WARNING_MESSAGE);
					} else {
						nomFichier = fileChooser.getSelectedFile();
						fichierChoisit.setText(nomFichier.getName());
					}
				}
			}
		});

		titreRecherche.setFont(policeTitre);
		titreRecherche.setAlignmentX(CENTER_ALIGNMENT);

		texteRechercheCouleur.setFont(policeTitre);
		texteRechercheCouleur.setAlignmentX(CENTER_ALIGNMENT);
		boutonRechercheCouleur.setAlignmentX(CENTER_ALIGNMENT);
		boutonRechercheCouleur.addActionListener(e -> {
			ArrayList<ParametreRequete> parametresMotCle = new ArrayList<>();

			for (int i = 0; i < dataObligatoire.size(); i++) {
				parametresMotCle.add(dataObligatoire.get(i));
			}
			for (int i = 0; i < dataEliminee.size(); i++) {
				parametresMotCle.add(dataEliminee.get(i));
			}

			if (!parametresMotCle.isEmpty()) {
				afficheAttente(true);
				ThreadRecherche threadRecherche = new ThreadRecherche(controllerRecherche, parametresMotCle,
						TypeRequete.IMAGE_RGB, TypeComparaison.CRITERE);
				GestionThread.rechercher(threadRecherche);
			}
		});

		boutonRetour.setAlignmentX(LEFT_ALIGNMENT);

		texteRechercheGris.setFont(policeTitre);
		texteRechercheGris.setAlignmentX(CENTER_ALIGNMENT);

		boxChoixGris.setAlignmentX(CENTER_ALIGNMENT);
		boutonRechercheGris.setAlignmentX(CENTER_ALIGNMENT);
		boutonRechercheGris.addActionListener(e -> {
			ArrayList<ParametreRequete> parametresMotCle = new ArrayList<>();
			int i = 0;
			for (Enumeration<AbstractButton> enumeration = groupeBouton.getElements(); enumeration.hasMoreElements();) {
				AbstractButton bouton = enumeration.nextElement();
				int gris = -1;
				String nom = "";
				if (bouton.isSelected()) {
					switch (i) {
					case 0:
						gris = 255;
						nom = "Blanc";
						break;
					case 1:
						gris = 127;
						nom = "Gris clair";
						break;
					case 2:
						gris = 67;
						nom = "Gris foncé";
						break;
					case 3:
						gris = 0;
						nom = "Noir";
						break;
					}
					parametresMotCle.add(new ParametreRequeteCouleur(new Color(gris, gris, gris), nom, 100, true));
				}
				i++;
			}

			if (groupeBouton.getSelection() != null) {
				afficheAttente(true);
				ThreadRecherche threadRecherche = new ThreadRecherche(controllerRecherche, parametresMotCle,
						TypeRequete.IMAGE_NB, TypeComparaison.CRITERE);
				GestionThread.rechercher(threadRecherche);
			}
		});

		texteComparaisonFichier.setFont(policeTitre);
		texteComparaisonFichier.setAlignmentX(CENTER_ALIGNMENT);
		labelFichier.setAlignmentX(CENTER_ALIGNMENT);
		fichierChoisit.setAlignmentX(CENTER_ALIGNMENT);
		boutonRechercheFichier.setAlignmentX(CENTER_ALIGNMENT);
		boutonRechercheFichier.addActionListener(e -> {
			if (nomFichier != null) {
				afficheAttente(true);
				ArrayList<ParametreRequete> parametres = new ArrayList<>();
				parametres.add(new ParametreRequeteFichier(nomFichier));
				ThreadRecherche threadRecherche = new ThreadRecherche(controllerRecherche, parametres,
						TypeRequete.IMAGE_RGB, TypeComparaison.FICHIER);
				GestionThread.rechercher(threadRecherche);
			}
		});
		boutonHistorique.setAlignmentX(RIGHT_ALIGNMENT);
		butElimine.setAlignmentX(CENTER_ALIGNMENT);
		butObligatoire.setAlignmentX(CENTER_ALIGNMENT);

		butElimine.setBackground(Color.CYAN);
		butObligatoire.setBackground(Color.CYAN);
		butObligatoire.addActionListener(this);
		butElimine.addActionListener(this);

		texteCouleursObligatoires.setFont(policeParagraphe);

		texteCouleursEliminees.setFont(policeParagraphe);

		groupeBouton.add(boutonBlanc);
		groupeBouton.add(boutonGrisClair);
		groupeBouton.add(boutonGrisFonce);
		groupeBouton.add(boutonNoir);
		groupeBouton.clearSelection();

		JList<ParametreRequeteCouleur> listeObligatoire = new JList<>(dataObligatoire);
		listeObligatoire.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listeObligatoire.setLayoutOrientation(JList.VERTICAL);
		JScrollPane scrollObligatoire = new JScrollPane(listeObligatoire);
		scrollObligatoire.setPreferredSize(new Dimension(300, 180));
		scrollObligatoire.setMaximumSize(new Dimension(400, 300));

		JList<ParametreRequeteCouleur> listeEliminee = new JList<>(dataEliminee);
		listeEliminee.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listeEliminee.setLayoutOrientation(JList.VERTICAL);
		JScrollPane scrollEliminee = new JScrollPane(listeEliminee);
		scrollEliminee.setPreferredSize(new Dimension(310, 180));
		scrollEliminee.setMaximumSize(new Dimension(410, 300));

		listeEliminee.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent mouseEvent) {
				if (SwingUtilities.isRightMouseButton(mouseEvent)
						&& listeEliminee.locationToIndex(mouseEvent.getPoint()) == listeEliminee.getSelectedIndex()) {
					if (!listeEliminee.isSelectionEmpty()) {
						int input = JOptionPane.showConfirmDialog(null, "Voulez-vous supprimer ce paramètre ?",
								"Suppression de paramètre", JOptionPane.YES_NO_OPTION);
						if (input == 0) {
							dataEliminee.remove(listeEliminee.getSelectedIndex());
						}
					}
				}
			}
		});

		listeObligatoire.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent mouseEvent) {
				if (SwingUtilities.isRightMouseButton(mouseEvent) && listeObligatoire
						.locationToIndex(mouseEvent.getPoint()) == listeObligatoire.getSelectedIndex()) {
					if (!listeObligatoire.isSelectionEmpty()) {
						int input = JOptionPane.showConfirmDialog(null, "Voulez-vous supprimer ce paramètre ?",
								"Suppression de paramètre", JOptionPane.YES_NO_OPTION);
						if (input == 0) {
							dataObligatoire.remove(listeObligatoire.getSelectedIndex());
						}
					}
				}
			}
		});
		boxPartieGauche.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		boxPartieCentrale.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		boxPartieDroite.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));

		boxTitre.add(titreRecherche);
		boxTitre.add(labelSablier);

		// boxPartieGauche.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		// boxPartieCentrale.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		// boxPartieDroite.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));

		// creation des diff?rents elements graphiques (JLabel, Combobox, Button,
		// TextAera ?)
		// mise en page : placements des differents elements graphiques dans des Box
		boxPartieObligatoire.add(texteCouleursObligatoires);
		boxPartieObligatoire.add(scrollObligatoire);
		boxPartieObligatoire.add(butObligatoire);
		boxPartieEliminee.add(texteCouleursEliminees);
		boxPartieEliminee.add(scrollEliminee);
		boxPartieEliminee.add(butElimine);

		boxChoixGris.add(boutonBlanc);
		boxChoixGris.add(Box.createRigidArea(new Dimension(0, 50))); // Espace entre choix
		boxChoixGris.add(boutonGrisClair);
		boxChoixGris.add(Box.createRigidArea(new Dimension(0, 50))); // Espace entre choix
		boxChoixGris.add(boutonGrisFonce);
		boxChoixGris.add(Box.createRigidArea(new Dimension(0, 50))); // Espace entre choix
		boxChoixGris.add(boutonNoir);

		boxTableaux.add(Box.createRigidArea(new Dimension(20, 0)));
		boxTableaux.add(boxPartieObligatoire);
		boxTableaux.add(Box.createRigidArea(new Dimension(90, 0)));
		boxTableaux.add(boxPartieEliminee);

		boxPartieGauche.setAlignmentY(TOP_ALIGNMENT);
		boxPartieGauche.add(texteRechercheCouleur);
		boxPartieGauche.add(Box.createRigidArea(new Dimension(0, 50))); // Espace entre titre et tableaux
		boxPartieGauche.add(boxTableaux);
		boxPartieGauche.add(Box.createRigidArea(new Dimension(0, 50))); // Espace entre tableaux et bouton recherche
		boxPartieGauche.add(boutonRechercheCouleur);

		boxPartieCentrale.setAlignmentY(TOP_ALIGNMENT);
		boxPartieCentrale.add(texteRechercheGris);
		boxPartieCentrale.add(Box.createRigidArea(new Dimension(0, 125))); // Espace entre titre et choix
		boxPartieCentrale.add(boxChoixGris);
		boxPartieCentrale.add(Box.createRigidArea(new Dimension(0, 125))); // Espace entre choix et bouton recherche
		boxPartieCentrale.add(boutonRechercheGris);

		boxPartieDroite.setAlignmentY(TOP_ALIGNMENT);
		boxPartieDroite.add(texteComparaisonFichier);
		boxPartieDroite.add(Box.createRigidArea(new Dimension(0, 175))); // Espace entre titre et image fichier
		boxPartieDroite.add(labelFichier);
		boxPartieDroite.add(fichierChoisit);
		boxPartieDroite.add(Box.createRigidArea(new Dimension(0, 175))); // Espace entre image fichier et bouton
		// recherche
		boxPartieDroite.add(boutonRechercheFichier);

		boxBoutons.add(boutonRetour);
		boxBoutons.add(Box.createRigidArea(new Dimension(75, 0)));
		boxBoutons.add(boutonHistorique);

		boxTrio.add(Box.createRigidArea(new Dimension(10, 0))); // Espace entre bordure gauche et partie gauche
		boxTrio.add(boxPartieGauche);
		boxTrio.add(Box.createRigidArea(new Dimension(25, 0))); // Espace entre partie gauche et partie centrale
		boxTrio.add(boxPartieCentrale);
		boxTrio.add(Box.createRigidArea(new Dimension(75, 0))); // Espace entre partie centrale et partie droite
		boxTrio.add(boxPartieDroite);
		boxTrio.add(Box.createRigidArea(new Dimension(50, 0))); // Espace avec la bordure droite

		// mise en page : placements des differentes box dans une box principale
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 50)));
		boxMiseEnPage.add(boxTitre);
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 50)));
		boxMiseEnPage.add(boxTrio);
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 75)));
		boxMiseEnPage.add(boxBoutons);

		// mise en page : ajout de la box principale dans le panel
		this.add(boxMiseEnPage);
		boxTitre.setVisible(false);
	}

	// Methodes privees pour le bon deroulement du cas

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource().equals(butObligatoire)) {
			DialogueAjoutCouleur ajoutCouleur = new DialogueAjoutCouleur(frame);
			if (ajoutCouleur.getValider()) {
				ParametreRequeteCouleur param = new ParametreRequeteCouleur(ajoutCouleur.getCouleur(),
						ajoutCouleur.getNom(), ajoutCouleur.getPourcentage(), true);
				if (!dataObligatoire.contains(param) && !dataEliminee.contains(param)) {
					dataObligatoire.addElement(param);
				} else {
					JOptionPane.showMessageDialog(instance, "Ajout impossible", "Erreur", JOptionPane.WARNING_MESSAGE);
				}
			} else {
				JOptionPane.showMessageDialog(instance, "Ajout impossible", "Erreur", JOptionPane.WARNING_MESSAGE);
			}
		}

		if (e.getSource().equals(butElimine)) {
			DialogueAjoutCouleur ajoutCouleur = new DialogueAjoutCouleur(frame);
			if (ajoutCouleur.getValider()) {
				ParametreRequeteCouleur param = new ParametreRequeteCouleur(ajoutCouleur.getCouleur(),
						ajoutCouleur.getNom(), ajoutCouleur.getPourcentage(), false);
				if (!dataObligatoire.contains(param) && !dataEliminee.contains(param)) {
					dataEliminee.addElement(param);
				} else {
					JOptionPane.showMessageDialog(instance, "Ajout impossible", "Erreur", JOptionPane.WARNING_MESSAGE);
				}
			} else {
				JOptionPane.showMessageDialog(instance, "Ajout impossible", "Erreur", JOptionPane.WARNING_MESSAGE);
			}
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
		String propertyName = propertyChangeEvent.getPropertyName();
		PropertyName choix = PropertyName.valueOf(propertyName);
		if (choix.equals(PropertyName.MESSAGE)) {
			labelTest.setText((String) propertyChangeEvent.getNewValue());
		}
		if (choix == PropertyName.RESULTAT_IMAGE) {
			afficheAttente(false);
			if (propertyChangeEvent.getNewValue() != null) {
				frame.changerPannel(NomPannels.RESULTAT_IMAGE.getNomPannel());
			} else {
				JOptionPane.showMessageDialog(instance, "Recherche impossible", "Erreur", JOptionPane.WARNING_MESSAGE);
			}
		}
	}

	private void afficheAttente(boolean attente) {
		boxTrio.setVisible(!attente);
		boxBoutons.setVisible(!attente);
		boxTitre.setVisible(attente);
		titreRecherche.setVisible(attente);
	}
}