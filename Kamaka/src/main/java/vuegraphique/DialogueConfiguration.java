package vuegraphique;

import java.awt.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

import controller.ControllerAdmin;
import controller.moteur.GestionMoteur;
import controller.moteur.Moteur;
import model.parametre.Parametre;
import model.parametre.ValBornee;
import model.parametre.ValChoix;
import model.parametre.ValeurParametre;

public class DialogueConfiguration extends JDialog {

	/**
	 * Auto-Generated Serial ID
	 */
	private static final long serialVersionUID = 1L;
	// Declaration polices
	private final Font policeParagraphe = new Font("Calibri", Font.PLAIN, 16);

	// Declaration et creation des differents panels
	private final JPanel panConfiguration = new JPanel();

	// Declaration des JLabels
	private final JLabel choixMoteur = new JLabel("Choix moteur :");
	private final JLabel activer = new JLabel("Activer:");

	// Declaration des boutons
	private final JButton boutonAnnuler = new JButton("Annuler");
	private final JButton boutonSauvegarder = new JButton("Sauvegarder");

	// Declaration combobox
	private final JComboBox<String> comboBoxMoteur = new JComboBox<>();

	// Declaration textfield

	// Declaration checkBox
	private final JCheckBox checkBoxActivation = new JCheckBox();

	// Declaration box
	private final Box boxChoixMoteur = Box.createHorizontalBox();
	private final Box boxParametres = Box.createVerticalBox();
	private final Box boxMiseEnPage = Box.createVerticalBox();
	private final Box boxActivation = Box.createHorizontalBox();
	private final Box boxBoutons = Box.createHorizontalBox();

	// Declaration maps
	private final Map<Parametre, JSlider> mapSlider = new HashMap<>();
	private final Map<Parametre, ButtonGroup> mapBoutons = new HashMap<>();

	private final ControllerAdmin controllerAdmin;

	/**
	 * Constructeur (Met en place le Dialog)
	 * @param frame La Frame parent
	 * @param controllerAdmin Le controlleur utilisé pour l'administration
	 */
	public DialogueConfiguration(FrameUtilisateur frame, ControllerAdmin controllerAdmin) {
		super(frame, ModalityType.DOCUMENT_MODAL);
		this.controllerAdmin = controllerAdmin;
		// mise en forme du Dialog (titre, dimension, ...)
		setTitle("Configuration");
		setSize(600, 400);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		// initialisation de panAide
		initialisationConfiguration();
		// ajout des pannels dans le ContentPane de la Frame
		getContentPane().add(panConfiguration);

		setVisible(true);
	}

	/**
	 * M?thode d'initialisation du panel panAide
	 */
	public void initialisationConfiguration() {
		panConfiguration.setBackground(Color.WHITE);

		choixMoteur.setFont(policeParagraphe);
		activer.setFont(policeParagraphe);

		boutonAnnuler.addActionListener(event -> dispose());

		boxChoixMoteur.add(choixMoteur);
		boxChoixMoteur.add(Box.createRigidArea(new Dimension(20, 0)));
		boxChoixMoteur.add(comboBoxMoteur);
		afficheMoteur();
		comboBoxMoteur.addActionListener(e -> afficheParametre());
		comboBoxMoteur.setMaximumSize(new Dimension(100, 30));

		boutonSauvegarder.addActionListener(e -> {
			Moteur moteur = GestionMoteur.getMoteurs()[comboBoxMoteur.getSelectedIndex() - 1];
			moteur.setUtilise(checkBoxActivation.isSelected());
			for (Parametre p : mapBoutons.keySet()) {
				for (Enumeration<AbstractButton> enumeration = mapBoutons.get(p).getElements(); enumeration
						.hasMoreElements();) {
					AbstractButton bouton = enumeration.nextElement();
					if (bouton.isSelected()) {
						int valBouton = Integer.parseInt(bouton.getText());
						moteur.modifConfig(p, valBouton, false);
					}
				}
			}
			for (Parametre p : mapSlider.keySet()) {
				moteur.modifConfig(p, mapSlider.get(p).getValue(), false);
			}
			JOptionPane.showMessageDialog(this, "Paramètres de " + moteur.getNom() + " sauvegardés", "Sauvegarde", JOptionPane.INFORMATION_MESSAGE);
		});
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 25)));
		boxMiseEnPage.add(boxChoixMoteur);
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 10)));
		boxMiseEnPage.add(boxParametres);
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 25)));
		boxMiseEnPage.add(boxBoutons);

		// Ajout de la box principale au panneau
		panConfiguration.add(boxMiseEnPage);

	}

	private void afficheMoteur() {
		Moteur[] listeMoteur = GestionMoteur.getMoteurs();
		comboBoxMoteur.removeAllItems();
		comboBoxMoteur.addItem("");
		for (Moteur m : listeMoteur) {
			comboBoxMoteur.addItem(m.getNom());
		}
	}

	private void afficheParametre() {
		boxParametres.removeAll();
		boxBoutons.removeAll();
		mapBoutons.clear();
		mapSlider.clear();
		if (comboBoxMoteur.getSelectedIndex() != 0) {
			Moteur moteur = GestionMoteur.getMoteurs()[comboBoxMoteur.getSelectedIndex() - 1];
			Parametre[] listeParametres = moteur.getParametres();
			checkBoxActivation.setSelected(moteur.isUtilise());
			boxActivation.add(activer);
			boxActivation.add(checkBoxActivation);
			boxParametres.add(boxActivation);
			for (Parametre p : listeParametres) {
				ValeurParametre valParam = p.getValeur();
				JLabel nomParam = new JLabel(p.getNom() + " : ");
				Box boxParam = Box.createHorizontalBox();
				boxParam.add(nomParam);
				switch (valParam.getType()) {
				case BORNEE:
					ValBornee valParamBornee = (ValBornee) valParam;
					int minValParamBornee = valParamBornee.getMin();
					int maxValParamBornee = valParamBornee.getMax();
					int currentValParamBornee = valParamBornee.getValeur();
					JSlider slideParam = new JSlider();
					JLabel labelValParam = new JLabel("Valeur actuelle : " + currentValParamBornee);
					slideParam.setMinimum(minValParamBornee);
					slideParam.setMaximum(maxValParamBornee);
					slideParam.setPaintTicks(true);
					slideParam.setValue(currentValParamBornee);
					slideParam.addChangeListener(event -> labelValParam
							.setText("Valeur actuelle : " + ((JSlider) event.getSource()).getValue()));
					boxParam.add(Box.createRigidArea(new Dimension(10, 0)));
					slideParam.setMaximumSize(new Dimension(100, 30));
					boxParam.add(slideParam);
					boxParam.add(Box.createRigidArea(new Dimension(10, 0)));
					boxParam.add(labelValParam);
					mapSlider.put(p, slideParam);
					slideParam.setEnabled(controllerAdmin.verificationConnexion());
					break;
				case CHOIX:
					ValChoix<?> valParamChoix = (ValChoix<?>) valParam;
					ArrayList<?> listeValParam = valParamChoix.getListeChoix();
					ButtonGroup groupeBouton = new ButtonGroup();
					Object currentvalParamChoix = valParamChoix.getChoix();
					for (Object l : listeValParam) {
						JRadioButton boutonParam = new JRadioButton(l.toString());
						boutonParam.setSelected(l.equals(currentvalParamChoix));
						groupeBouton.add(boutonParam);
						boxParam.add(boutonParam);
						boxParam.add(Box.createRigidArea(new Dimension(10, 0)));
						boutonParam.setEnabled(controllerAdmin.verificationConnexion());
					}
					mapBoutons.put(p, groupeBouton);
					break;
				}
				boxParametres.add(Box.createRigidArea(new Dimension(0, 10)));
				boxParametres.add(boxParam);

			}
			boxBoutons.add(boutonAnnuler);
			boxBoutons.add(Box.createRigidArea(new Dimension(50, 0)));
			boxBoutons.add(boutonSauvegarder);
			// Ajout des box ? la box principale
		}
		SwingUtilities.updateComponentTreeUI(this);
	}

}
