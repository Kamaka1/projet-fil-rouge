package vuegraphique;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.*;

import controller.ControllerAfficheDetail;
import model.NomPannels;
import model.recherche.Historique;
import model.recherche.Recherche;
import model.recherche.requete.TypeRequete;

public class PanHistorique extends PanGeneral {
	// controleurs du cas + panel des cas inclus ou etendus en lien avec un acteur
	// les attributs metiers (ex : numClient)
	private static final DefaultListModel<Recherche> histoTexte = new DefaultListModel<>();
	private static final DefaultListModel<Recherche> histoImage = new DefaultListModel<>();
	private static final DefaultListModel<Recherche> histoAudio = new DefaultListModel<>();
	// Les elements graphiques :

	/**
	 * Auto-Generated Serial ID
	 */
	private static final long serialVersionUID = 5344472356472182044L;

	// Declaration et creation des polices d'ecritures
	private final Font policeParagraphe = new Font("Calibri", Font.PLAIN, 18);
	// Declaration et creation des ComboBox
	// Declaration et creation des Button

	// Declaration et creation des TextArea
	// Declaration et creation des Labels
	private final JLabel texte = new JLabel("Texte");
	private final JLabel image = new JLabel("Image");
	private final JLabel audio = new JLabel("Audio");

	// Mise en page : les Box
	private final Box boxMiseEnPage = Box.createVerticalBox();

	private final Box boxMiseEnPageHistoriques = Box.createHorizontalBox();
	private final Box boxTexte = Box.createVerticalBox();
	private final Box boxImage = Box.createVerticalBox();
	private final Box boxAudio = Box.createVerticalBox();

	private final ControllerAfficheDetail controllerAfficheDetail;


	public PanHistorique(
			// parametres pour l�initialisation des attributs m�tiers
			FrameUtilisateur frame,
			ControllerAfficheDetail controllerAfficheDetail
	// parametres correspondants au controleur du cas + panel des cas inclus ou
	// etendus
	// en relation avec un acteur
	) {
		// initialisation des attributs metiers
		super(frame);
		// initilaisation du controleur du cas + panels
		this.controllerAfficheDetail = controllerAfficheDetail;
		// des cas inclus ou etendus en lien avec un acteur
	}

	// M�thode d�initialisation du panel
	public void initialisation() {
		super.initialisation();
		// mise en forme du panel (couleur, �)

		setBackground(Color.WHITE);

		texte.setFont(policeParagraphe);
		boxTexte.setAlignmentY(TOP_ALIGNMENT);
		image.setFont(policeParagraphe);
		boxImage.setAlignmentY(TOP_ALIGNMENT);
		audio.setFont(policeParagraphe);
		boxAudio.setAlignmentY(TOP_ALIGNMENT);

		JList<Recherche> listeTexte = new JList<>(histoTexte);
		listeTexte.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listeTexte.setLayoutOrientation(JList.VERTICAL);
		listeTexte.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent mouseEvent) {
				if (SwingUtilities.isLeftMouseButton(mouseEvent)
						&& listeTexte.locationToIndex(mouseEvent.getPoint()) == listeTexte.getSelectedIndex()) {
					if (!listeTexte.isSelectionEmpty()) {
						int input = JOptionPane.showConfirmDialog(instance,
								"Voulez-vous voir les détails de cette recherche " + listeTexte.getSelectedValue(),
								"Détails", JOptionPane.YES_NO_OPTION);
						if (input == 0) {
							frame.changerPannel(NomPannels.RESULTAT_TEXTE.getNomPannel());
							controllerAfficheDetail.afficheDetails(listeTexte.getSelectedValue());
						}
					}
				}
			}
		});
		JScrollPane scrollTexte = new JScrollPane(listeTexte);
		scrollTexte.setPreferredSize(new Dimension(40, 100));
		
		JList<Recherche> listeAudio = new JList<>(histoAudio);
		listeAudio.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listeAudio.setLayoutOrientation(JList.VERTICAL);
		listeAudio.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent mouseEvent) {
				if (SwingUtilities.isLeftMouseButton(mouseEvent)
						&& listeAudio.locationToIndex(mouseEvent.getPoint()) == listeAudio.getSelectedIndex()) {
					if (!listeAudio.isSelectionEmpty()) {
						int input = JOptionPane.showConfirmDialog(instance,
								"Voulez-vous voir les détails de cette recherche " + listeAudio.getSelectedValue(),
								"Détails", JOptionPane.YES_NO_OPTION);
						if (input == 0) {
							frame.changerPannel(NomPannels.RESULTAT_AUDIO.getNomPannel());
							controllerAfficheDetail.afficheDetails(listeAudio.getSelectedValue());
						}
					}
				}
			}
		});
		JScrollPane scrollAudio = new JScrollPane(listeAudio);
		scrollAudio.setPreferredSize(new Dimension(40, 100));
		
		JList<Recherche> listeImage = new JList<>(histoImage);
		listeImage.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listeImage.setLayoutOrientation(JList.VERTICAL);
		listeImage.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent mouseEvent) {
				if (SwingUtilities.isLeftMouseButton(mouseEvent)
						&& listeImage.locationToIndex(mouseEvent.getPoint()) == listeImage.getSelectedIndex()) {
					if (!listeImage.isSelectionEmpty()) {
						int input = JOptionPane.showConfirmDialog(instance,
								"Voulez-vous voir les détails de cette recherche " + listeImage.getSelectedValue(),
								"Détails", JOptionPane.YES_NO_OPTION);
						if (input == 0) {
							frame.changerPannel(NomPannels.RESULTAT_IMAGE.getNomPannel());
							controllerAfficheDetail.afficheDetails(listeImage.getSelectedValue());
						}
					}
				}
			}
		});
		JScrollPane scrollImage = new JScrollPane(listeImage);
		scrollImage.setPreferredSize(new Dimension(40, 100));

		// creation des diff�rents elements graphiques (JLabel, Combobox, Button,
		// TextAera �)
		// mise en page : placements des differents elements graphiques dans des Box

		boxTexte.add(texte);
		boxTexte.add(scrollTexte);
		boxImage.add(image);
		boxImage.add(scrollImage);

		boxAudio.add(audio);
		boxAudio.add(scrollAudio);

		boxMiseEnPageHistoriques.add(Box.createRigidArea(new Dimension(25, 0))); // Espace entre bordure gauche et
																					// parties
		boxMiseEnPageHistoriques.add(boxTexte);
		boxMiseEnPageHistoriques.add(Box.createRigidArea(new Dimension(75, 0))); // Espace entre parties
		boxMiseEnPageHistoriques.add(boxImage);
		boxMiseEnPageHistoriques.add(Box.createRigidArea(new Dimension(75, 0))); // Espace entre parties
		boxMiseEnPageHistoriques.add(boxAudio);

		// mise en page : placements des differentes box dans une box principale
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 100))); // Espace entre bordure haute et parties
		boxMiseEnPage.add(boxMiseEnPageHistoriques);
		boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 100))); // Espace jusqu'au bouton retour
		boxMiseEnPage.add(boutonRetour);
		// mise en page : ajout de la box principale dans le panel
		this.add(boxMiseEnPage);
	}

    // Methodes priv�es pour le bon deroulement du cas
	public static void getHistorique() {
		histoTexte.clear();
		histoAudio.clear();
		histoImage.clear();
		ArrayList<Recherche> listeRecherche = Historique.getRecherches();
		for (Recherche l : listeRecherche) {
			TypeRequete type= l.getRequete().getType();
			switch (type) {
			case TEXTE:
					histoTexte.addElement(l);
				break;
			case IMAGE_RGB:
			case IMAGE_NB:
					histoImage.addElement(l);
				break;
			case SON:
					histoAudio.addElement(l);
				break;
			}
		}
	}
}