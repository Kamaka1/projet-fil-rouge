package vuegraphique;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import controller.moteur.GestionMoteur;
import controller.moteur.Moteur;
import model.Chemins;
import model.exception.FileDeleteException;

public class DialogueSuppressionFichier extends JDialog {

    /**
     * Auto-Generated Serial ID
     */
    private static final long serialVersionUID = 4319061952049878116L;

    // Declaration des images
    private final String imgUrl = Chemins.IMAGEFICHIER.getChemin();
    private final ImageIcon fichier = new ImageIcon(imgUrl);
    // Declaration et creation des polices d'ecritures
    private final Font policeTitre = new Font("Calibri", Font.BOLD, 18);
    // Declaration et creation des differents panels
    private final JPanel panFichier = new JPanel();
    // Declaration et creation des Button
    private final JButton butIndexer = new JButton();
    private final JButton butAnnuler = new JButton();
    private final JLabel fichierChoisit = new JLabel();
    // Declaration et creation des Box
    private final Box boxTitre = Box.createHorizontalBox();
    private final Box boxTexte = Box.createHorizontalBox();
    private final Box boxImage = Box.createVerticalBox();
    private final Box boxMiseEnPage = Box.createVerticalBox();
    private final Box boxBoutons = Box.createHorizontalBox();

    private File fichierChoisi;

    private DialogueSuppressionFichier instance;

    /**
     * Contructeur, met en place le Dialog
     * @param frame La frame parent.
     */
    public DialogueSuppressionFichier(FrameUtilisateur frame) {
        super(frame, ModalityType.DOCUMENT_MODAL);
        // mise en forme du Dialog (titre, dimension, ...)
        setTitle("Suppression Fichier");
        setSize(550, 475);
        setResizable(false);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        // initialisation de panFichier
        initialisationFichier();
        // ajout des pannels dans le ContentPane de la Frame
        getContentPane().add(panFichier);

        setVisible(true);
    }

    /**
     * M?thode d'initialisation du Panel panFichier
     */
    public void initialisationFichier() {
        instance = this;
        // mise en forme du panel (couleur, ?)
        panFichier.setBackground(Color.WHITE);

        // creation des diff?rents elements graphiques (JLabel, Combobox, Button, TextAera ?)
        JLabel titre = new JLabel("Suppression de fichier :");
        titre.setFont(policeTitre);

        JLabel explication = new JLabel("Explorateur de fichier :");
        Image imageFichier = fichier.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT);
        ImageIcon scaledFichier = new ImageIcon(imageFichier);
        JLabel labelFichier = new JLabel(scaledFichier);
        labelFichier.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                JFileChooser fileChooser = new JFileChooser(Chemins.DATA_KAMAKA.getChemin());
                Moteur moteur = GestionMoteur.getMoteurs()[0];
                FileNameExtensionFilter fileNameExtensionFilter = new FileNameExtensionFilter("Fichiers autorisés", moteur.getExtensionAudio(), moteur.getExtensionImage(), moteur.getExtensionTexte());
                fileChooser.setFileFilter(fileNameExtensionFilter);
                int valeurRetour = fileChooser.showOpenDialog(null);
                if (valeurRetour == JFileChooser.APPROVE_OPTION) {
                    if (!fileNameExtensionFilter.accept(fileChooser.getSelectedFile().getAbsoluteFile())) {
                        JOptionPane.showMessageDialog(instance, "Tu n'a pas le droit de supprimer ce fichier", "Erreur", JOptionPane.WARNING_MESSAGE);
                    } else {
                        fichierChoisi = fileChooser.getSelectedFile();
                        fichierChoisit.setText(fichierChoisi.getName());
                    }
                }
            }
        });
        labelFichier.setAlignmentX(CENTER_ALIGNMENT);
        fichierChoisit.setAlignmentX(CENTER_ALIGNMENT);

        butIndexer.setText("Supprimer");
        butIndexer.addActionListener(event -> {
            for (Moteur m : GestionMoteur.getMoteurs()) {
                try {
                    m.supprimerFichier(fichierChoisi);
                } catch (FileNotFoundException e) {
                    JOptionPane.showMessageDialog(this, "Fichier inconnu ou interdit", "Erreur", JOptionPane.WARNING_MESSAGE);
                    return;
                } catch (FileDeleteException e) {
                    JOptionPane.showMessageDialog(this, "Erreur lors de la suppression", "Erreur", JOptionPane.WARNING_MESSAGE);
                    return;
                } catch (NullPointerException e) {
                    JOptionPane.showMessageDialog(this, "Fichier non sélectionné", "Erreur", JOptionPane.WARNING_MESSAGE);
                    return;
                }
            }
            JOptionPane.showMessageDialog(this, "Fichier supprimé !", "Suppression de fichier", JOptionPane.INFORMATION_MESSAGE);
            dispose();
        });

        butAnnuler.setText("Annuler");
        butAnnuler.addActionListener(event -> dispose());

        // mise en page : placements des differents elements graphiques dans des Box
        boxTitre.add(titre);
        boxTexte.add(explication);
        boxImage.add(labelFichier);
        boxImage.add(fichierChoisit);
        boxBoutons.add(butIndexer);
        boxBoutons.add(Box.createRigidArea(new Dimension(80, 0)));
        boxBoutons.add(butAnnuler);
        // mise en page : placements des differentes box dans une box principale

        boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 50)));
        boxMiseEnPage.add(boxTitre);
        boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 75)));
        boxMiseEnPage.add(boxTexte);
        boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 25)));
        boxMiseEnPage.add(boxImage);

        boxMiseEnPage.add(Box.createRigidArea(new Dimension(0, 75)));
        boxMiseEnPage.add(boxBoutons);

        // mise en page : ajout de la box principale dans le panel
        panFichier.add(boxMiseEnPage);
    }

}
