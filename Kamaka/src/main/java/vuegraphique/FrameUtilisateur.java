package vuegraphique;

import java.awt.*;

import javax.swing.*;

import controller.ControllerAdmin;
import controller.ControllerAfficheDetail;
import controller.ControllerRecherche;
import model.Chemins;
import model.NomPannels;
import model.recherche.requete.TypeRequete;

public class FrameUtilisateur extends JFrame {

    // Les attributs metiers (ex : numClient)

    // Declaration des controlleurs
    final ControllerAdmin controlAdmin;
    // Declaration et creation des elements graphiques (JLabel)

    // Declaration et creation de la barre de menu (MenuBar)
    private final MenuBar barreMenu = new MenuBar();

    // Declaration et creation des differents panels
    private final PanAccueil panAccueil = new PanAccueil(this);
    private final JPanel panContents = new JPanel();

    private final PanDescripteur panDescripteur;

    // Declaration et creation du gestionnaire des cartes (CardLayout)
    private final CardLayout cartes = new CardLayout();

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    // Le constructeur
    public FrameUtilisateur(ControllerAdmin controlAdmin, ControllerRecherche controllerRecherche, ControllerAfficheDetail controllerAfficheDetail) {
        super("Moteur de recherche");
        this.controlAdmin = controlAdmin;
        Image icon = Toolkit.getDefaultToolkit().getImage(Chemins.IMAGEKAMAKA.getChemin());
        this.setIconImage(icon);

        // initialisation des attributs metiers

        // mise en forme de la frame (titre, dimension, ...)
        setSize(1280, 720);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // initialisation des differents panels : appel a leur methode d'initialisation

        panDescripteur = new PanDescripteur(this, controlAdmin);
        PanRechercheTexte panRechercheTexte = new PanRechercheTexte(this, controllerRecherche);
        PanRechercheAudio panRechercheAudio = new PanRechercheAudio(this, controllerRecherche);
        PanRechercheImage panRechercheImage = new PanRechercheImage(this, controllerRecherche);
        PanResultatTexte panResultatTexte = new PanResultatTexte(this, controllerRecherche, controllerAfficheDetail);
        PanResultatAudio panResultatAudio = new PanResultatAudio(this, controllerRecherche, controllerAfficheDetail);
        PanResultatImage panResultatImage = new PanResultatImage(this, controllerRecherche, controllerAfficheDetail);
        PanHistorique panHistorique = new PanHistorique(this, controllerAfficheDetail);

        panDescripteur.initialisation();
        panRechercheTexte.initialisation();
        panRechercheImage.initialisation();
        panRechercheAudio.initialisation();
        panAccueil.initialisation();
        panResultatTexte.initialisation();
        panResultatAudio.initialisation();
        panResultatImage.initialisation();
        panHistorique.initialisation();

        // ajout des pannels dans le ContentPane de la Frame
        panContents.setLayout(cartes);


        panContents.add(panDescripteur, NomPannels.DESCRIPTEUR.getNomPannel());
        panContents.add(panRechercheTexte, NomPannels.RECHERCHE_TEXTE.getNomPannel());
        panContents.add(panRechercheImage, NomPannels.RECHERCHE_IMAGE.getNomPannel());
        panContents.add(panRechercheAudio, NomPannels.RECHERCHE_AUDIO.getNomPannel());
        panContents.add(panResultatTexte, NomPannels.RESULTAT_TEXTE.getNomPannel());
        panContents.add(panResultatImage, NomPannels.RESULTAT_IMAGE.getNomPannel());
        panContents.add(panResultatAudio, NomPannels.RESULTAT_AUDIO.getNomPannel());
        panContents.add(panHistorique, NomPannels.HISTORIQUE.getNomPannel());

        getContentPane().add(panContents);

        // mise en page : mises en place des cartes
        initialisationAcceuil();

        // mise en place du menu
        initialisationMenu();
        setMenuBar(barreMenu);
        this.pack();
        // appel a la methode d'initialisation du menu

        // appel a la methode d'initialisation de la page d'accueil (optionnel)
        this.pack();
        this.setVisible(true);
    }

    private void initialisationAcceuil() {
        panAccueil.setVisible(true);
        panContents.add(panAccueil, NomPannels.ACCUEIL.getNomPannel());
        cartes.show(panContents, NomPannels.ACCUEIL.getNomPannel());
        // cartes.show(panContents, NomPannels.HISTORIQUE.getNomPannel()); // Pour
        // tester
    }

    public void initialisationMenu() {

        MenuItem ajouterFichier = new MenuItem("Ajouter fichier");
        ajouterFichier.addActionListener(event -> new DialogueAjoutFichier(this));

        MenuItem supprimerFichier = new MenuItem("Supprimer fichier");
        supprimerFichier.addActionListener(event -> new DialogueSuppressionFichier(this));
        supprimerFichier.setEnabled(false);

        MenuItem descripteurTexte = new MenuItem("Descripteurs texte");
        descripteurTexte.addActionListener(event -> {
            panDescripteur.descripteurTexte(TypeRequete.TEXTE);
            cartes.show(panContents, NomPannels.DESCRIPTEUR.getNomPannel());
        });
        descripteurTexte.setEnabled(false);

        MenuItem descripteurImage = new MenuItem("Descripteurs image");
        descripteurImage.addActionListener(event -> {
            panDescripteur.descripteurTexte(TypeRequete.IMAGE_NB);
            cartes.show(panContents, NomPannels.DESCRIPTEUR.getNomPannel());
        });
        descripteurImage.setEnabled(false);

        MenuItem descripteurAudio = new MenuItem("Descripteurs audio");
        descripteurAudio.addActionListener(event -> {
            panDescripteur.descripteurTexte(TypeRequete.SON);
            cartes.show(panContents, NomPannels.DESCRIPTEUR.getNomPannel());
        });
        descripteurAudio.setEnabled(false);

        Menu menuFichiers = new Menu("Fichiers");
        menuFichiers.add(ajouterFichier);
        menuFichiers.add(supprimerFichier);
        menuFichiers.addSeparator();
        menuFichiers.add(descripteurTexte);
        menuFichiers.add(descripteurImage);
        menuFichiers.add(descripteurAudio);
        barreMenu.add(menuFichiers);
        /*
         * Je ne sais pas si c'est possible de faire des Menu cliquables, je fais donc
         * des MenuItem
         *
         * Menu texte = new Menu("Texte"); barreMenu.add(texte);
         *
         * Menu image = new Menu("Image"); barreMenu.add(image);
         *
         * Menu son = new Menu("Son"); barreMenu.add(son);
         *
         * Menu configuration = new Menu("Configuration"); barreMenu.add(configuration);
         *
         * Menu connexion = new Menu("Connexion"); barreMenu.add(connexion);
         *
         * Menu aide = new Menu("Aide"); barreMenu.add(aide);
         */
        MenuItem texte = new MenuItem("Fichier texte");
        texte.addActionListener(event -> cartes.show(panContents, NomPannels.RECHERCHE_TEXTE.getNomPannel()));

        MenuItem image = new MenuItem("Fichier image");
        image.addActionListener(event -> cartes.show(panContents, NomPannels.RECHERCHE_IMAGE.getNomPannel()));

        MenuItem audio = new MenuItem("Fichier audio");
        audio.addActionListener(event -> cartes.show(panContents, NomPannels.RECHERCHE_AUDIO.getNomPannel()));

        Menu menuRecherches = new Menu("Recherches");
        menuRecherches.add(texte);
        menuRecherches.add(image);
        menuRecherches.add(audio);
        barreMenu.add(menuRecherches);

        MenuItem connexion = new MenuItem("Connexion");


        MenuItem configuration = new MenuItem("Configuration");
        configuration.addActionListener(event -> new DialogueConfiguration(this, controlAdmin));

        MenuItem aide = new MenuItem("Aide");
        aide.addActionListener(event -> new DialogueAide(this));

        Menu menuAutre = new Menu("Autre");
        menuAutre.add(connexion);
        menuAutre.add(configuration);
        menuAutre.add(aide);
        barreMenu.add(menuAutre);

        connexion.addActionListener(event -> {
            if (controlAdmin.verificationConnexion()) {
                controlAdmin.seDeconnecter();
                connexion.setLabel("Connexion");
                descripteurAudio.setEnabled(false);
                descripteurImage.setEnabled(false);
                descripteurTexte.setEnabled(false);
                supprimerFichier.setEnabled(false);
                JOptionPane.showMessageDialog(this, "Mode admin désactivé !", "Déconnexion", JOptionPane.INFORMATION_MESSAGE);
            } else {
                new DialogueConnexion(this, controlAdmin);
                if (controlAdmin.verificationConnexion()) {
                    connexion.setLabel("Deconnexion");
                    descripteurAudio.setEnabled(true);
                    descripteurImage.setEnabled(true);
                    descripteurTexte.setEnabled(true);
                    supprimerFichier.setEnabled(true);
                }
            }
        });
    }

    public void changerPannel(String nomPannel) {
        cartes.show(panContents, nomPannel);
    }
}