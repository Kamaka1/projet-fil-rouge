package vuegraphique;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class DialogueAide extends JDialog {

	/**
	 * Auto-Generated Serial ID
	 */
	private static final long serialVersionUID = -4955024231755552391L;
	
	// Declaration et creation des differents panels
	private final JPanel panAide = new JPanel();
	
	/**
	 * Constructeur (Met en place le Dialog)
	 * @param frame La Frame parent
	 */
	public DialogueAide(FrameUtilisateur frame) {
		super(frame, ModalityType.DOCUMENT_MODAL);
		// mise en forme du Dialog (titre, dimension, ...)
		setTitle("Aide");
		setSize(900, 400);
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		// initialisation de panAide
		initialisationAide();
		// ajout des pannels dans le ContentPane de la Frame
		getContentPane().add(panAide);
		
		setVisible(true);
	}
	
	/**
	 * M?thode d'initialisation du panel panAide
	 */
	public void initialisationAide() {
		panAide.setBackground(Color.LIGHT_GRAY);
		JLabel labAide = new JLabel("<html>Vous pouvez effectuer 3 types de recherche :<br>"
				+ "-Image<br>"
				+ "-Texte<br>"
				+ "-Audio<br>"
				+ "Pour chacun de ces types de recherche, vous pouvez ajouter des critères<br>"
				+ "dans les menus correspondants.<br>"
				+ "Si vous vous êtes trompé en ajoutant un critère, vous pouvez le retirer en<br>"
				+ "faisant un clic droit dessus.<br>"
				+ "Dans le cas où vous souhaiteriez faire une recherche à partir d'un autre fichier, cliquez<br>"
				+ "sur le logo du fichier.<br>"
				+ "Si vous souhaitez enregistrer une recherche, cliquez sur le bouton \"sauvegarder\" lors de<br>"
				+ "l'apparition des résultats, vous pourrez aussi consulter l'ensemble<br>"
				+ "des requêtes sauvegardées en cliquant sur le bouton \"historique\".<br>"
				+ "Les administrateurs connaissant le mot de passe peuvent se connecter<br>"
				+ "afin de régler les paramètres des moteurs de recherche."
				+ "</html>");
		labAide.setFont(new Font("Calibri", Font.PLAIN, 16));
		panAide.add(labAide);
		panAide.setVisible(true);
	}

	
	
}
