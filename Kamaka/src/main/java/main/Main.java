package main;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;

import controller.ControllerAdmin;
import controller.ControllerAfficheDetail;
import controller.ControllerRecherche;
import controller.moteur.GestionMoteur;
import controller.moteur.Moteur;
import model.Indexation;
import model.recherche.TypeIndexation;
import thread.GestionThread;
import vuegraphique.FrameUtilisateur;

/**
 * Class main permettant de lancer l'exécution du programme
 */
public class Main {

    public static void main(String[] args) {
		System.out.println("Lancement en cours, veuillez patienter...");
        Moteur[] moteurs = GestionMoteur.getMoteurs();

        for (Moteur m : moteurs){
			GestionThread.reIndexer(new Indexation(m, TypeIndexation.AUDIO));
			GestionThread.reIndexer(new Indexation(m, TypeIndexation.IMAGE));
			GestionThread.reIndexer(new Indexation(m, TypeIndexation.TEXTE));
		}


        ControllerAdmin controlAdmin = new ControllerAdmin();
        ControllerRecherche controllerRecherche = new ControllerRecherche();
		ControllerAfficheDetail controllerAfficheDetail = new ControllerAfficheDetail();
    	try {
			  UIManager.setLookAndFeel(new NimbusLookAndFeel());
			} catch (UnsupportedLookAndFeelException e) {
			  e.printStackTrace();
			}
		new FrameUtilisateur(controlAdmin, controllerRecherche, controllerAfficheDetail);
    }
}
