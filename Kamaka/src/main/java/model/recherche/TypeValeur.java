package model.recherche;

/**
 * Enumération du type de valeur
 */
public enum TypeValeur {
    DIFFERENCE, RESSEMBLANCE, TEMPS, OCCURRENCE
}
