package model.recherche;

/**
 * Permet de gérer les résultats d'une recherche
 */
public class Resultat {

    /**
     * Nom du fichier résultat de la requête
     */
    private final String nomFichier;
    /**
     * Valeur d'occurrence, de ressemblance, de différence de la comparaison
     * @see Valeur
     */
    private final Valeur valeur;

    /**
     * Constructeur d'un résultat
     * @param nomFichier Nom du fichier
     * @param valeur Valeur du résultat
     * @param typeValeur Le type de la valeur
     * @see TypeValeur
     */
    public Resultat(String nomFichier, int valeur, TypeValeur typeValeur) {
        this.nomFichier = nomFichier;
        this.valeur = new Valeur(valeur, typeValeur);
    }

    /**
     * Getter du nom du fichier
     * @return le nom du fichier
     */
    public String getNomFichier() {
        return nomFichier;
    }

    /**
     * Getter de la valeur du résultat
     * @return la valeur du résultat
     * @see Valeur
     */
    public Valeur getValeur() {
        return valeur;
    }

    /**
     * Redéfinition de la méthode ToString
     * @return un String contenant le nom du fichier
     */
    @Override
    public String toString() {
        return "" + nomFichier;
    }


    /**
     * Redéfinition de la méthode equals pour un Resultat
     * @param obj : Resultat avec lequel on fait la comparaison
     * @return true si les deux resultat sont égaux
     */
    @Override
    public boolean equals(Object obj){
        if(!(obj instanceof Resultat)){
            return false;
        }
        Resultat res = (Resultat) obj;
        return nomFichier.equals(res.getNomFichier());
    }
}
