package model.recherche;

/**
 * Enumération du type d'indexation
 */
public enum TypeIndexation {
    AUDIO, TEXTE, IMAGE
}
