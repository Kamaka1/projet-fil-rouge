package model.recherche;

/**
 * Enumération du type de comparaison (fichier ou critère)
 */
public enum TypeComparaison {
    FICHIER, CRITERE
}
