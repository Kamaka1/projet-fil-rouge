package model.recherche.requete;

import java.awt.*;
import java.util.Objects;

/**
 * Paramètre pour les requêtes couleurs
 * @see ParametreRequete
 */
public class ParametreRequeteCouleur extends ParametreRequete {

    /**
     * Couleur du paramètre
     * @see Color
     */
    private final Color color;
    /**
     * Nom associé au paramètre, à la couleur
     */
    private final String nom;
    /**
     * Pourcentage de dominance de la couleur sur l'image
     */
    private final int pourcentage; //sur 100 -> 58% = 58

    /**
     * Constructeur du paramètre de requête couleur
     * @param color La couleur qui nous intéresse
     * @param nom Le nom de la couleur
     * @param pourcentage Pourcentage de dominance
     * @param obligatoire True si on veut que la couleur soit présente, false si on veut que ce soit éliminé
     * @see Color
     */
    public ParametreRequeteCouleur(Color color, String nom, int pourcentage, boolean obligatoire){
        this.color = color;
        this.nom = nom;
        this.pourcentage = pourcentage;
        this.obligatoire = obligatoire;
    }

    /**
     * Permet d'obtenir la couleur du paramètre
     * @return la couleur du paramètre
     * @see Color
     */
    public Color getColor() {
        return color;
    }

    @Override
	public String toString() {
		return nom + " : R=" + color.getRed() + ", G=" + color.getGreen() + ", B=" + color.getBlue() + " : " + pourcentage + "%";
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParametreRequeteCouleur that = (ParametreRequeteCouleur) o;
        return Objects.equals(color, that.color);
    }
}
