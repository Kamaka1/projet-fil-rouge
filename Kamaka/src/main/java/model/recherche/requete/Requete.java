package model.recherche.requete;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

import model.recherche.TypeComparaison;

/**
 * Requete à passer en paramètre lorsque l'on lance une recherche
 */
public class Requete implements Iterable<ParametreRequete> {
    /**
     * Type de la requête
     * @see TypeRequete
     */
    private final TypeRequete type;
    /**
     * Liste des paramètres de la requête
     * @see ParametreRequete
     */
    private final ArrayList<ParametreRequete> parametres;
    /**
     * Type de comparaison de la requête (fichier ou critère)
     * @see TypeComparaison
     */
    private final TypeComparaison typeComparaison;

    /**
     * Constructeur de la requete
     * @param type Type de requete (image, son, texte ,...)
     * @param typeComparaison type de comparaison (fichier, critère)
     * @see TypeRequete
     * @see TypeComparaison
     */
    public Requete(TypeRequete type, TypeComparaison typeComparaison){
        this.type = type;
        this.parametres = new ArrayList<>();
        this.typeComparaison = typeComparaison;
    }

    /**
     * Permet d'ajouter un paramètre à la requête
     * @param p Le paramètre à ajouter
     * @see ParametreRequete
     */
    public void ajoutParametre(ParametreRequete p){
        this.parametres.add(p);
    }

    /**
     * Permet d'obtenir le type de la requête (image, son, texte)
     * @return le type de la requête
     * @see TypeRequete
     */
    public TypeRequete getType() {
        return type;
    }

    @Override
    public Iterator<ParametreRequete> iterator() {
        return this.parametres.listIterator();
    }

    /**
     * Permet d'obtenir la liste des paramètres de la requête
     * @return la liste des paramètres
     * @see ParametreRequete
     */
    public ArrayList<ParametreRequete> getParametres() {
        return parametres;
    }

    @Override
    public String toString() {
        StringBuilder message = new StringBuilder("Paramètres : ");
        for (int i = 0; i < parametres.size(); i++) {
            message.append(parametres.get(i).toString());
            if (i < parametres.size()-1)
                message.append(", ");
        }
        return message.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Requete that = (Requete) o;
        return type == that.type && Objects.equals(parametres, that.parametres) && typeComparaison == that.typeComparaison;
    }

    /**
     * Permet d'obtenir le type de comparaison de la requête
     * @return le type de comparaison
     * @see TypeComparaison
     */
    public TypeComparaison getTypeComparaison() {
		return typeComparaison;
	}
}
