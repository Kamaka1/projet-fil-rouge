package model.recherche.requete;

/**
 * Class parent des paramètres à ajouter pour réaliser une requête
 */
public abstract class ParametreRequete {
    /**
     * Si le paramètre est obligatoire il doit être en résultat, sinon il est éliminé et ne doit pas en faire parti
     */
    protected boolean obligatoire;

    /**
     * Permet de savoir si le paramètre est obligatoire
     * @return true si le paramètre est obligatoire, false sinon
     */
    public boolean isObligatoire() {
        return obligatoire;
    }

    @Override
    public String toString() {
        return "ParametreRequete{" +
                "obligatoire=" + obligatoire +
                '}';
    }
}
