package model.recherche.requete;

/**
 * Enumération des types de requête
 */
public enum TypeRequete {
//Comparaison 0         1       2
//Indexation  0         1       1       2
            TEXTE, IMAGE_RGB, IMAGE_NB, SON
}
