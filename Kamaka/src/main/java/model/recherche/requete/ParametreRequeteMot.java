package model.recherche.requete;

import java.util.Objects;

/**
 * Paramètre pour les requêtes par mot clé
 * @see ParametreRequete
 */
public class ParametreRequeteMot extends ParametreRequete {
	/**
	 * Le mot clé que l'on recherche
	 */
    private final String nom;
	/**
	 * Son nombre d'occurrence minimal
	 */
	private final int occurrence;

	/**
	 * Constructeur du paramètre pour les requetes par mot clé
	 * @param nom Le mot que l'on cherche
	 * @param occ Son nombre d'occurrence minimal
	 * @param obligatoire True si on le cherche, false si on l'élimine
	 */
	public ParametreRequeteMot(String nom, int occ, boolean obligatoire) {
		this.nom = nom;
		this.occurrence = occ;
		this.obligatoire = obligatoire;
	}

	/**
	 * Permet d'obtenir le mot clé recherché
	 * @return le mot clé
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Permet d'obtenir le nombre d'occurrence minimal
	 * @return le nombre d'occurrence
	 */
	public int getOccurrence() {
		return occurrence;
	}

	@Override
	public String toString() {
		return nom + ", occurrence : " + occurrence;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ParametreRequeteMot that = (ParametreRequeteMot) o;
		return Objects.equals(nom, that.nom);
	}
}
