package model.recherche.requete;

import java.io.File;
import java.util.Objects;

/**
 * Paramètre de requête pour les recherches par fichier
 * @see ParametreRequete
 */
public class ParametreRequeteFichier extends ParametreRequete{
    /**
     * Fichier à rechercher
     * @see File
     */
    private final File fichier;

    /**
     * Constructeur du paramètre
     * @param fichier fichier que l'on cherche à comparer
     * @see File
     */
    public ParametreRequeteFichier(File fichier){
        this.fichier = fichier;
        this.obligatoire = true;
    }

    /**
     * Permet d'obtenir le fichier que l'on cherche à comparer
     * @return le fichier
     * @see File
     */
    public File getFichier() {
        return fichier;
    }

    @Override
    public String toString() {
        return fichier.getName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParametreRequeteFichier that = (ParametreRequeteFichier) o;
        return Objects.equals(fichier, that.fichier);
    }
}
