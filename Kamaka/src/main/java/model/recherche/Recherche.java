package model.recherche;

import controller.moteur.Moteur;
import model.recherche.requete.Requete;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Contient tous les résultats d'une recherche.
 *
 * @author Rémi et Matéo
 * @see Resultat
 */
public class Recherche implements Iterable<Resultat> {

    /**
     * Liste des résultats de la recherche
     * @see Resultat
     */
    private final ArrayList<Resultat> resultats;
    /**
     * Liste des moteurs ayant fait la recherche
     * @see Moteur
     */
    private final ArrayList<Moteur> moteurs;
    /**
     * Requête réalisée
     * @see Requete
     */
    private final Requete requete;

    /**
     * Création de la recherche.
     *
     * @param requete La requête qui a été demandé.
     * @see Requete
     */
    public Recherche(Requete requete) {
        resultats = new ArrayList<>();
        moteurs = new ArrayList<>();
        this.requete = requete;
    }

    @Override
    public Iterator<Resultat> iterator() {
        return resultats.listIterator();
    }

    /**
     * Ajoute un résultat dans la liste des résultats
     * @param resultat : résultat à ajouter
     * @see Resultat
     */
    public void ajoutResultat(Resultat resultat) {
        resultats.add(resultat);
    }

    /**
     * Permet d'ajouter une liste de moteurs à la liste des moteurs actifs
     * @param moteur : le moteur à ajouter
     * @see Moteur
     */
    public void ajoutMoteur(Moteur moteur) {
        if (!moteurs.contains(moteur))
            moteurs.add(moteur);
    }

    /**
     * Permet d'ajouter une liste de moteurs à la liste des moteurs actifs
     * @param moteurs : la liste des moteurs à ajouter
     * @see Moteur
     */
    public void ajoutMoteur(ArrayList<Moteur> moteurs) {
        for (Moteur moteur : moteurs) {
            ajoutMoteur(moteur);
        }
    }

    /**
     * Getter de la requête
     * @return la requête
     * @see Requete
     */
    public Requete getRequete() {
        return requete;
    }

    /**
     * Getter de la liste des moteurs
     * @return la liste des moteurs
     * @see Moteur
     */
    public ArrayList<Moteur> getMoteurs() {
        return moteurs;
    }

    /**
     * Vérifie la présence d'au moins un moteur
     * @return false si la liste est vide
     */
    public boolean haveMoteur() {
        return !moteurs.isEmpty();
    }

    /**
     * Getter des résultats
     * @return la liste des résultats
     * @see Resultat
     */
    public ArrayList<Resultat> getResultats() {
    	return resultats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Recherche resultats1 = (Recherche) o;
        for (Moteur m : resultats1.getMoteurs()) {
            if (!moteurs.contains(m))
                return false;
        }
        for (Moteur m : moteurs) {
            if (!resultats1.getMoteurs().contains(m))
                return false;
        }

        return requete.equals(resultats1.getRequete());
    }

    @Override
    public String toString() {
        StringBuilder message = new StringBuilder(requete.toString());
        if (haveMoteur()) {
            message.append(" \n[");
            for (int i = 0; i < moteurs.size(); i++) {
                message.append(moteurs.get(i));
                if (i < moteurs.size() - 1) {
                    message.append(" + ");
                }
            }
            message.append("]");
        }
        return message.toString();
    }
}
