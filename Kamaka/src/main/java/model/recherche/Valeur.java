package model.recherche;

/**
 * Valeur de retour des résultats
 */
public class Valeur {

    /**
     * Type de la valeur
     * @see TypeValeur
     */
    private final TypeValeur type;
    /**
     * Valeur de la valeur
     */
    private final int valeur;

    /**
     * Constructeur de la valeur
     * @param valeur valeur que l'on souhaite attribuer
     * @param type Type de la valeur
     * @see TypeValeur
     */
    public Valeur(int valeur, TypeValeur type) {
        this.type = type;
        this.valeur = valeur;
    }

    /**
     * Getter du type de valeur
     * @return le type de la valeur
     * @see TypeValeur
     */
    public TypeValeur getType() {
        return type;
    }

    /**
     * Getter de la valeur
     * @return un entier correspondant à la valeur
     */
    public int getValeur() {
        return valeur;
    }

    /**
     * Redéfinition de la méthode ToString pour afficher la valeur d'un résultat
     * @return une chaîne de caractère contenant la valeur du résultat
     */
    @Override
    public String toString() {
        return " Valeur = " + valeur + " : " + type;
    }
}
