package model.recherche;

import java.util.ArrayList;

/**
 * Permet de gérer un historique.<br>
 * <strong>Classe utilitaire</strong>, méthodes static.
 *
 * @author Rémi et Matéo
 * @see Recherche
 */
public class Historique {

    /**
     * Liste de tout l'historique.
     * @see Recherche
     */
    private static final ArrayList<Recherche> recherches = new ArrayList<>();


    /**
     * C'est une classe <strong>utilitaire</strong>, toutes les méthodes à appeller sont <strong>static</strong>.
     */
    private Historique() {}

    /**
     * Si une recherche n'y est pas encore, elle est ajoutée à la liste de l'historique
     * @param recherche : recherche à ajouter à l'historique
     * @see Recherche
     */
    public static void ajoutRecherche(Recherche recherche) {
        if (recherche != null)
            recherches.add(recherche);
    }

    /**
     * Supprime une recherche de l'historique si elle existe dans l'historique
     * @param recherche : la recherche à supprimer de l'historique
     * @return true si la recherche a bien été supprimé
     * @see Recherche
     */
    public static boolean supprimerRecherche(Recherche recherche) {
        return recherches.remove(recherche);
    }

    /**
     * Getter de l'historique
     * @return l'historique des recherches
     * @see Recherche
     */
    public static ArrayList<Recherche> getRecherches() {
        return recherches;
    }

    /**
     * Permet de récupérer une recherche de l'historique
     * @param r : La recherche à récherche à chercher dans l'historique.
     * @return la recherche si elle apparaît dans l'historique, null sinon
     * @see Recherche
     */
    public static Recherche getRecherche(Recherche r) {
        for (Recherche r2 : recherches) {
            if (r2.equals(r)) {
                return r2;
            }
        }
        return null;
    }
}
