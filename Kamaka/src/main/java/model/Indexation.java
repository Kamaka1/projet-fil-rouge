package model;

import controller.moteur.Moteur;
import model.recherche.TypeIndexation;

import java.util.Objects;

/**
 * Classe utile pour l'exécution des indexations.<br>
 * Permet de regrouper toutes les informations nécessaires à une indexation.
 *
 * @author Rémi
 * @see thread.ThreadRecherche
 */
public class Indexation {

    /**
     * Le type de l'indexation.
     * @see TypeIndexation
     */
    private final TypeIndexation typeIndexation;

    /**
     * Le moteur sur lequel faire l'indexation.
     * @see Moteur
     */
    private final Moteur moteur;

    /**
     * Création de l'indexation.
     * @param moteur Le moteur sur lequel faire l'indexation.
     * @param typeIndexation Le type de l'indexation.
     *
     * @see Moteur
     * @see TypeIndexation
     */
    public Indexation(Moteur moteur, TypeIndexation typeIndexation) {
        this.moteur = moteur;
        this.typeIndexation = typeIndexation;
    }

    /**
     * Permet de récupérer le type de l'indexation
     * @return Le type de l'indexation
     * @see TypeIndexation
     */
    public TypeIndexation getTypeIndexation() {
        return typeIndexation;
    }

    /**
     * Permet de récupérer le moteur sur lequel faire l'indexation.
     * @return Le moteur sur lequel faire l'indexation.
     */
    public Moteur getMoteur() {
        return moteur;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Indexation that = (Indexation) o;
        return typeIndexation == that.typeIndexation && Objects.equals(moteur, that.moteur);
    }

    @Override
    public int hashCode() {
        return Objects.hash(typeIndexation, moteur);
    }
}
