package model;

/**
 * Enumération des différents chemins pour regrouper ces valeurs
 */
public enum Chemins {

	IMAGES ("src/main/resources/images/"),
	DATA_KAMAKA("src/c/moteurKamaka/DATA/"),
	KAMAKA_AUDIO(DATA_KAMAKA.getChemin() + "audios/"),
	KAMAKA_TEXTE(DATA_KAMAKA.getChemin() + "textes/"),
	KAMAKA_IMAGE(DATA_KAMAKA.getChemin() + "images/"),
	IMAGEKAMAKA(IMAGES.getChemin()+ "kamaka.png"),
	IMAGEFICHIER (IMAGES.getChemin() + "fichier.png"),
	IMAGEFICHIER2 (IMAGES.getChemin() + "fichier2.png"),
	IMAGESABLIER (IMAGES.getChemin() + "sablier.gif");

	private final String chemin;
	
	/**
	 * 
	 * @param chemin Chemin vers la resource
	 */
	Chemins(String chemin) {
		this.chemin = chemin;
	}
	
	/**
	 * 
	 * @return Chemin vers la resource
	 */
	public String getChemin() {
		return chemin;
	}
}
