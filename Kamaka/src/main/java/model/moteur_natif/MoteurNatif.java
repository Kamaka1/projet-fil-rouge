package model.moteur_natif;


import controller.moteur.Moteur;

/**
 * Appel aux fonctions C des moteurs de recherches.
 * Ne doit pas être utilisé en dehors des sous-classes de Moteur
 * @see Moteur
 * @author Rémi et Matéo
 */
@SuppressWarnings("unused")
public abstract class MoteurNatif {

    /**
     * Récupère la valeur d'un paramètre du fichier config
     * @param nomParam Nom du paramètre à récupérer
     * @return La valeur du paramètre
     */
    public abstract int recupereConfig(String nomParam);

    /**
     * Modification d'un paramètre du fichier config
     * @param nomParam Nom du paramètre à modifier
     * @param valeur Nouvelle valeur du paramètre
     */
    public abstract void modifConfig(String nomParam, int valeur);

    /**
     * Création des descripteurs audio.
     */
    public abstract void indexationAudioNative();
    /**
     * Création des descripteurs texte.
     */
    public abstract void indexationTexteNative();
    /**
     * Création des descripteurs image.
     */
    public abstract void indexationImageNative();

    /**
     * Effectue la recherche par critère simple demandé.
     *
     * @param typeComp Type de la comparaison.
     * @param typeIndex Type de l'indexation.
     * @param aTrouver Critère à trouver.
     * @return Tableau contenant des chaines de caractères formatées : [valeur]=[nomFichier].[extension]
     */
    public abstract String[] comparaisonCritereNative(int typeComp, int typeIndex, String aTrouver);
    /**
     * Effectue la recherche simple par fichier audio demandé.
     *
     * @param nomFic Nom du fichier
     * @return Tableau contenant des chaines de caractères formatées : [valeur]=[nomFichier].[extension]
     */
    public abstract String[] comparaisonFichierAudioNative(String nomFic);
    /**
     * Effectue la recherche simple par fichier texte demandé.
     *
     * @param nomFic Nom du fichier
     * @return Tableau contenant des chaines de caractères formatées : [valeur]=[nomFichier].[extension]
     */
    public abstract String[] comparaisonFichierTexteNative(String nomFic);
    /**
     * Effectue la recherche simple par fichier image demandé.
     *
     * @param nomFic Nom du fichier
     * @return Tableau contenant des chaines de caractères formatées : [valeur]=[nomFichier].[extension]
     */
    public abstract String[] comparaisonFichierImageNative(String nomFic);

}
