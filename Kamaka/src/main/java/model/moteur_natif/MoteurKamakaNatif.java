package model.moteur_natif;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;

/**
 * Une implémentation de la classe MoteurNatif.
 * @see MoteurNatif
 * @author Rémi et Matéo
 */
public class MoteurKamakaNatif extends MoteurNatif {

    public native int recupereConfig(String nomParam);
    public native void modifConfig(String nomParam, int valeur);

    public native void indexationAudioNative();
    public native void indexationTexteNative();
    public native void indexationImageNative();

    public native String[] comparaisonCritereNative(int typeComp, int typeIndex, String aTrouver);
    public native String[] comparaisonFichierAudioNative(String nomFic);
    public native String[] comparaisonFichierTexteNative(String nomFic);
    public native String[] comparaisonFichierImageNative(String nomFic);



    static {
        try {
            String libName = "libKamaka.so";
            URL url = MoteurKamakaNatif.class.getResource("/librairies/" + libName);

            File tmpDir;
            tmpDir = Files.createTempDirectory("Kamaka").toFile();
            tmpDir.deleteOnExit();

            File nativeLibTmpFile = new File(tmpDir, libName);
            nativeLibTmpFile.deleteOnExit();
            InputStream in = url.openStream();
            Files.copy(in, nativeLibTmpFile.toPath());

            System.load(nativeLibTmpFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}