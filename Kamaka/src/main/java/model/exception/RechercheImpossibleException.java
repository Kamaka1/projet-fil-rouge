package model.exception;

/**
 * Exception a appeler lorsqu'une recherche n'est pas possible
 */
public class RechercheImpossibleException extends RuntimeException {
        public RechercheImpossibleException(String s){
            super(s);
        }
}
