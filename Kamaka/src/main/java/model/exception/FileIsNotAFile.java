package model.exception;

/**
 * Exception a appeler lorsque le fichier que l'on cherche n'est en réalité pas un fichier (un dossier par exemple)
 */
public class FileIsNotAFile extends RuntimeException{

    public FileIsNotAFile(){
        super();
    }
}
