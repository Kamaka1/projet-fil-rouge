package model.exception;

/**
 * Exception a appeler lorsqu'il y a un problème lors de la suppression d'un fichier
 */
public class FileDeleteException extends RuntimeException {
    public FileDeleteException(){
        super();
    }
}
