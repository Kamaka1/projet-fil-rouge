package model;

/**
 * Enumération des différents événements poussant au changement de l'observer
 */
public enum PropertyName {
    RESULTAT_AUDIO, RESULTAT_TEXTE, RESULTAT_IMAGE, ATTENTE_RESULTAT, MESSAGE
}
