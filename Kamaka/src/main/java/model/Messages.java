package model;

/**
 * Enumération des différents messages à afficher pour savoir si une indexation est en cours
 */
public enum Messages {
    A_JOUR("Corpus de documents à jour"),
    INDEXATION("Indexation en cours...");

    private final String message;

    Messages(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
