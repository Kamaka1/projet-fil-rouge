package model;

/**
 * Enumération des différents pannels
 */
public enum NomPannels {

	RECHERCHE_TEXTE ("RECHERCHE_TEXTE"),
	RECHERCHE_IMAGE ("RECHERCHE_IMAGE"),
	RECHERCHE_AUDIO ("RECHERCHE_AUDIO"),
	ACCUEIL ("ACCUEIL"),
	DESCRIPTEUR ("DESCRIPTEUR"),
	RESULTAT_TEXTE ("RESULTAT_TEXTE"),
	RESULTAT_IMAGE ("RESULTAT_IMAGE"),
	RESULTAT_AUDIO ("RESULTAT_AUDIO"),
	HISTORIQUE ("HISTORIQUE");
	
	private final String nomPannel;
	
	/**
	 * 
	 * @param string Nom du panel
	 */
	NomPannels(String string) {
		this.nomPannel = string;
	}
	
	/**
	 * 
	 * @return Nom du panel
	 */
	public String getNomPannel() {
		return nomPannel;
	}
}
