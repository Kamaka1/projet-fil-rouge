package model.parametre;

/**
 * Énumération listant tous les types que les valeurs des paramètres peuvent prendre.
 * @see Parametre
 * @see ValeurParametre
 * @author Rémi et Matéo
 */
public enum TypeValeurParametre {
    BORNEE, CHOIX
}
