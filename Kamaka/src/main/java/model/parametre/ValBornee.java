package model.parametre;

/**
 * Valeur bornée entre deux autres valeurs.
 * @see Parametre
 * @see ValeurParametre
 * @author Rémi et Matéo
 */
public class ValBornee extends ValeurParametre {

    /**
     * La borne minimale.
     */
    private final int min;
    /**
     * La borne maximale.
     */
    private final int max;
    /**
     * La valeur actuelle du paramètre.
     */
    private int valeur;

    /**
     * Création de la valeur sans la valeur actuelle.
     * @param min La borne minimale.
     * @param max La borne maximale.
     */
    public ValBornee(int min, int max) {
        this.max = max;
        this.min = min;
        this.setType(TypeValeurParametre.BORNEE);
    }

    /**
     * Permet de récupérer la valeur minimale du paramètre.
     * @return La valeur minimale du paramètre.
     */
    public int getMin() {
        return min;
    }

    /**
     * Permet de récupérer la valeur maximale du paramètre.
     * @return La valeure maximale du paramètre
     */
    public int getMax() {
        return max;
    }

    /**
     * Permet de récupérer la valeur actuelle du paramètre.
     * @return La valeur actuelle du paramètre.
     */
    public int getValeur() {
        return valeur;
    }

    /**
     * Permet de modifier la valeur actuelle de la valeur du paramètre.
     * @param valeur La nouvelle valeur du paramètre.
     */
    public void setValeur(int valeur) {
        this.valeur = valeur;
    }
}
