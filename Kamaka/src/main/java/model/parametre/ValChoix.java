package model.parametre;

import java.util.ArrayList;

/**
 * Valeur à choix multiples.
 * @param <TypeChoix> Le type du choix.
 * @see Parametre
 * @see ValeurParametre
 * @author Rémi et Matéo
 */
public class ValChoix<TypeChoix> extends ValeurParametre {

    /**
     * Liste des choix possibles.
     * @see TypeChoix
     */
    private final ArrayList<TypeChoix> listeChoix;
    /**
     * Le choix actuel affecté au paramètre.
     * @see TypeChoix
     */
    private TypeChoix choix;

    /**
     * Création de la valeur.<br>
     * Pour ajouter des choix, utiliser addChoix.
     */
    public ValChoix() {
        this.setType(TypeValeurParametre.CHOIX);
        this.listeChoix = new ArrayList<>();
    }

    /**
     * Permet de récupérer le choix actuel affecté au paramètre.
     * @return Le choix affecté actuellement au paramètre.
     * @see TypeChoix
     */
    public TypeChoix getChoix() {
        return choix;
    }

    /**
     * Permet d'avoir l'indice du choix dans la liste
     * @return l'indice du choix
     */
    public int getChoixIndice() {
        return this.listeChoix.lastIndexOf(choix);
    }

    /**
     * Permet de récuperer la liste de tous les choix possible.
     * @return La liste des choix possibles.
     * @see TypeChoix
     */
    public ArrayList<TypeChoix> getListeChoix() {
        return listeChoix;
    }

    /**
     * Permet d'ajouter un choix possible au paramètre.<br>
     * Utilisé uniquement lors de la création des paramètres.
     * @param choix Le nouveau choix
     * @see TypeChoix
     */
    public void addChoix(TypeChoix choix) {
        this.listeChoix.add(choix);
    }

    /**
     * Permet de modifier le choix du paramètre
     * @param choix le nouveau choix que l'on veut associer
     * @see TypeChoix
     */
    public void setChoix(TypeChoix choix) {
        this.choix = choix;
    }

}
