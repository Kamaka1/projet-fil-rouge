package model.parametre;

import controller.moteur.Moteur;
import model.recherche.requete.TypeRequete;

import java.util.Objects;

/**
 * Objet représentant un paramètre d'un moteur.
 * @see Moteur
 * @author Rémi et Matéo
 */
public class Parametre {

    /**
     * Le nom du paramètre.
     */
    private final String nom;
    /**
     * La description du paramètre.
     */
    private final String description;
    /**
     * La valeur du paramètre.
     * @see ValeurParametre
     */
    private final ValeurParametre valeur;

    /**
     * Le type de la requête
     * @see TypeRequete
     */
    private final TypeRequete typeRequete;

    /**
     * Création du paramètre
     * @param nom           Le nom du paramètre.
     * @param description   La description du paramètre.
     * @param valeur        La valeur du paramètre.
     * @param tr Le type de la requete.
     * @see ValeurParametre
     * @see TypeRequete
     */
    public Parametre(String nom, String description, ValeurParametre valeur, TypeRequete tr) {
        this.nom = nom;
        this.valeur = valeur;
        this.description = description;
        this.typeRequete = tr;
    }

    /**
     * Permet de récupérer le nom du paramètre.
     * @return Le nom du paramètre.
     */
    public String getNom() {
        return nom;
    }

    /**
     * Permet de récupérer la valeur du paramètre.
     * @return La valeur du paramètre.
     * @see ValeurParametre
     */
    public ValeurParametre getValeur() {
        return valeur;
    }

    /**
     * Permet de récupérer la valeur du type de requête
     * @return le type de requête
     * @see TypeRequete
     */
    public TypeRequete getTypeRequete() {
        return typeRequete;
    }

    /**
     * Redéfinition de la méthode equals
     * @param o l'objet auquel on compare
     * @return true si les deux paramètres sont égaux
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Parametre parametre = (Parametre) o;
        return Objects.equals(nom, parametre.nom);
    }
}
