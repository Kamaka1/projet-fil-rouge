package model.parametre;

/**
 * Classe abstraite permettant de définir la valeur du paramètre.
 * @see Parametre
 * @author Rémi et Matéo
 */
public abstract class ValeurParametre {

    /**
     * Le type de la valeur du paramètre
     * @see TypeValeurParametre
     */
    private TypeValeurParametre type;

    /**
     * Permet d'obtenir le type de la valeur du paramètre
     * @return le type de la valeur du paramètre
     * @see TypeValeurParametre
     */
    public TypeValeurParametre getType() {
        return type;
    }

    /**
     * Permet de modifier le type de la valeur du paramètre
     * @param type le nouveau type a associer
     * @see TypeValeurParametre
     */
    protected void setType(TypeValeurParametre type) {
        this.type = type;
    }
}
