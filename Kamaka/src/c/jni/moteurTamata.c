#include <jni.h>
#include <stdio.h>
#include "model_moteur_natif_MoteurTamataNatif.h"
#include "../moteurTamata/indexation/indexation_utils.h"
#include "../moteurTamata/indexation/indexation_audio/indexation_audio.h"
#include "../moteurTamata/indexation/indexation_image/indexation_image.h"
#include "../moteurTamata/indexation/indexation_texte/indexation_texte.h"
#include "../moteurTamata/comparaison/fcn_comp_globale.h"

JNIEXPORT jint JNICALL Java_model_moteur_1natif_MoteurTamataNatif_recupereConfig (JNIEnv * env, jobject obj, jstring jNomParam){
    char * nomParam = (*env)->GetStringUTFChars(env, jNomParam, NULL);
    int * valeur = malloc(sizeof(int));
    recupere_config(nomParam, valeur);
    return *valeur;
}

JNIEXPORT void JNICALL Java_model_moteur_1natif_MoteurTamataNatif_modifConfig (JNIEnv * env, jobject obj, jstring jNomParam, jint jValeur){
    char * nomParam = (*env)->GetStringUTFChars(env, jNomParam, NULL);
    modif_config(nomParam, jValeur);
}

JNIEXPORT void JNICALL Java_model_moteur_1natif_MoteurTamataNatif_indexationAudioNative (JNIEnv * env, jobject obj) {
    indexation_audio_all();
}

JNIEXPORT void JNICALL Java_model_moteur_1natif_MoteurTamataNatif_indexationTexteNative (JNIEnv * env, jobject obj) {
    multi_indexation_texte();
}

JNIEXPORT void JNICALL Java_model_moteur_1natif_MoteurTamataNatif_indexationImageNative (JNIEnv * env, jobject obj) {
    multi_indexation_image();
}

JNIEXPORT jobjectArray JNICALL Java_model_moteur_1natif_MoteurTamataNatif_comparaisonCritereNative (JNIEnv * env , jobject obj, jint jType, jint jTypeIndex, jstring jATrouver) {
    char * aTrouver = (*env)->GetStringUTFChars(env, jATrouver, NULL);
    if (jType == 1) {
        char * stop;
        char * sVert;
        char * sBleu;

        int rouge;
        int vert;
        int bleu;
        int n;

        stop = strchr(aTrouver, ',');
        sVert = stop + 1;
        *stop = '\0';

        stop = strchr(sVert, ',');
        sBleu = stop + 1;
        *stop = '\0';

        recupere_config("nbr_bits_quantification", &n);

	    rouge = atoi(aTrouver);
	    vert = atoi(sVert);
	    bleu = atoi(sBleu);

        int val = traitement_couleur(rouge, vert, bleu, n);

        char string_val[10];
        sprintf(string_val, "%d", val);

        comparaison_critere(jType, string_val);
    } else {
        comparaison_critere(jType, aTrouver);
    }

    (*env)->ReleaseStringUTFChars(env, jATrouver, aTrouver);

    jclass classString = (*env)->FindClass(env, "java/lang/String");

    char ** noms = malloc(sizeof(char[TAILLE_MAX_FIC][TAILLE_MAX_FIC]));


			FILE *f = fopen("./src/c/moteurTamata/resultat/resultatReq", "r"); // On ouvre le fichier de résultats
			char res[100] = "", commande[100] = "";
			int nbResultats = 0;
			while(fgets(res, 100, f)!=NULL){
				int * existe = malloc(sizeof(int));
				char * path = malloc(sizeof(char[100]));
				char * stop;
				char * stopFin;
				char * occ;
				stop = strchr(res, '=');
				*stop = '\0';
				occ = stop+1;
				stopFin = strchr(occ, '\n');
				*stopFin = '\0';

				get_nom(jTypeIndex, existe, res, path); // On récupère le nom des fichiers (associés aux id)

                noms[nbResultats] = malloc(sizeof(char[TAILLE_MAX_FIC]));
                noms[nbResultats+1] = NULL;
                strcpy(noms[nbResultats], occ);
                strcat(noms[nbResultats], "=");
                strcat(noms[nbResultats], path);

				nbResultats++;
			}

    jobjectArray arrayString = (*env)->NewObjectArray(env, nbResultats, classString, NULL);

	if(nbResultats>0){
	    for (int i = 0; noms[i] != NULL; i++)
            {
                jstring jNoms = (*env)->NewStringUTF(env, noms[i]);
                (*env)->SetObjectArrayElement(env, arrayString, i, jNoms);
            }
	}

    return arrayString;
}

JNIEXPORT jobjectArray JNICALL Java_model_moteur_1natif_MoteurTamataNatif_comparaisonFichierAudioNative (JNIEnv * env , jobject obj, jstring jNomFic) {
    char * nomFic = (*env)->GetStringUTFChars(env, jNomFic, NULL);
    int returnValue = comparaison_fichier_audio(nomFic);

    (*env)->ReleaseStringUTFChars(env, jNomFic, nomFic);
    jclass classString = (*env)->FindClass(env, "java/lang/String");

     if(returnValue == 90){
        return (*env)->NewObjectArray(env, 0, classString, NULL);
     }

    char ** noms = malloc(sizeof(char[TAILLE_MAX_FIC][TAILLE_MAX_FIC]));


			FILE *f = fopen("./src/c/moteurTamata/resultat/resultatReq", "r"); // On ouvre le fichier de résultats
			char res[100] = "", commande[100] = "";
			int nbResultats = 0;
			while(fgets(res, 100, f)!=NULL){
				int * existe = malloc(sizeof(int));
				char * path = malloc(sizeof(char[100]));
				char * stop;
				char * stopFin;
				char * occ;
				stop = strchr(res, '=');
				*stop = '\0';
				occ = stop+1;
				stopFin = strchr(occ, '\n');
				*stopFin = '\0';

				get_nom(2, existe, res, path); // On récupère le nom des fichiers (associés aux id)

                noms[nbResultats] = malloc(sizeof(char[TAILLE_MAX_FIC]));
                noms[nbResultats+1] = NULL;
                strcpy(noms[nbResultats], occ);
                strcat(noms[nbResultats], "=");
                strcat(noms[nbResultats], path);

				nbResultats++;
			}

        jobjectArray arrayString = (*env)->NewObjectArray(env, nbResultats, classString, NULL);

	    for (int i = 0; noms[i] != NULL; i++)
        {
            jstring jNoms = (*env)->NewStringUTF(env, noms[i]);
            (*env)->SetObjectArrayElement(env, arrayString, i, jNoms);
        }

    return arrayString;
}

JNIEXPORT jobjectArray JNICALL Java_model_moteur_1natif_MoteurTamataNatif_comparaisonFichierTexteNative (JNIEnv * env , jobject obj, jstring jNomFic) {
    char * nomFic = (*env)->GetStringUTFChars(env, jNomFic, NULL);
    comparaison_fichier_texte(nomFic);
    (*env)->ReleaseStringUTFChars(env, jNomFic, nomFic);
    jclass classString = (*env)->FindClass(env, "java/lang/String");

    char ** noms = malloc(sizeof(char[TAILLE_MAX_FIC][TAILLE_MAX_FIC]));


			FILE *f = fopen("./src/c/moteurTamata/resultat/resultatReq", "r"); // On ouvre le fichier de résultats
			char res[100] = "", commande[100] = "";
			int nbResultats = 0;
			while(fgets(res, 100, f)!=NULL){
				int * existe = malloc(sizeof(int));
				char * path = malloc(sizeof(char[100]));
				char * stop;
				char * stopFin;
				char * occ;
				stop = strchr(res, '=');
				*stop = '\0';
				occ = stop+1;
				stopFin = strchr(occ, '\n');
				*stopFin = '\0';

				get_nom(0, existe, res, path); // On récupère le nom des fichiers (associés aux id)

                noms[nbResultats] = malloc(sizeof(char[TAILLE_MAX_FIC]));
                noms[nbResultats+1] = NULL;
                strcpy(noms[nbResultats], occ);
                strcat(noms[nbResultats], "=");
                strcat(noms[nbResultats], path);

				nbResultats++;
			}

    jobjectArray arrayString = (*env)->NewObjectArray(env, nbResultats, classString, NULL);

	    for (int i = 0; noms[i] != NULL; i++)
        {
            jstring jNoms = (*env)->NewStringUTF(env, noms[i]);
            (*env)->SetObjectArrayElement(env, arrayString, i, jNoms);
        }

    return arrayString;
}

JNIEXPORT jobjectArray JNICALL Java_model_moteur_1natif_MoteurTamataNatif_comparaisonFichierImageNative (JNIEnv * env , jobject obj, jstring jNomFic) {
    char * nomFic = (*env)->GetStringUTFChars(env, jNomFic, NULL);
    comparaison_fichier_image(nomFic);

    (*env)->ReleaseStringUTFChars(env, jNomFic, nomFic);
    jclass classString = (*env)->FindClass(env, "java/lang/String");

    char ** noms = malloc(sizeof(char[TAILLE_MAX_FIC][TAILLE_MAX_FIC]));


			FILE *f = fopen("./src/c/moteurTamata/resultat/resultatReq", "r"); // On ouvre le fichier de résultats
			char res[100] = "", commande[100] = "";
			int nbResultats = 0;
			while(fgets(res, 100, f)!=NULL){
				int * existe = malloc(sizeof(int));
				char * path = malloc(sizeof(char[100]));
				char * stop;
				char * stopFin;
				char * occ;
				stop = strchr(res, '=');
				*stop = '\0';
				occ = stop+1;
				stopFin = strchr(occ, '\n');
				*stopFin = '\0';

				get_nom(1, existe, res, path); // On récupère le nom des fichiers (associés aux id)

                noms[nbResultats] = malloc(sizeof(char[TAILLE_MAX_FIC]));
                noms[nbResultats+1] = NULL;
                strcpy(noms[nbResultats], occ);
                strcat(noms[nbResultats], "=");
                strcat(noms[nbResultats], path);

				nbResultats++;
			}

    jobjectArray arrayString = (*env)->NewObjectArray(env, nbResultats, classString, NULL);

	    for (int i = 0; noms[i] != NULL; i++)
        {
            jstring jNoms = (*env)->NewStringUTF(env, noms[i]);
            (*env)->SetObjectArrayElement(env, arrayString, i, jNoms);
        }

    return arrayString;
}