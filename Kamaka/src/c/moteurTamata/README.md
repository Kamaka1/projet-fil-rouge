# Projet fil rouge

## Format des descripteurs

### Format général

* Les descripteurs sont formatés tel une matrice.
* 3 caractères sont utilisés pour le format des descripteurs :
	* '\n' qui sépare les descripteurs.
	* '|' qui sépare les lignes de la matrice.
	* ':' qui sépare les colonnes de la matrice.
* La case correspondant à la première ligne, première colonne contient toujours l'identifiant unique du descripteur.

### Audio

* Il y a 2 variables récupérées dans le fichier de configuration : n (la taille d'une fenêtre) et m (le nombre d'intervalles).
* Une variable k est calculé et correspond au nombre de fenêtre dans le fichier son.
* Le descripteur est créé comme ceci (à partir de la deuxième ligne):
	* Il y a k+2 lignes (+1 en comptant l'id).
	* Il y a m colonnes.
	* Dans chaque case, il y a un nombre entier.
	* La case deuxième ligne, première colonne est réservée à k
	* La case troisième ligne, première colonne est réservée à m
* Exemple avec k = 3 et m = 5 :
	id|3|5|12:13:14:0:2|21:23:25:999:31415|47:78:89:5:33
* Exemple avec k = 5 et m = 2 :
	id|5|2|12:13|21:23|47:78|21:23|47:78

### Texte

* Il y a une variable récupérée dans le fichier de configuration : f (la fréquence à partir de laquelle un prend un compte un mot).
* Un descripteur d'un fichier texte est créé comme ceci (à partir de la première ligne) :
	* Il y a x + 4 (l'id, f, le nombre de termes significatifs, le nombre total de tokens du texte et x mots significatifs) lignes et 2 colonnes.
* Exemple avec 3 mots significatifs, pour un texte de 300 mots :
	id|9|3|300|mot1:16|mot2:32|mot3:10
* Exemple de fichier contenant plusieurs descripteurs :
	1|2|3|300|mot1:16|mot2:32|mot3:10\n2|2|1|30|mot1:16\n3|2|4|330|mot1:12|mot2:14|mot3:5|mot4:124

### Image

* Il y a une variable récupérée dans le fichier de configuration : n (la valeur de la quantification pour le traitement des images RGB et NB).
* Un descripteur d'un fichier image est créé comme ceci (à partir de la première ligne) :
	* Il y a au maximum 2^d*n lignes (+2 pour l'identifiant et le nombre de composantes, d étant le nombre de composantes de l'image : 3 pour RGB, 1 	   pour NB)
	* Il y a 2 colonnes
* Exemple avec d = 1 :
	59|1|2|0:3111|3:12249
* Exemple avec d = 3 et n=2 :
	1|3|64|0:480|1:448|2:476|3:472|4:438|5:459|6:498|7:463|8:462|9:498|10:505|11:465|12:476|13:450|14:467|15:430|16:456|17:451|18:501|19:481|20:509|21:10455|22:472|23:451|24:494|25:468|26:450|27:458|28:465|29:456|30:526|31:476|32:484|33:452|34:444|35:454|36:426|37:479|38:454|39:455|40:485|41:479|42:440|43:483|44:439|45:491|46:473|47:485|48:484|49:499|50:437|51:462|52:443|53:476|54:482|55:497|56:464|57:456|58:450|59:446|60:499|61:478|62:464|63:484
* Exemple de fichier contenant plusieurs descripteurs :
	62|1|2|0:5263|2:11121\n63|1|2|0:5270|2:15130
