/***********************************************************************************
* indexation_texte.h : header du fichier indexation_texte.c :
* Fonctions permettant le nettoyage, le filtrage et l'indexation des fichiers textes
* Développeur : Thibault DELPY
***********************************************************************************/

#ifndef INDEXATION_TEXTE_H_INCLUS
#define INDEXATION_TEXTE_H_INCLUS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "../indexation_utils.h"
#include "../../fonctions.h"

/**
* Phase de nettoyage d'un fichier texte (passage de .xml à .clean)
* Paramètres :
* char * nomFichier IN : Le nom du fichier sans extension à nettoyer
* Valeur de retour :
* 0 -> La fonction n'a rencontré aucun problème
* 21 -> Problème lors de l'ouverture du fichier .xml
* 22 -> Problème lors de l'ouverture du fichier .clean
*/
int xml_to_clean(char * nomFichier);

/**
* Phase de filtrage d'un fichier texte (passage de .clean à .tok)
* Paramètres :
* char * nomFichier IN : Le nom du fichier sans extension à filtrer
* Valeur de retour :
* 0 -> La fonction n'a rencontré aucun problème
* 21 -> Problème lors de l'ouverture du fichier .clean
* 22 -> Problème lors de l'ouverture du fichier stopwords
* 23 -> Problème lors de l'ouverture du fichier .tok
*/
int clean_to_tok(char * nomFichier);

/**
* Création du descripteur correspondant au fichier passé en paramètres
* Paramètres :
* char * nomFichier IN : Le nom du fichier à lire (ici un .tok) sans l'extension et sans le chemin
* char * id IN : l'id que prendra le descripteur lors de la phase d'indexation
* Valeur de retour :
* 0 -> La fonction n'a rencontré aucun problème
* 21 -> La fonction n'a pas réussi à ouvrir le fichier .tok
*/
int indexation_texte(char * nomFichier, char * id);

/**
* Lance le nettoyage, le filtrage puis l'indexation de tous les fichiers textes qui ne sont pas déjà indexés
* Valeur de retour :
* 0 -> La fonction n'a rencontré aucun problème
*/
int multi_indexation_texte();

#endif