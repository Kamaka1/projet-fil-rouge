#include "indexation_image.h"

int traitement_couleur(int red, int green, int blue, int n)
{
    //Fonction permettant de retourner une valeur à partir des
    int R1 = 0;
    int R2 = 0;
    int R3 = 0;
    int G1 = 0;
    int G2 = 0;
    int G3 = 0;
    int B1 = 0;
    int B2 = 0;
    int B3 = 0;
    int resultat;
    int tabr[10];
    int tabg[10];
    int tabb[10];

    /*  //Initialisation des tableaux
    for (int i = 0; i < 10; i++)
    {
        tabr[i] = 0;
        tabg[i] = 0;
        tabb[i] = 0;
    }

    //Conversion des valeurs de chaque couleur en binaire
    for (int i = 0; red > 0; i++)
    {
        tabr[i] = red % 2;
        red = red / 2;
    }
    for (int i = 0; green > 0; i++)
    {
        tabg[i] = green % 2;
        green = green / 2;
    }
    for (int i = 0; blue > 0; i++)
    {
        tabb[i] = blue % 2;
        blue = blue / 2;
    }

    //Récupération des 3 bits de poids forts
    if (tabr[0] == 1)
        R1 = 1;
    if (tabg[0] == 1)
        G1 = 1;
    if (tabb[0] == 1)
        B1 = 1;
    if (tabr[1] == 1)
        R2 = 1;
    if (tabg[1] == 1)
        G2 = 1;
    if (tabb[1] == 1)
        B2 = 1;
    if (tabr[2] == 1)
        R3 = 1;
    if (tabg[2] == 1)
        G3 = 1;
    if (tabb[2] == 1)
        B3 = 1;
    */

    //Calcul des 3 bits de poids forts pour une valeur rouge
    if (red >= 128)
    {
        R1 = 1;
    }
    red = red - (R1 * 128);
    if (red >= 64)
    {
        R2 = 1;
    }
    red = red - (R2 * 64);
    if (red >= 32)
    {
        R3 = 1;
    }

    //Calcul des 3 bits de poids forts pour une valeur verte
    if (green >= 128)
    {
        G1 = 1;
    }
    green = green - (G1 * 128);
    if (green >= 64)
    {
        G2 = 1;
    }
    green = green - (G2 * 64);
    if (green >= 32)
    {
        G3 = 1;
    }

    //Calcul des 3 bits de poids forts pour une valeur bleue
    if (blue >= 128)
    {
        B1 = 1;
    }
    blue = blue - (B1 * 128);
    if (blue >= 64)
    {
        B2 = 1;
    }
    blue = blue - (B2 * 64);
    if (blue >= 32)
    {
        B3 = 1;
    }

    //Traitement de la valeur du pixel en fonction des n (=2 ou 3) bits de poids fort
    if (n == 3)
    {
        resultat = R1 * 256 + R2 * 128 + R3 * 64 + G1 * 32 + G2 * 16 + G3 * 8 + B1 * 4 + B2 * 2 + B3 * 1;
    }
    if (n == 2)
    {
        resultat = R1 * 32 + R2 * 16 + G1 * 8 + G2 * 4 + B1 * 2 + B2 * 1;
    }

    //On retourne la valeur du pixel
    return resultat;
}

int traitement_gris(long nuance)
{
    int resultat;

    //On a ici 4 valeurs de retours (niveuax de gris) possibles
    if (nuance < 63)
    {
        resultat = 0;
    }
    else if (nuance >= 63 && nuance < 127)
    {
        resultat = 1;
    }
    else if (nuance >= 127 && nuance < 191)
    {
        resultat = 2;
    }
    else if (nuance >= 191)
    {
        resultat = 3;
    }

    //On retourne la valeur du niveau de gris
    return resultat;
}

int indexation_image(char *nomFichier)
{

    //Déclaration des variables
    long histo[600];
    int n, exec = 0;
    char str[5] = "";
    char *end;
    char descripteur[4096] = "";
    char buffer[1000];
    long ligne;
    long colonne;
    long type;
    int repet;
    int nb_token = 0;
    int tabr[50000];
    int tabg[50000];
    int tabb[50000];

    int *existe = malloc(sizeof(int));
    char *str_id = malloc(sizeof(char[100]));

    //Initialisation de l'histogramme
    for (int i = 0; i < 512; i++)
    {
        histo[i] = 0;
    }

    //Récupération du nombre de bits de quantification
    exec = recupere_config("nbr_bits_quantification", &n);
    if (exec)
    {
        return exec;
    }

    //On vérifie si le fichier est déjà indexé ou non
    int valRetour = get_id(1, existe, str_id, nomFichier);
    if (valRetour)
    {
        free(existe);
        free(str_id);
        return valRetour;
    }

    //S'il n'est pas indexé on l'indexe
    if (*existe)
    {
        free(existe);
        free(str_id);
        return 0;
    }
    FILE *fictxt = NULL;

    //Récupération et ouverture du fichier en mode lecture
    fictxt = fopen(nomFichier, "r");
    if (fictxt == NULL)
    {
        free(existe);
        free(str_id);
        //Erreur lors de l'ouverture du fichier 
        return 1;
    }
    else
    {
        //Placement du curseur au début du fichier .txt
        fseek(fictxt, 0, SEEK_SET);

        //Récupération des variables contenues dans la première ligne du fichier (nombre de lignes, nombre de colonnes et type d'image)
        fscanf(fictxt, "%s", str);
        ligne = strtol(str, &end, 10);
        fscanf(fictxt, "%s", str);
        colonne = strtol(str, &end, 10);
        repet = ligne * colonne;
        fscanf(fictxt, "%s", str);
        type = strtol(str, &end, 10);
        strcpy(descripteur, str_id);

        //Histogramme dans le cas d'un fichier NB (type == 1)
        if (type == 1)
        {
            int val = 0;
            long nuance;
            for (int i = 0; i < repet; i++)
            {
                fscanf(fictxt, "%s", str);
                nuance = strtol(str, &end, 10);
                val = traitement_gris(nuance);
                histo[val] = histo[val] + 1;
            }
        }

        //Histogramme dans le cas d'un fichier RBG (type == 3)
        else if (type == 3)
        {
            //Récupération de la matrice des pixels rouges
            for (int r = 0; r < repet; r++)
            {
                fscanf(fictxt, "%s", str);
                long red = strtol(str, &end, 10);
                tabr[r] = red;
            }
            //Récupération de la matrice de pixels verts
            for (int g = 0; g < repet; g++)
            {
                fscanf(fictxt, "%s", str);
                long green = strtol(str, &end, 10);
                tabg[g] = green;
            }
            //Récupération de la matrice des pixels bleus
            for (int b = 0; b < repet; b++)
            {
                fscanf(fictxt, "%s", str);
                long blue = strtol(str, &end, 10);
                tabb[b] = blue;
            }
            //Traitement de chaque pixel
            for (int cpt = 0; cpt < repet; cpt++)
            {
                int val = traitement_couleur(tabr[cpt], tabg[cpt], tabb[cpt], n);
                histo[val]++;
            }
        }
        else
        {
            free(existe);
            free(str_id);
            //Erreur lors de la lecture du fichier .txt : mauvais format
            return 2;
        }

        if(type == 3)
        {
            //Calcul du nombre de tokens
            if (n == 2)
            {
                nb_token = 64;
            }
            if (n == 3)
            {
                nb_token = 512;
            }
        }
        if(type == 1)
        {
            nb_token = 4;
        }
        

        //Ecriture de la première partie du descripteur
        strcat(descripteur, "|");
        sprintf(buffer, "%ld", type);
        strcat(descripteur, buffer);
        strcat(descripteur, "|");
        sprintf(buffer, "%d", nb_token);
        strcat(descripteur, buffer);

        //Ecriture de la deuxième partie du descripteur à partir de l'histogramme
        if (type == 3)
        {
            if (n == 2)
            {
                for (int i = 0; i < 64; i++)
                {
                    strcat(descripteur, "|");
                    sprintf(buffer, "%d", i);
                    strcat(descripteur, buffer);
                    strcat(descripteur, ":");
                    sprintf(buffer, "%ld", histo[i]);
                    strcat(descripteur, buffer);
                }
            }
            if (n == 3)
            {
                for (int i = 0; i < 512; i++)
                {
                    strcat(descripteur, "|");
                    sprintf(buffer, "%d", i);
                    strcat(descripteur, buffer);
                    strcat(descripteur, ":");
                    sprintf(buffer, "%ld", histo[i]);
                    strcat(descripteur, buffer);
                }
            }
        }
        if (type == 1)
        {
            for (int i = 0; i < 4; i++)
            {
                strcat(descripteur, "|");
                sprintf(buffer, "%d", i);
                strcat(descripteur, buffer);
                strcat(descripteur, ":");
                sprintf(buffer, "%ld", histo[i]);
                strcat(descripteur, buffer);
            }
        }
    }

    //Fermeture du fichier utilisé
    fclose(fictxt);

    //Ecriture du descripteur dans le bon fichier
    exec = ecriture_fichiers(1, existe, str_id, nomFichier, descripteur);
    if (exec)
    {
        return exec;
    }

    free(existe);
    free(str_id);

    //On retourne 0 si tout s'est bien passé
    return 0;
}

int multi_indexation_image()
{
    int valRetour;
    char nomFichier[TAILLE_MAX];
    char nbTokensString[TAILLE_MAX];

    // Récupération de la liste des fichiers à indexer
    char **noms = malloc(sizeof(char[TAILLE_MAX_FIC][TAILLE_MAX_FIC]));
    valRetour = get_fichiers(1, noms, "txt");
    if (valRetour)
    {
        for (int i = 0; noms[i] != NULL; i++)
        {
            free(noms[i]);
        }
        free(noms);
        return valRetour;
    }

    for (int i = 0; noms[i] != NULL; i++)
    {
        char *stop = strchr(noms[i], '\n');
        *stop = '\0';
        valRetour = indexation_image(noms[i]);
        if (valRetour)
        {
            for (int i = 0; noms[i] != NULL; i++)
            {
                free(noms[i]);
            }
            free(noms);
            return valRetour;
        }
    }

    for (int i = 0; noms[i] != NULL; i++)
    {
        free(noms[i]);
    }
    free(noms);
    return 0;
}
