/****************************************************************************
* indexation_image.h : Déclaration des variables et fonctions pour
* l'indexation image.
* Développeur : Dorian Bordes 
*****************************************************************************/

#ifndef INDEXATION_IMAGE_H_INCLUS
#define INDEXATION_IMAGE_H_INCLUS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "../indexation_utils.h"
#include "../../fonctions.h"

/**
* Phase de traitement des pixels pour les images RGB
* Paramètres :
* int red, green, blue IN : Les valeurs des composantes rouges, vertes et bleues
* int n IN : Le nombre de bits de quantification (2 ou 3) 
* Valeur de retour : 
* int resultat : La valeur finale du pixel couleur après le traitement
*/
int traitement_couleur(int red, int green, int blue, int n);

/**
* Phase de traitement des pixels pour les images NB
* Paramètres :
* int nuance IN : La valeur de la nuance de gris
* Valeur de retour : 
* int resultat : La valeur finale de la nuance après le traitement
*/
int traitement_gris(long nuance);


/**
* Phase de traitement des pixels pour les images NB
* Paramètres :
* int char *nomFichier IN : Le nom du fichier que l'on souhaite indexer
* Valeur de retour : 
* 0 -> L'indexation s'est bien passée
* 1 -> Erreur lors de l'ouverture du fichier à lire
* 2 -> Problème lors de la lecture du type du fichier
*/
int indexation_image(char *nomFichier);

/**
* Indexation des fichiers images RGB et NB de la base de donnée
* Valeur de retour : 
* 0 -> L'indexation de tout les documents s'est bien passée
*/
int multi_indexation_image();

#endif