/****************************************************************************
* indexation_audio.c : Déclaration des variables et fonctions pour
* l'indexation audio.
* Développeur : Rémi Laborie
*****************************************************************************/

#ifndef INDEX_AUDIO
#define INDEX_AUDIO


#include <stdio.h>
#include <stdlib.h>

#define TAILLE_TAB 100
#define NB_MAX_FLOAT 1024
#define INTERVALLE_MIN -0.5
#define INTERVALLE_MAX 0.5
#define DESCRIPTEUR_TAILLE_MAX 900000
#define CHEMIN_BASE_DESC "descripteur/descripteur_audio/base_descripteur_audio"
#define CHEMIN_LISTE_BASE "descripteur/descripteur_audio/liste_base_audio"
#include "../indexation_utils.h"
#include "../../fonctions.h"

/**
 * Variable de retour :
 * 0  * Execution normale
 * 41 * Erreur de lecture du fichier config
 * 42 * Erreur d'ouverture d'un fichier binaire
 * 43 * Erreur d'ouverture d'un fichier bas_descripteur
 **/
int indexation_audio_all();

// Fonction privée
int indexation_audio(char * path, int n, int m);


#endif
