/****************************************************************************
* indexation_utils.c : Fonctions génériques utiles aux indexations
* et également la comparaison pour certaines.
* Développeur : Rémi Laborie
*****************************************************************************/

#include <string.h>

#include "indexation_utils.h"

int get_fichiers(int type, char ** noms, char * extension) {

    char unixCommande[1000];
    char path[TAILLE_MAX_FIC];
    
    /*******************************************************
     *    Récupération du nom des fichiers grâce au ls     *
     *******************************************************/

    // Création d'un fichier temporaire pour faire le lien entre le fichier de config et le programme
    system("touch tmp");
    FILE * f = fopen("tmp", "r");

    if (f == NULL) {
        return 45;
    }

    // Début de la commande unix (voir le .h pour les chemins)
    switch (type)
    {
    case 0:
        CHEMIN_TEXTE;
        break;
    case 1:
        CHEMIN_IMAGE;
        break;
    case 2:
        CHEMIN_AUDIO
        break;
    
    default:
        return 44;
        break;
    }
    strcat(unixCommande, "*.");
    strcat(unixCommande, extension);

    // Écriture du resultat du ls dans un fichier tmp
    if (type == 0) {
        strcat(unixCommande, " > ../../../../../tmp");
    } else {
        strcat(unixCommande, " > ./tmp");
    }
    
    system(unixCommande);

    // Lecture du fichier tmp pour récupérer le résultat
    for (int i = 0; fgets(path, TAILLE_MAX_FIC, f) != NULL; i++) {
        noms[i] = malloc(sizeof(char[TAILLE_MAX_FIC]));
        noms[i+1] = NULL;
        strcpy(noms[i], path);
    }
    
    // Fermeture et suppression du fichier tmp
    fclose(f);
    system("rm ./tmp");

    return 0;
}

int get_id(int type, int * existe, char * str_id, char * path) {


    char nom_fic[TAILLE_MAX_FIC];
    char * stop, * stop_fin, * tmp_nom_fic;
    char buffer[TAILLE_MAX_FIC];

    // Variables pour la recherche d'un nouvel id
    int trouve = 1;
    int new_id = 1;
    char tmp_str_new_id[100];
    char buffer2[TAILLE_MAX_FIC];
    char * tmp_str_id;
    int tmp_id;

    // Suppression du chemin pour ne garder que le nom et l'extention du fichier
    stop = strrchr(path, '/');
    if (stop != NULL) {
        strcpy(nom_fic, stop + 1);
    } else {
        strcpy(nom_fic, path);
    }

    // Ouverture du fichier liste_base
    FILE * liste_base = NULL;
    switch (type)
    {
    case 0:
    liste_base = fopen(CHEMIN_LISTE_BASE_TEXTE, "r");
        break;
    case 1:
    liste_base = fopen(CHEMIN_LISTE_BASE_IMAGE, "r");
        break;
    case 2:
    liste_base = fopen(CHEMIN_LISTE_BASE_AUDIO, "r");
        break;
    case 10:
    //liste_base = fopen(CHEMIN_LISTE_BASE_TEXTE_COMP, "r");
    liste_base = fopen(CHEMIN_LISTE_BASE_TEXTE, "r");
        break;
    case 11:
    //liste_base = fopen(CHEMIN_LISTE_BASE_IMAGE_COMP, "r");
    liste_base = fopen(CHEMIN_LISTE_BASE_IMAGE, "r");
        break;
    case 12:
    //liste_base = fopen(CHEMIN_LISTE_BASE_AUDIO_COMP, "r");
    liste_base = fopen(CHEMIN_LISTE_BASE_AUDIO, "r");
        break;
    
    default:
        return 44;
        break;
    }
    if (liste_base == NULL) {
        return 45;
    }
    
    *existe = 0;
    // Parcours de toutes les lignes pour voir si le fichier est déjà indéxé
    while (fgets(buffer, TAILLE_MAX_FIC, liste_base) != NULL) {
        
        stop = strchr(buffer, '=');
        stop_fin = strchr(buffer, '\n');
        tmp_nom_fic = stop + 1;
        // On sépare l'id et le nom du fichier
        *stop = '\0';
        // On enlève le \n
        *stop_fin = '\0';
        // buffer correspond maintenant à l'id du fichier

        // Si on trouve le nom du fichier parmi les bases;
        if (strcmp(nom_fic, tmp_nom_fic) == 0) {
            // On dit qu'il existe
            *existe = 1;
            // On récupère son id
            strcpy(str_id, buffer);
            // Et on peut sortir de la boucle
            break;
        }
    }
    fclose(liste_base);

    // Si le fichier n'a pas d'id, qui n'a donc pas été indéxé, lui en trouver un disponible
    if (!*existe) {

        // On cherche un id tant que new_id est utilisé (trouve = 1)
        while (trouve) {
            trouve = 0;
            sprintf(tmp_str_new_id, "%d", new_id);

            // Réouverture du fichier liste_base
            FILE * liste_base = NULL;
            switch (type)
            {
            case 0:
                    liste_base = fopen(CHEMIN_LISTE_BASE_TEXTE, "r");
                break;
            case 1:
                    liste_base = fopen(CHEMIN_LISTE_BASE_IMAGE, "r");
                break;
            case 2:
                    liste_base = fopen(CHEMIN_LISTE_BASE_AUDIO, "r");
                break;
            case 10:
                    liste_base = fopen(CHEMIN_LISTE_BASE_TEXTE, "r");
                    //liste_base = fopen(CHEMIN_LISTE_BASE_TEXTE_COMP, "r");
                break;
            case 11:
                    liste_base = fopen(CHEMIN_LISTE_BASE_IMAGE, "r");
                    //liste_base = fopen(CHEMIN_LISTE_BASE_IMAGE_COMP, "r");
                break;
            case 12:
                    liste_base = fopen(CHEMIN_LISTE_BASE_AUDIO, "r");   
                    //liste_base = fopen(CHEMIN_LISTE_BASE_AUDIO_COMP, "r");
                break;
            
            default:
                return 44;
                break;
            }
            if (liste_base == NULL) {
                return 45;
            }

            // Parcours de toutes les lignes pour voir si l'id existe déjà
            while (fgets(buffer2, TAILLE_MAX_FIC, liste_base) != NULL) {

                stop = strchr(buffer2, '=');
                tmp_str_id = buffer2;
                tmp_nom_fic = stop + 1;
                tmp_id = atoi(tmp_str_id);
                *stop = '\0';

                // Si on trouve l'id,
                if (strcmp(tmp_str_new_id, tmp_str_id) == 0) {
                    // On dit qu'on l'a trouvé
                    trouve = 1;
                    // On peut sortir de la boucle pour chercher une nouveau
                    break;
                }
            }
            fclose(liste_base);

            // Si l'id n'a pas été trouvé,
            if (!trouve) {
                // On le récupère
                strcpy(str_id, tmp_str_new_id);
                // Et on peut sortir de la boucle
                break;
            } else {
                // Sinon, on cherche un nouvel id
                new_id++;
            }
        }
    }
    return 0;
}

int ecriture_fichiers(int type, int * existe, char * str_id, char * path, char * descripteur) {
    if (*existe) {
         // Si il existe et qu'on lance l'indexation quand même (cas qui ne doit normalement pas arriver)
         // Cette fonctionnalité poura être utilisé si on souhaite modifier les descripteurs sans forcément supprimer liste_base et base_descripteur
        char unixCommande[100];
        // Utilisation de la commande sed pour remplacer un descripteur déjà présent
        strcpy(unixCommande, "sed -i 's/^");
        strcat(unixCommande, str_id);
        strcat(unixCommande, ".*/");
        strcat(unixCommande, descripteur);
        strcat(unixCommande, "/' ");
        switch (type)
        {
        case 0:
            strcat(unixCommande, CHEMIN_BASE_DESC_TEXTE);
            break;
        case 1:
            strcat(unixCommande, CHEMIN_BASE_DESC_IMAGE);
            break;
        case 2:
            strcat(unixCommande, CHEMIN_BASE_DESC_AUDIO);
            break;
        
        default:
            return 44;
            break;
        }
        system(unixCommande);

    } else {
    
    char nom_fic[TAILLE_MAX_FIC];
    char * stop = strrchr(path, '/');
    if (stop != NULL) {
        strcpy(nom_fic, stop + 1);
    } else {
        strcpy(nom_fic, path);
    }

    // Écriture dans le fichier liste_base
        FILE * base_desc = NULL;
            switch (type)
            {
            case 0:
                base_desc = fopen(CHEMIN_BASE_DESC_TEXTE, "a");
                break;
            case 1:
                base_desc = fopen(CHEMIN_BASE_DESC_IMAGE, "a");
                break;
            case 2:
                base_desc = fopen(CHEMIN_BASE_DESC_AUDIO, "a");
                break;
            
            default:
                return 44;
                break;
            }
        if (base_desc == NULL) {
            return 45;
        }

        fputs(descripteur, base_desc);
        fputs("\n", base_desc);
        fclose(base_desc);

    // Écriture dans le fichier base_descripteur

        FILE * liste_base = NULL;
            switch (type)
            {
            case 0:
                liste_base = fopen(CHEMIN_LISTE_BASE_TEXTE, "a");
                break;
            case 1:
                liste_base = fopen(CHEMIN_LISTE_BASE_IMAGE, "a");
                break;
            case 2:
                liste_base = fopen(CHEMIN_LISTE_BASE_AUDIO, "a");
                break;
            
            default:
                return 44;
                break;
            }
            
        if (base_desc == NULL) {
            return 45;
        }

        fputs(str_id, liste_base);
        fputs("=", liste_base);
        fputs(nom_fic, liste_base);
        fputs("\n", liste_base);

        fclose(liste_base);
    }
}

// Même principe que get_id mais version recherche du nom
int get_nom(int type, int * existe, char * str_id, char * path) {
    *existe = 0;
    char *stop;

    // Ouverture du fichier pour savoir si le fichier a déjà un id
    FILE * liste_base = NULL;
    switch (type)
    {
    case 0:
    liste_base = fopen(CHEMIN_LISTE_BASE_TEXTE, "r");
        break;
    case 1:
    liste_base = fopen(CHEMIN_LISTE_BASE_IMAGE, "r");
        break;
    case 2:
    liste_base = fopen(CHEMIN_LISTE_BASE_AUDIO, "r");
        break;
    case 10:
    //liste_base = fopen(CHEMIN_LISTE_BASE_TEXTE_COMP, "r");
    liste_base = fopen(CHEMIN_LISTE_BASE_TEXTE, "r");
        break;
    case 11:
    //liste_base = fopen(CHEMIN_LISTE_BASE_IMAGE_COMP, "r");
    liste_base = fopen(CHEMIN_LISTE_BASE_IMAGE, "r");
        break;
    case 12:
    //liste_base = fopen(CHEMIN_LISTE_BASE_AUDIO_COMP, "r");
    liste_base = fopen(CHEMIN_LISTE_BASE_AUDIO, "r");
        break;
    
    default:
        return 44;
        break;
    }
    if (liste_base == NULL) {
        return 45;
    }
    
    char buffer[TAILLE_MAX_FIC];
    while (fgets(buffer, TAILLE_MAX_FIC, liste_base) != NULL) {

        stop = strchr(buffer, '=');
        char * stop_fin = strchr(buffer, '\n');
        char * tmp_nom_fic = stop + 1;
        *stop = '\0';
        *stop_fin = '\0';

        if (strcmp(buffer, str_id) == 0) {
            *existe = 1;
            strcpy(path, tmp_nom_fic);            
        }
    }
    fclose(liste_base);
    return 0;
}

int recupere_config(char config[], int *valeur_config)
{

	// Création d'un fichier temporaire pour faire le lien entre le fichier de config et le programme
	system("touch tmp");
	FILE *f=fopen("tmp", "r");

	if (f==NULL) {
		return 64; // Problème d'ouverture du fichier, on retourne un code d'errreur
	}

	// Commande . ou source pour récupérer les variables du fichier config (sur unix) et les mettre dans le fichier tmp

	char unixCommande[1000];
	if (system("source 2> /dev/null"))
		strcpy(unixCommande, ". ");
	else
		strcpy(unixCommande, "source ");
	strcat(unixCommande, "src/c/moteurTamata/admin.config ;    echo $");
	strcat(unixCommande, config);
	strcat(unixCommande, " > tmp ");
	system(unixCommande);

	// Récupération des variables depuis le fichier tmp dans le programme C
	char config_recup[100] = "";
	fgets(config_recup, 100, f);

	// Conversion du seuil en int pour pouvoir return plus facilement
	*valeur_config = atoi(config_recup);

	// Fermeture et suppression du fichier tmp
	fclose(f);
	system("rm tmp");

	return 0;
}

int modif_config(char nom_config[], int valeur)
{
	// Convertit la config en une string pour être traitée après par le strcat
	char string_config[100] = "";
	sprintf(string_config, "%d", valeur);
	char s1[1000] = "sed -i 's/"; // La commande sed permet d'aller directement changer une valeur dans un fichier config
	strcat(s1, nom_config); // La commande peut paraître longue, mais c'est seulement pour remplacer
	strcat(s1, "=.*/"); // à chaque fois où il le faut par le nom de la config passée en paramètre
	strcat(s1, nom_config);
	strcat(s1, "=");
	strcat(s1, string_config);
	strcat(s1, "/' ./src/c/moteurTamata/admin.config");
	system(s1);

	return 0; //Retourne 0 (tout s'est bien passé)
}