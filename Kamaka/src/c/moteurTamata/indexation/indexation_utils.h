/****************************************************************************
* indexation_utils.c : Déclaration des variables et fonctions pour
* les fonctions génériques indexation_utils.
* Développeur : Rémi Laborie
*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define TAILLE_MAX_FIC 1024
#define TAILLE_MAX 10000
// Commande ls en fonction d'où provient la demande
#define CHEMIN_TEXTE strcpy(unixCommande, "cd ./src/c/moteurTamata/DATA/textes ; ls -1 "); strcat(unixCommande, "");
#define CHEMIN_IMAGE strcpy(unixCommande, "ls -1 ./src/c/moteurTamata/DATA/images/");
#define CHEMIN_AUDIO strcpy(unixCommande, "ls -1 ./src/c/moteurTamata/DATA/audios/");


#define CHEMIN_BASE_DESC_AUDIO "./src/c/moteurTamata/descripteur/descripteur_audio/base_descripteur_audio"
#define CHEMIN_LISTE_BASE_AUDIO "./src/c/moteurTamata/descripteur/descripteur_audio/liste_base_audio"
#define CHEMIN_BASE_DESC_IMAGE "./src/c/moteurTamata/descripteur/descripteur_image/base_descripteur_image"
#define CHEMIN_LISTE_BASE_IMAGE "./src/c/moteurTamata/descripteur/descripteur_image/liste_base_image"
#define CHEMIN_BASE_DESC_TEXTE "./src/c/moteurTamata/descripteur/descripteur_texte/base_descripteur_texte"
#define CHEMIN_LISTE_BASE_TEXTE "./src/c/moteurTamata/descripteur/descripteur_texte/liste_base_texte"

/* Les chemins étaient différents pour certais test
#define CHEMIN_BASE_DESC_AUDIO_COMP "../../../descripteur/descripteur_audio/base_descripteur_audio"
#define CHEMIN_LISTE_BASE_AUDIO_COMP "../../../descripteur/descripteur_audio/liste_base_audio"
#define CHEMIN_BASE_DESC_IMAG_COMPE "../../../descripteur/descripteur_image/base_descripteur_image"
#define CHEMIN_LISTE_BASE_IMAGE_COMP "../../../descripteur/descripteur_image/liste_base_image"
#define CHEMIN_BASE_DESC_TEXTE_COMP "../../../descripteur/descripteur_texte/base_descripteur_texte"
#define CHEMIN_LISTE_BASE_TEXTE_COMP "../../../descripteur/descripteur_texte/liste_base_texte"
*/

/**
 * Paramètres :
 * type * IN Type de l'indexation (0 -> texte, 1 -> image, 2 -> audio)
 * noms * IN/OUT Résultat de la commande ls, liste des noms des fichiers avec leur extension.
 * extension * IN Extension des fichiers voulu
 *
 * Variable de retour :
 * 0  * Execution normale
 * 44 * Erreur de type
 * 45 * Erreur d'ouverture d'un fichier
 **/
int get_fichiers(int type, char ** noms, char * extension);

/**
 * Paramètres :
 * type * IN Type de l'indexation (0 -> texte, 1 -> image, 2 -> audio) ou type de comparaison (10 -> texte, 11 -> image, 12 -> audio)
 * existe * IN/OUT Pointeur sur un int, 0 -> le fichier n'est pas indexé 1 -> le fichier est déjà indexé
 * str_id * IN/OUT L'id du fichier passé en paramètre
 * path * IN Nom ou chemin du fichier pour lequel il faut trouver l'id (avec extension)
 *
 * Variable de retour :
 * 0  * Execution normale
 * 44 * Erreur de type
 * 45 * Erreur d'ouverture d'un fichier
 **/
int get_id(int type, int * existe, char * str_id, char * path);

/**
 * Paramètres :
 * type * IN Type de l'indexation (0 -> texte, 1 -> image, 2 -> audio)
 * existe * IN/OUT Pointeur sur un int, 0 -> le fichier n'est pas indexé 1 -> le fichier est déjà indexé
 * str_id * IN/OUT L'id du fichier passé en paramètre
 * path * IN nom ou chemin du fichier (avec extension)
 * descripteur * IN Descripteur du fichier
 *
 * Variable de retour :
 * 0  * Execution normale
 * 44 * Erreur de type
 * 45 * Erreur d'ouverture d'un fichier
 **/
int ecriture_fichiers(int type, int * existe, char * str_id, char * path, char * descripteur);

/**
 * Paramètres :
 * type * IN Type de l'indexation (0 -> texte, 1 -> image, 2 -> audio) ou type de comparaison (10 -> texte, 11 -> image, 12 -> audio)
 * existe * IN/OUT Pointeur sur un int, 0 -> le fichier n'est pas indexé 1 -> le fichier est déjà indexé
 * str_id * IN L'id du fichier à trouver
 * path * IN/OUT Nom ou chemin du fichier pour lequel il faut trouver l'id (avec extension)
 *
 * Variable de retour :
 * 0  * Execution normale
 * 44 * Erreur de type
 * 45 * Erreur d'ouverture d'un fichier
 **/
int get_nom(int type, int * existe, char * str_id, char * path);



/*
Fonction pour récupérer les configurations du fichier admin.config
config[] est le nom de la configuration récuperer
valeur_config correspond à sa valeur

Retourne 0 si tout s'est bien passé, 64 si il y a eu un problème d'ouverture de fichier
*/
int recupere_config(char config[], int *valeur_config);

/*
Fonction pour modifier les configurations du fichier admin.config
nom_config[] est le nom de la configuration à modifier
valeur est la nouvelle valeur attribuée à la config

Retourne 0 si tout s'est bien passé
*/
int modif_config(char nom_config[], int valeur);