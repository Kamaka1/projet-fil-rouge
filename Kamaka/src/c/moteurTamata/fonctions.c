/****************************************************************************
* fonctions.c : Contient le codage des différentes fonctions principales
* Développeur : RABINIAUX Bastien
*****************************************************************************/

#include "fonctions.h"
#include "indexation/indexation_utils.h"


/* 
Codes d'erreur :
61 : le mot de passe saisi pour aller en mode admin n'est pas bon
62 : valeur de config saisie pas dans les bornes indiquées
63 : le numéro de menu saisi n'est pas bon
64 : recupere_config n'a pas fonctonné correctement (le fichier tmp n'a pas pu être ouvert)
65 : il y a eu un problème de saisie avec la fonction lire
66 : Mauvaise couleur dans liste_couleur
*/

int saisie_mdp()
{
	char c;
	char mdp[] = "tamatadmin", saisie[15]; // Mot de passe modifiable, pourrait être une configuration plus tard
	int erreur = 0;
	while (strcmp(mdp, saisie))
	{
		if (erreur == 4)
		{
			return 61; //renvoie un code d'erreur
		}
		printf("Pour aller en mode administrateur, entrez le mot de passe : ");
		
		lire(saisie, 15);
		
		if ((strcmp(mdp, saisie))) //Si il y a une différence (donc différent de 0)
		{ 
			affiche_erreur(&erreur, 1);
		}
		
	}
	return 0;


}


int affiche_erreur(int *erreur, int num_menu){

	if((*erreur>=0)&&(*erreur < 3)){ // Cela laisse 3 essais de plus à la personne qui se trompe
		printf("\tErreur de saisie, veuillez recommencer (%d fois)\n", 3-*erreur);
		(*erreur)++;
		return num_menu;
	}else
		(*erreur)++; // Cette ligne sert pour les fonction qui ne se servent pas du return
		return remonte_menu(num_menu);
}


int remonte_menu(int num_menu)
{
	switch (num_menu)
	{
		case 1 : // Menu administrateur
		case 2 : // Menu utilisateur
			return 0; // Retournent tous les deux au menu principal
		case 3 : // Menu de recherche de fichier texte
		case 4 : // Menu de recherche de fichier image
		case 5 : // Menu de recherche de fichier audio
			return 2; // Retournent tous les trois au menu utilisateur
		case 6 : // Menu pour générer les descripteurs
		case 7 : // Menu pour consulter les descripteurs
			return 1; // Retournent tous les deux au menu administrateur
		default : // Menu principal
			exit(0); // Le programme se ferme
	}
}


int choix_mode(char msg[])
{
	char c;
	int choix = 63; 
	printf("\tVeuillez choisir %s : ", msg);
	scanf("%d", &choix);
	while ((c = getchar()) != '\n' && c != EOF) { }	
	return choix; // Renvoie le choix du menu ou le code d'erreur sinon
}

void affichage_resultat(int exec, int type, char* message) {
		if (exec == 0){
			FILE *f = fopen("./src/c/moteurTamata/resultat/resultatReq", "r"); // On ouvre le fichier de résultats
			char res[100] = "", commande[100] = "";
			int flag = 0;
			while(fgets(res, 100, f)!=NULL){
				int * existe = malloc(sizeof(int));
				char * path = malloc(sizeof(char[100]));
				char * stop;
				char * occ;
				stop = strchr(res, '=');
				*stop = '\0';
				occ = stop+1;
				get_nom(type, existe, res, path); // On récupère le nom des fichiers (associés aux id)
				printf("%s, %s : %s\n", path, message, occ);
				if (!flag){
					switch(type){ // switch qui permet, selon le type de fichier, d'attribuer le bon éditeur avec le bon chemin
						case 0 : 
							strcpy(commande, "gedit ./src/c/moteurTamata/DATA/textes/");
							break;
						case 1:
							strcpy(commande, "eog ./src/c/moteurTamata/DATA/images/");
							stop = strchr(path, '.');
                			* stop = '\0';
                			strcat(path, ".*"); // Sans ça, eog essayait d'ouvrir l'image.txt
							break;
						case 2:
							// strncpy(commande, "play DATE/audios/ ");
							break;
						default :
							printf("Mauvais type");
							break;
					}
					strcat(commande, path);
					strcat(commande, " 2>/dev/null &"); // Pour éviter les WARNING de VxServ
				}
				flag = 1; // Il ya bien eu au moins résultat, le flag est marqué
			}
			if (type != 2) { // Si différent de fichier audio car on ne sait pas ouvrir un fichier audio sur WSL
				if (flag) { // Si il y a eu au moins un résultat, on exécute la commande
					system(commande);
				}
				else
					printf("Aucun résultat\n");
			}
			fclose(f); // On ferme le fichier de résultat
	} 
	else {
		affiche_exec(exec); // Si exec n'est pas égal à 0, on affiche l'erreur correspondante
	}
}


void affiche_exec(int exec){
	switch(exec){
		case 1 :
			printf("Erreur lors de l'ouverture du fichier à lire\n");
			break;
		case 2 :
			printf("Problème lors de la lecture du type du fichier\n");
			break;
		case 21 :
			printf("Problème lors de l'ouverture du fichier .clean\n");
			break;
		case 22 :
			printf("Problème lors de l'ouverture du fichier stopwords\n");
			break;
		case 23 :
			printf("Problème lors de l'ouverture du fichier .tok\n");
			break;
		case 41 :
			printf("Erreur de lecture du fichier config\n");
			break;
		case 42 :
			printf("Erreur d'ouverture d'un fichier binaire\n");
			break;
		case 43 :
			printf("Erreur d'ouverture d'un fichier bas_descripteur\n");
			break;
		case 44 :
			printf("Erreur de type\n");
			break;
		case 45 :
			printf("Erreur d'ouverture d'un fichier\n");
			break;
		case 61 :
			printf("Le mot de passe saisi pour aller en mode admin n'est pas bon\n");
			break;
		case 62 :
			printf("Valeur de config saisie pas dans les bornes indiquées\n");
			break;
		case 63 :
			printf("Le numéro de menu saisi n'est pas bon\n");
			break;
		case 64 :
			printf("Recupere_config n'a pas fonctonné correctement (le fichier tmp n'a pas pu être ouvert)\n");
			break;
		case 65 :
			printf("Il y a eu un problème de saisie avec la fonction lire\n");
			break;
		case 66 :
			printf("Mauvaise couleur dans liste_couleur\n");
			break;
		case 80 : 
			printf("Erreur lors de l'ouverture du fichier\n");
			break;
		case 81 : 
			printf("Erreur de type (comparaison par critère)\n");
			break;
		case 86 : 
			printf("Erreur nom de fichier inexistant (comparaison par fichier)\n");
			break;
		case 88 : 
			printf("Erreur, descripteur non trouvé\n");
			break;
		case 90 : 
			printf("Erreur, extrait audio comparé plus grand que tous les autres\n");
			break;
		case 91 : 
			printf("Erreur d'exécution de la comparaison par fichier audio\n");
			break;
		default :
			printf("Erreur non identifiée\n");
			break;
	}
}


int liste_couleur(int couleur, int *pixR, int *pixG, int *pixB) {
	switch (couleur) {
		case 0 : //rouge
			*pixR = 255;
			*pixG = 0;
			*pixB = 0;
			break;
		case 1 : //vert
			*pixR = 0;
			*pixG = 255;
			*pixB = 0;
			break;
		case 2 : //bleu
			*pixR = 0;
			*pixG = 0;
			*pixB = 255;
			break;
		default : // La couleur entrée n'est pas la bonne, code d'erreur
			return 66;
			break;
	}
	return 0;
}


int lire(char *chaine, int longueur)
{
    char *position = NULL, c;
 
    // On lit le texte saisi au clavier
    if (fgets(chaine, longueur, stdin) != NULL)  // On vérifie qu'il n'y ai pas de problèmes
    {
        position = strchr(chaine, '\n'); // On recherche le \n
        if (position != NULL) // Si on a trouvé le retour à la ligne
        {
            *position = '\0'; // On le remplace par \0
        }
		else
		{
			while ((c = getchar()) != '\n' && c != EOF) { } // On vide le buffer
		}
        return 0; // On renvoie 0 si la fonction s'est déroulée sans erreur
    }
    else
    {
		while ((c = getchar()) != '\n' && c != EOF) { }
        return 65; // Si la saisie a posé problème, on renvoie un code d'erreur
    }
}


int demande_actualisation(){
	char saisie[1], c;
	printf("\nAppuyez sur entrée quand vous avez fini de lire\n");
	if((fgets(saisie, 1, stdin))!=NULL)
	while ((c = getchar()) != '\n' && c != EOF) { } // On vide le buffer
	return 0;
}



