/****************************************************************************
* Comp_Doc_Texte.c : Fonctions permettant la comparaison par fichier texte.
* Développeur : Matéo Gatel
*****************************************************************************/

#include "../../fcn_comp_globale.h"
#include "../../../indexation/indexation_utils.h"

int comp_Doc_Texte(char * nomFicParam, int * resIdFic, int * resNbMotCommun, int * nbElem){
	//Déclaration & initialisation des variables
	FILE * ficDesc = NULL;
	FILE * ficDesc2 = NULL;
	int idFic, idDesc, nbToken;
	char *stop, *suite, *suite2, *suite3;
        *nbElem = 0;
        int contains;
	char descTxt[TAILLE_UN_DESC], autreDesc[TAILLE_UN_DESC];
	char token[TAILLE_RESULTAT];
	char * listeMotCleDesc[TAILLE_RESULTAT];
	int nbMotCle = 0, exec = 0;

	//Récupération de l'id du fichier (grâce à son nom)
	int * existe = malloc(sizeof(int));
        char * str_id = malloc(sizeof(char[TAILLE_RESULTAT]));
        exec = get_id(10, existe, str_id, nomFicParam);
        if(exec){
                return exec;
        }
		if (!*existe) {
			return 86;
		}
        idFic = atoi(str_id);

	//Ouverture du fichier de descripteur
	ficDesc = fopen(CHEMIN_DESC_TEXTE, "r");
	if(ficDesc == NULL){
		return 80; //Erreur ouverture fichier
	}

	//Parcours du fichier pour trouver les tokens du fichier passé en paramètre
	while(fgets(descTxt, TAILLE_UN_DESC, ficDesc) != NULL){
		//Récupération de l'id
		stop = strchr(descTxt, '|');
		suite = stop+1;
		*stop = '\0';
		idDesc = atoi(descTxt);
		//Si l'id récupéré ici est le même que celui trouvé avec le nom du fichier
		if(idDesc == idFic){
			//Récupération du nombre de tokens
			stop = strchr(suite, '|')+1;
			stop = strchr(stop, '|');
			suite2 = stop+1;
			*stop = '\0';
			suite = strchr(suite, '|')+1;
			nbToken = atoi(suite);

			suite2 = strchr(suite2, '|')+1;
			//Récupération de la liste des tokens ainsi que leur nombre
			for(int i = 0; i<nbToken; i++){
				char * la_suite = malloc(sizeof(char[TAILLE_UN_DESC]));
				char * suite3;

				if(i==0){
					strncpy(la_suite, suite2, strlen(suite2));
				}else{
					strncpy(la_suite, suite3, strlen(suite3));
				}
               			stop = strchr(la_suite, ':');
               			char * suite4 = stop+1;
                		*stop = '\0';
				listeMotCleDesc[i] = la_suite;
				nbMotCle++;

				if(i<nbToken-1){
					suite3 = strchr(suite4, '|')+1;
				}
			}
		}
	}
	fclose(ficDesc);

	//Réouverture du fichier
	ficDesc2 = fopen(CHEMIN_DESC_TEXTE, "r");
	if(ficDesc2 == NULL){
		return 80; //Erreur ouverture fichier
	}
	//Mais pour naviguer cette fois ci dans tous les descripteurs SAUF le bon
	while(fgets(autreDesc, TAILLE_UN_DESC, ficDesc2) != NULL){
		//Récupération de l'id
		stop = strchr(autreDesc, '|');
                suite = stop+1;
                *stop = '\0';
                idDesc = atoi(autreDesc);
		//Si l'id est différent (si le descripteur n'est pas celui du fichier)
		if(idDesc != idFic){
			//Récupération du nombre de token
			suite = strchr(suite, '|')+1;
			stop = strchr(suite, '|');
			suite2 = stop+1;
			*stop = '\0';
			nbToken = atoi(suite);

			suite2 = strchr(suite2, '|')+1;

			//Pour chaque token
                        for(int i = 0; i<nbToken; i++){
                                char * la_suite = malloc(sizeof(char[TAILLE_UN_DESC]));
                                char * suite3;

                                if(i==0){
                                        strncpy(la_suite, suite2, strlen(suite2));
                                }else{
                                        strncpy(la_suite, suite3, strlen(suite3));
                                }
                                stop = strchr(la_suite, ':');

                                char * suite4 = stop+1;
				*stop = '\0';
                                strcpy(token, la_suite);
                                if(i<nbToken-1){
                                        suite3 = strchr(suite4, '|')+1;
                                }
				//on va vérifier s'il est dans notre liste et si on l'a déjà ajouté aux résultats
				for(int j=0; j<nbMotCle; j++){
					if(strcmp(listeMotCleDesc[j], token) == 0){
						contains = 0;
						int k;
						for(k=0; k<*nbElem; k++){
							if(resIdFic[k] == idDesc){
								contains = 1;
								break;
							}
						}
						if(contains == 0){
							resIdFic[*nbElem] = idDesc;
							resNbMotCommun[*nbElem] = 1;
							(*nbElem)++;
						}else{
							resNbMotCommun[k]++;
						}
					}
				}
                        }
		}
	}
	fclose(ficDesc2);
        return 0;
}

int comparaison_fichier_texte(char * nomFic){
	int resId[TAILLE_RESULTAT];
	int resNbMotCom[TAILLE_RESULTAT];
	int nbRes;
	int exec = 0;
	//Exécution de la comparaison et ajout des résultats dans les tableaux
	exec = comp_Doc_Texte(nomFic, resId, resNbMotCom, &nbRes);
	if(exec != 0){
		return exec;
	}
	//Tri des tableaux et remplissage du fichier de résultat
	exec += tri_Et_Remplissage(resId, resNbMotCom, &nbRes, 0);

	return exec;
}
