/****************************************************************************
* Comp_Doc_Audio.c : Fonctions permettant la comparaison par fichier audio.
* Développeur : Matéo Gatel
*****************************************************************************/

#include "../../fcn_comp_globale.h"
#include "../../../indexation/indexation_utils.h"
#include "../../../fonctions.h"

int comp_Doc_Audio(char *nomFicParam, int *resId, int *resDifference, int *resIndice, int *nbRes)
{
	//Déclaration et initialisation des variables
	int erreur, idFicParam, idDesc, nbColFicParam = 0, nbLigFicParam = 0, somTotFicParam = 0, somTotDesc = 0;
	FILE *ficDesc = NULL;
	long nbCharLigneDesc = 900000;
	char descAudio[nbCharLigneDesc];
	char *suite, *stop, *suite2, *suite3, *suite4;
	int minDiff, indMinDiff;
	(*nbRes) = 0;

	//Récupération du seuil dans le fichier admin.config
	int exec = 0, seuil;
	exec = recupere_config("seuil_config", &seuil);
	if(exec){
		return exec;
	}

	//Récupération de l'id du fichier passé en paramètre
	int *existe = malloc(sizeof(int));
	char *str_id = malloc(sizeof(char[TAILLE_RESULTAT]));

	erreur = get_id(12, existe, str_id, nomFicParam);
	if (erreur)
		return erreur;

	if (!*existe)
	{
		return 86;
	}
	idFicParam = atoi(str_id);

	//Ouverture du fichier avec les descripteurs
	ficDesc = fopen(CHEMIN_DESC_AUDIO, "r");
	if (ficDesc == NULL)
	{
		return 80; //Erreur ouverture fichier
	}
	//Parcours du fichier pour récupérer la matrice du fichier passé en paramètre
	while (fgets(descAudio, nbCharLigneDesc, ficDesc) != NULL)
	{
		//Récupération de l'id
		stop = strchr(descAudio, '|');
		suite = stop + 1;
		*stop = '\0';
		idDesc = atoi(descAudio);

		//Si c'est le bon descripteur on récupère sa taille
		if (idDesc == idFicParam)
		{
			stop = strchr(suite, '|');
			suite2 = stop + 1;
			*stop = '\0';
			nbLigFicParam = atoi(suite);

			stop = strchr(suite2, '|');
			suite3 = stop + 1;
			*stop = '\0';
			nbColFicParam = atoi(suite2);
			break;
		}
	}

	if (nbLigFicParam == 0 || nbColFicParam == 0)
	{
		return 88; //Erreur descripteur non trouvé
	}
	//Et on rempli la matrice
	int matriceExtrait[nbLigFicParam][nbColFicParam];
	for (int i = 0; i < nbLigFicParam; i++){
		for (int j = 0; j < nbColFicParam; j++){
			char * la_suite = malloc(sizeof(char[1000000]));
			if (j == 0 && i == 0){
				strncpy(la_suite, suite3, strlen(suite3));
			}else{
				strncpy(la_suite, suite4, strlen(suite4));
			}
			//Erreur modif param Rémi peut etre ici ?
			if (j < nbColFicParam - 1){
				stop = strchr(la_suite, ':');
			}else{
				if (!(i == nbLigFicParam - 1 && j == nbColFicParam - 1)){
					stop = strchr(la_suite, '|');
				}
			}
			char *suite5 = stop + 1;
			*stop = '\0';
			matriceExtrait[i][j] = atoi(la_suite);
			somTotFicParam += atoi(la_suite);
			suite4 = suite5;
		}
	}
	fclose(ficDesc);

	//Réouverture du fichier avec les descripteurs
	ficDesc = fopen(CHEMIN_DESC_AUDIO, "r");
	if (ficDesc == NULL){
		return 80; //Erreur ouverture fichier
	}
	//On parcourt les descripteurs sauf le bon
	while (fgets(descAudio, nbCharLigneDesc, ficDesc) != NULL)
	{
		//Récupération de l'id
		stop = strchr(descAudio, '|');
		suite = stop + 1;
		*stop = '\0';
		idDesc = atoi(descAudio);

		//Si ce n'est pas le même descripteur
		if (idDesc != idFicParam)
		{
			//Récupération de la taille
			stop = strchr(suite, '|');
			suite2 = stop + 1;
			*stop = '\0';
			int nbLig = atoi(suite);

			stop = strchr(suite2, '|');
			suite3 = stop + 1;
			*stop = '\0';
			int nbCol = atoi(suite2);

			int matDesc[nbLig][nbCol];
			//On récupère ses valeurs dans une matrice
			for (int i = 0; i < nbLig; i++)
			{
				for (int j = 0; j < nbCol; j++)
				{
					char * la_suite = malloc(sizeof(char[500000]));
					if (j == 0 && i == 0)
					{
						strncpy(la_suite, suite3, strlen(suite3));
					}
					else
					{
						strncpy(la_suite, suite4, strlen(suite4));
					}
					if (j < nbCol - 1)
					{
						stop = strchr(la_suite, ':');
					}
					else
					{
						if (!(i == nbLig - 1 && j == nbCol - 1))
						{
							stop = strchr(la_suite, '|');
						}
					}
					char *suite5 = stop + 1;
					*stop = '\0';
					matDesc[i][j] = atoi(la_suite);
					suite4 = suite5;
				}
			}

			//Comparaison, si le descripteur actuel est plus petit que celui passé en paramètre,
			//Impossible qu'il le contienne
			if (nbLigFicParam < nbLig){
				//On calcul toutes les différences dans le descripteurs
				int diffFicDesc[nbLig - nbLigFicParam];
				int nbElemDiff = 0;
				int valElem, valLigne;
				//Pour la longueur de l'extrait
				for (int k = 0; k < nbLig - nbLigFicParam; k++){
					//On le refait à chaque ligne + 1 jusqu'à longueur de l'extrait
					for (int i = k; i < k + nbLigFicParam; i++){
						//Pour chaque colonne
						for (int j = 0; j < nbCol; j++){
							//Différence des deux valeurs
							valElem = matriceExtrait[i - k][j] - matDesc[i][j];
							//Si elle est négative, on la passe en positif
							if (valElem < 0){
								valElem = -valElem;
							}
							valLigne += valElem;
						}
						somTotDesc += valLigne;
						valLigne = 0;
					}
					//On ajoute le résultat
					diffFicDesc[nbElemDiff] = somTotDesc;
					nbElemDiff++;
					somTotDesc = 0;
				}

				//On récupère la différence la plus faible
				if (nbElemDiff > 0)
				{
					minDiff = diffFicDesc[0];
					indMinDiff = 0;
					for (int i = 0; i < nbElemDiff; i++)
					{
						if (diffFicDesc[i] < minDiff)
						{
							minDiff = diffFicDesc[i];
							indMinDiff = i;
						}
					}
					//On a calculé et on compare si l'extrait est bien dans le fichier
					int borneSup = MIN_DIFF * 100 / seuil;
					if (minDiff < borneSup)
					{
						resId[*nbRes] = idDesc;
						resDifference[*nbRes] = minDiff;
						//resIndice[*nbRes] = indMinDiff;
						// Pourcentage
						float pourcentage = ((float)indMinDiff / nbElemDiff) * 100;
						float *secondes = malloc(sizeof(float));

						int * existe = malloc(sizeof(int));
						char * path = malloc(sizeof(char[100]));
						char string_id[1000];
						sprintf(string_id, "%d", idDesc);

						erreur = get_nom(2, existe, string_id, path);
						if (erreur)
							return erreur;

						erreur = get_secondes(path, pourcentage, secondes);
						if (erreur)
							return erreur;

						resIndice[*nbRes] = (int)*secondes;
						(*nbRes)++;
					}
				}
				else
				{
					//Erreur pas normal que le tableau soit vide
					return 91;
				}
			}
			else
			{
				//Erreur, fichier donné plus grand que tous les autres
				return 90;
			}
		}
	}
	fclose(ficDesc);
	return 0;
}

int get_secondes(char *path, float pourcentage, float *temps)
{
    char nom_fic[TAILLE_MAX_FIC];
    char * stop = strrchr(path, '/');
    if (stop != NULL) {
        strcpy(nom_fic, stop + 1);
    } else {
        strcpy(nom_fic, path);
    }

	stop = strchr(nom_fic, '.');
	*stop = '\0';
	strcat(nom_fic, ".wav");

	// Création d'un fichier temporaire pour faire le lien entre le fichier de config et le programme
	system("touch tmp");
	FILE *f = fopen("tmp", "r");

	if (f == NULL)
	{
		return 41;
	}

	char unixCommande[1000];
	strcpy(unixCommande, "ffmpeg -i ./src/c/moteurTamata/DATA/audios/");
	strcat(unixCommande, nom_fic);
	strcat(unixCommande, " 2>&1 | grep Duration | cut -c13-23 > tmp");
	system(unixCommande);

	// Récupération descripteur variables depuis le fichier tmp dans le programme C
	char chaine_heures[TAILLE_RESULTAT] = "";
	fgets(chaine_heures, TAILLE_RESULTAT, f);

	stop = strchr(chaine_heures, ':');
	char *chaine_minutes = stop + 1;
	*stop = '\0';

	stop = strchr(chaine_minutes, ':');
	char *chaine_secondes = stop + 1;
	*stop = '\0';

	stop = strchr(chaine_secondes, '.');
	char *chaine_milli = stop + 1;
	*stop = '\0';

	int heures = atoi(chaine_heures);
	int minutes = atoi(chaine_minutes);
	int secondes = atoi(chaine_secondes);
	int milli = atoi(chaine_milli);

	// Fermeture et suppression du fichier tmp
	fclose(f);
	system("rm tmp");

	*temps = heures * 3600 + minutes * 60 + secondes + ((float)milli) / 100;
	*temps = (*temps) * pourcentage / 100;
	return 0;
}

//Permet la comparaison de fichier audio
int comparaison_fichier_audio(char *nomFic)
{
	int resId[TAILLE_RESULTAT];
	int resIndice[TAILLE_RESULTAT];
	int resDifference[TAILLE_RESULTAT];
	int nbRes;
	int exec = 0;
	//Exécution de la comparaison
	exec = comp_Doc_Audio(nomFic, resId, resDifference, resIndice, &nbRes);
	if (exec != 0)
	{
		return exec;
	}
	//Tri et ajout des résultats dans le fichier de résultat
	exec = tri_Et_Remplissage(resId, resIndice, &nbRes, 1);
	return exec;
}
