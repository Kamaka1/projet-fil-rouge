/****************************************************************************
* fcn_comp_globale.c : Fonctions globales utile à toutes les comparaisons :
* Trier les résultats ainsi que les ajouter dans le fichier de résultat.
* Développeur : Matéo Gatel
*****************************************************************************/

#include "fcn_comp_globale.h"
#include "../fonctions.h"
#include "../indexation/indexation_utils.h"

int tri_Et_Remplissage(int *idDesc, int *occDesc, int *nbRes, int ordre)
{
	int exec = 0;
	//Tri des résultats dans le bon ordre
        exec = tri_Res(idDesc, occDesc, nbRes, ordre);
        if(exec){
		return exec;
	}
	//Remplissage du fichier de résultat
	exec = remplissage_Fichier(idDesc, occDesc, nbRes);
	return exec;
}

int remplissage_Fichier(int *idDesc, int *occDesc, int *nbRes)
{
        int nbResAttendu, exec =0;
       	//Récupération du nombre de résultats à ajouter dans le fichier
	exec = recupere_config("nbr_fichiers_config", &nbResAttendu);
        if(exec){
		return exec; //Si erreur récupération param
	}
	char unixCmd[100];
        char instruction[100];
	//On vide le fichier (s'il y avait un résultat avant)
        sprintf(unixCmd, "> %s", NOM_FIC_RES);
        system(unixCmd);
	//On vérifie si on doit limiter le nombre de résultat ou s'il n'y en a pas assez
        int nbResFinal;
        if (*nbRes > nbResAttendu)
        {
                nbResFinal = nbResAttendu;
        }
        else
        {
                nbResFinal = *nbRes;
        }
	//Pour chaque résultat, on l'écrit dans le fichier
        for (int i = 0; i < nbResFinal; i++)
        {
                sprintf(unixCmd, "echo %d=%d >> %s", idDesc[i], occDesc[i], NOM_FIC_RES);
                system(unixCmd);
        }
        return 0;
}

int tri_Res(int *idDesc, int *occDesc, int *nbRes, int ordre)
{
        int occTemp, idTemp;
        for (int i = 0; i < *nbRes; i++)
        {
                for (int j = 0; j < (*nbRes) - 1; j++)
                {
			//Tri par ordre décroissant
                        if (occDesc[j] < occDesc[j + 1] && ordre == 0)
                        {
                                idTemp = idDesc[j];
                                occTemp = occDesc[j];

                                idDesc[j] = idDesc[j + 1];
                                occDesc[j] = occDesc[j + 1];

                                idDesc[j + 1] = idTemp;
                                occDesc[j + 1] = occTemp;
                        }
			//Tri par ordre croissant
                        if (occDesc[j] > occDesc[j + 1] && ordre == 1)
                        {
                                idTemp = idDesc[j + 1];
                                occTemp = occDesc[j + 1];

                                idDesc[j + 1] = idDesc[j];
                                occDesc[j + 1] = occDesc[j];

                                idDesc[j] = idTemp;
                                occDesc[j] = occTemp;
                        }
                }
        }
        return 0;
}
