/****************************************************************************
* menu.c : Contient le codage des fonctions d'affichage des différents écrans
* Développeur : RABINIAUX Bastien
*****************************************************************************/

#include "menu.h"
#include "../indexation/indexation_utils.h"

/*
Tous les écrans sont affichés avec une multitude de printf, il est possible de n'en mettre qu'un pour tout.
Cependant, en mettre plusieurs permet d'éviter le décalage produit avec le retour à la ligne de fonction \.
Ainsi, ce qui est affiché ici est ce qui sera affiché à l'exécution.
*/

void affiche_menu()
{
	system("clear"); // Efface l'écran précédent
	
	printf("______________________________________________________\n\n");
	printf("		       -------- \n");
	printf("		      | KAMAKA |\n");
	printf("		       -------- \n");
	printf("\n");
	printf(" --------------------	 	  ----------------- \n");
	printf("| Administrateur : 1 |		 | Utilisateur : 2 |\n");
	printf(" -------------------- 	      	  ----------------- \n");
	printf("\n");
	printf("Exit : 0\n");
	printf("______________________________________________________\n\n");
	
}

void affiche_admin()
{
	int /*seuil = -1,*/ seuil_mot = -1, nbr_fichiers= -1, nbr_intervalles = -1, nbr_points = -1, nbr_bits_quantification = -1, exec;
	/* Si -1 est affiché, alors il y a un problème
	Sinon, c'est exec qui prends le dessus.
	Si il vaut 0, rien ne se passe, mais si il y a un problème avec récupère config
	alors l'erreur correspondante sera directement affichée
	*/

	/* Partie mise en commentaire car le seuil de similarité n'a finalement pas été traité
	exec = recupere_config("seuil_config", &seuil);
	if (exec)
	{
		affiche_exec(exec);
	}
	*/
	exec = recupere_config("seuil_mot_config", &seuil_mot);
	if (exec)
	{
		affiche_exec(exec);
	}
	exec = recupere_config("nbr_fichiers_config", &nbr_fichiers);
	if (exec)
	{
		affiche_exec(exec);
	}
	exec = recupere_config("nbr_intervalles_config", &nbr_intervalles);
	if (exec)
	{
		affiche_exec(exec);
	}
	exec = recupere_config("nbr_points_config", &nbr_points);
	if (exec)
	{
		affiche_exec(exec);
	}
	exec = recupere_config("nbr_bits_quantification", &nbr_bits_quantification);
	if (exec)
	{
		affiche_exec(exec);
	}


	system("clear"); // Efface l'écran précédent
	
	printf("______________________________________________________\n\n");
	printf("                  ---------------------  \n");
	printf("                 | Mode administrateur | \n");
	printf("                  ---------------------  \n");
	/*
	printf("\n");
	printf(" -------------------------------- \n");
	printf("| 1 - Seuil de Similarité : %d %% |\n", seuil);
	printf(" -------------------------------- \n");
	*/
	printf("\n");
	printf(" ------------------------------------- \n");
	printf("| 1 - Seuil d'occurence d'un mot : %d |\n", seuil_mot);
	printf(" ------------------------------------- \n");
	printf("\n");
	printf(" -------------------------------------- \n");
	printf("| 2 - Nombre de fichiers affichés : %d |\n", nbr_fichiers);
	printf(" -------------------------------------- \n");
	printf("\n");
	printf(" -------------------------------------------------- \n");
	printf("| 3 - Nombre d'intervalles dans l'histogramme : %d |\n", nbr_intervalles);
	printf(" -------------------------------------------------- \n");
	printf("\n");
	printf(" ---------------------------------------------- \n");
	printf("| 4 - Nombre de points dans une fenêtre : %d |\n", nbr_points);
	printf(" ---------------------------------------------- \n");
	printf("\n");
	printf(" ------------------------------------------- \n");
	printf("| 5 - Nombre de bits de quantification : %d |\n", nbr_bits_quantification);
	printf(" ------------------------------------------- \n");
	printf("\n");
	printf(" ------------------------------ \n");
	printf("| 6 - Générer les descripteurs |\n");
	printf(" ------------------------------ \n");
	printf("\n");
	printf(" -------------------------------- \n");
	printf("| 7 - Consulter les descripteurs |\n");
	printf(" -------------------------------- \n");
	printf("\n");
	printf("Exit : 0                            Menu précédent : 8\n");
	printf("______________________________________________________\n\n");
		
}


// Fonction pour afficher le mode user (écran numéro 2)
void affiche_user()
{
	int nbr_res = -1, exec;
	/* Si -1 est affiché, alors il y a un problème
	Sinon, c'est exec qui prends le dessus.
	Si il vaut 0, rien ne se passe, mais si il y a un problème avec récupère config
	alors l'erreur correspondante sera directement affichée
	*/
	exec = recupere_config("nbr_fichiers_config", &nbr_res);
	if (exec)
	{
		affiche_exec(exec);
	}
	
	
	system("clear"); // Efface l'écran précédent
	
	printf("______________________________________________________________\n\n");
	printf(" 		    ------------------  \n");
	printf("		   | Mode utilisateur | \n");
	printf(" 		    ------------------  \n");
	printf("\n");
	printf(" --------------------------------\n");
	printf("| Recherche de fichier texte : 1 |\n");
	printf(" --------------------------------\n");
	printf("\n");
	printf(" --------------------------------\n");
	printf("| Recherche de fichier image : 2 |\n");
	printf(" --------------------------------\n");
	printf("\n");
	printf(" --------------------------------\n");
	printf("| Recherche de fichier audio : 3 |\n");
	printf(" --------------------------------\n");
	printf("\n");
	printf("Les recherches afficheront %d résultats au maximum\n", nbr_res);
	printf("\n");
	printf("Exit : 0                                   Menu précédent : 4\n");
	printf("______________________________________________________________\n\n");	
	
}




void affiche_recherche_texte()
{
	system("clear"); // Efface l'écran précédent
	
	printf("______________________________________________________________\n\n");
	printf(" 		 --------------------------- \n");
	printf("		| Recherche de fichier texte |\n");
	printf(" 		 --------------------------- \n");
	printf("\n");
	printf(" ---------------------------- \n");
	printf("| Comparaison par mot clé : 1 |\n");
	printf(" ---------------------------- \n");
	printf("\n");
	printf(" ---------------------------------- \n");
	printf("| Comparaison par fichier texte : 2 |\n");
	printf(" ---------------------------------- \n");
	printf("\n");
	printf("Exit : 0                                   Menu précédent : 3\n");
	printf("______________________________________________________________\n\n");

}



void affiche_recherche_image()
{
	system("clear"); // Efface l'écran précédent
	
	printf("______________________________________________________________\n\n");
	printf(" 		 --------------------------- \n");
	printf("		| Recherche de fichier image |\n");
	printf(" 		 --------------------------- \n");
	printf("\n");
	printf(" ----------------------------------------- \n");
	printf("| Comparaison par dominance de couleur : 1 |\n");
	printf(" ----------------------------------------- \n");
	printf("\n");
	printf(" ------------------------------------- \n");
	printf("| Comparaison par niveau de gris : 2  |\n");
	printf(" ------------------------------------- \n");
	printf("\n");
	printf(" --------------------------------- \n");
	printf("| Comparaison par fichier image: 3 |\n");
	printf(" --------------------------------- \n");
	printf("\n");
	printf("Exit : 0                                   Menu précédent : 4\n");
	printf("______________________________________________________________\n\n");
}


void affiche_recherche_audio()
{
	system("clear"); // Efface l'écran précédent
	
	printf("______________________________________________________________\n\n");
	printf(" 		 --------------------------- \n");
	printf("		| Recherche de fichier audio |\n");
	printf(" 		 --------------------------- \n");
	printf("\n");
	printf(" ---------------------------- \n");
	printf("| Comparaison par fichier : 1 |\n");
	printf(" ---------------------------- \n");
	printf("\n");
	printf("Exit : 0                                   Menu précédent : 2\n");
	printf("______________________________________________________________\n\n");

}


void affiche_genere_descripteurs()
{
	system("clear"); // Efface l'écran précédent
	
	printf("______________________________________________________________\n\n");
	printf(" 		 ---------------------------- \n");
	printf("		| Génération des descripteurs |\n");
	printf(" 		 ---------------------------- \n");
	printf("\n");
	printf(" ----------------------------------- \n");
	printf("| Générer tous les descripteurs : 1 |\n");
	printf(" ----------------------------------- \n");
	printf("\n");
	printf(" ------------------------------------ \n");
	printf("| Générer les descripteurs texte : 2 |\n");
	printf(" ------------------------------------ \n");
	printf("\n");
	printf(" ------------------------------------ \n");
	printf("| Générer les descripteurs image : 3 |\n");
	printf(" ------------------------------------ \n");
	printf("\n");
	printf(" ------------------------------------ \n");
	printf("| Générer les descripteurs audio : 4 |\n");
	printf(" ------------------------------------ \n");
	printf("\n");
	printf("Exit : 0                                   Menu précédent : 5\n");
	printf("______________________________________________________________\n\n");

}


void affiche_consulte_descripteurs()
{
	system("clear"); // Efface l'écran précédent
	
	printf("______________________________________________________________\n\n");
	printf(" 		 ------------------------------- \n");
	printf("		| Consultation des descripteurs |\n");
	printf(" 		 ------------------------------- \n");
	printf("\n");
	printf(" -------------------------------------- \n");
	printf("| Consulter les descripteurs texte : 1 |\n");
	printf(" -------------------------------------- \n");
	printf("\n");
	printf(" -------------------------------------- \n");
	printf("| Consulter les descripteurs image : 2 |\n");
	printf(" -------------------------------------- \n");
	printf("\n");
	printf(" -------------------------------------- \n");
	printf("| Consulter les descripteurs audio : 3 |\n");
	printf(" -------------------------------------- \n");
	printf("\n");
	printf("Exit : 0                                   Menu précédent : 4\n");
	printf("______________________________________________________________\n\n");

}