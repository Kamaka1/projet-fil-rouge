/****************************************************************************
* fonctions.h : Contient le prototype des différentes fonctions principales
* Développeur : RABINIAUX Bastien
*****************************************************************************/

#ifndef DEF_FONCTIONS
#define DEF_FONCTIONS
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "interface/menu.h"

/*
Fonction pour choisir ce que l'on veux dans les menus (naviguation)
msg[] correspond au message qui sera affiché

Renvoie le choix désiré, 63 si il y a erreur
*/
int choix_mode(char msg[]);

/*
Fonction qui nous indique que l'on peux essayer 3 fois encore
erreur sert à traiter le nombre d'erreurs faites
num_menu est le numéro du menu actuel

Retourne le numéro du menu précédent si l'utilsateur se trompe 4 fois, le numéro actuel sinon
*/
int affiche_erreur(int *erreur, int num_menu);

/*
Fonction qui remonte au menu précédent 
num_menu correspond au numéro du menu actuel
*/
int remonte_menu(int num_menu);

/* 
Fonction de saisie du mot de passe pour passer en mode administrateur

Renvoie 0 si tout s'est bien passé, 61 sinon
*/
int saisie_mdp();

/*
Fonction qui affiche le résultat des requêtes de l'utilisateur
exec est le résultat des requêtes utilisateurs, il doit valoir 0 quand tout se passe bien
type est le type de fichier (0,1,2;texte,image,audio)
message correspond au message qui sera affiché, différent selon les recherches

Si exec vaut 0, le traitement se fera, sinon l'erreur correspondante à sa valeur sera affichée
*/
void affichage_resultat(int exec, int type, char* message);

/* Fonction qui attribue à une couleur les composantes RGB correspondantes
couleur est le numéro que donne l'utilisateur pour faire sa recherche (0,1,2;rouge,vert,bleu)
pixR, pixG et pixB correspondent au triplet RGB dont la valeur changera selon la couleur choisie

Modifie le triplet si la couleur est valide, sinon renvoie 66
*/
int liste_couleur(int couleur, int *pixR, int *pixG, int *pixB);

/* Fonction qui permet d'enlever le /n suite au fgets
chaine est la chaine dans laquelle on veut stocker notre saisie
longueur correspond à sa longueur (taille du tableau)

Retourne 0 si tout va bien, 65 si fgets à posé problème
*/
int lire(char *chaine, int longueur);

// Fonction qui attends la touche entrée pour continuer
int demande_actualisation();

/* Fonction qui affiche les erreurs liées à la variable exec
Si l'erreur n'est pas identifé, cela sera marqué
*/
void affiche_exec(int exec);

#endif
