/****************************************************************************
* fcn_comp_globale.h : Déclaration de toutes les fonctions des comparaisons
* ainsi que de toutes les variables globales utilisées correspondantes.
* Développeur : Matéo Gatel
*****************************************************************************/

#ifndef FCN_COMP
#define FCN_COMP

#include <string.h>
#include <stdlib.h>
#include <stdio.h>


//Comparaison globale
#define CHEMIN_FIC_CONFIG "./src/c/moteurKamaka/admin.config" //Chemin vers le fichier de config
#define NOM_FIC_RES "./src/c/moteurKamaka/resultat/resultatReq" //Chemin vers le fichier de résultat
#define TAILLE_UN_DESC 10000 //Taille d'un descripteur
#define TAILLE_RESULTAT 100 //Taille d'un résultat
/**
* Rempli le fichier de résultat avec les paramètres
* Paramètres :
* int * idDesc IN/OUT : tableau de résultat qui contient la liste des id des fichiers
* int * occDesc IN/OUT : tableau de résultat qui contient l'occurence / différence (la valeur qui nous interesse d'afficher)
* int * nbRes IN/OUT : nombre de résultat dans ces deux tableaux (identiques)
* Valeurs de retour :
* 0 -> La fonction n'a rencontré aucun problème.
* 64 -> Erreur recup_config
*/
int remplissage_Fichier(int*, int*, int*);
/**
* Tri dans un ordre passé en paramètre deux tableaux en fonction des "occurrences"
* Paramètres :
* int * idDesc IN/OUT : tableau de résultat qui contient la liste des id des fichiers
* int * occDesc IN/OUT : tableau de résultat qui contient la valeur qui nous interesse (associé à l'id)
* int * nbRes IN/OUT : nombre de résultat dans les deux tableaux (taille identique)
* int ordre IN : Tri dans un ordre : 0 = décroissant, 1 = croissant
* Valeur de retour :
* 0 -> La fonction n'a rencontré aucun problème.
*/
int tri_Res(int*, int*, int*, int);
/**
* Tri et rempli le fichier de résultat. (Appelle les deux fonctions précédentes)
* Paramètres :
* int * idDesc IN/OUT : tableau de résultat qui contient la liste des id des fichiers
* int * occDesc IN/OUT : tableau de résultat qui contient la valeur qui nous interesse (associé à l'id)
* int * nbRes IN/OUT : nombre de résultat dans les deux tableaux (taille identique)
* int ordre IN : Tri dans un ordre : 0 = décroissant, 1 = croissant
* Valeurs de retour :
* 0 -> La fonction n'a rencontré aucun problème.
* 64 -> Erreur recup_config
*/
int tri_Et_Remplissage(int *, int*, int*, int);


//Comparaison fichier audio
#define CHEMIN_LIEN_AUDIO "./src/c/moteurKamaka/descripteur/descripteur_audio/liste_base_audio"
#define CHEMIN_DESC_AUDIO "./src/c/moteurKamaka/descripteur/descripteur_audio/base_descripteur_audio"
#define MIN_DIFF 5065
/**
* Fonction a appeler pour comparer un fichier audio aux autres. Va ensuite effectuer le tri / remplissage dans le fichier de résultat
* Paramètre :
* char * nomFic IN : nom du fichier à comparer.
* Valeurs de retour :
* 0 -> La fonction n'a rencontré aucun problème.
* 64 -> Erreur recup_config
* 80 -> Erreur d'ouverture du fichier base_descripteur_audio
* 86 -> Erreur récupération id du fichier avec le nom
* 90 -> Le fichier donné en param est plus long que tous les autres (impossible de le trouver dans un autre fichier)
* 91 -> Erreur, tableau vide (anormal)
*/
int comparaison_fichier_audio(char*);
/**
* Compare le fichier donnée en param aux autres descripteurs audio. Insère les résultats dans deux tableaux
* Paramètres :
* char * nomFicParam IN : nom du fichier à comparer.
* int * resId IN/OUT : tableau des id résultats
* int * resDifference IN : tableau des valeurs qui nous interesse (occurrence, difference) / Pas utilisé actuellement (indice plus interessant)
* int * resIndice IN/OUT : tableau des indices auxquels les extraits apparaissent
* Valeur de retour :
* 0 -> La fonction n'a rencontré aucun problème.
* 64 -> Erreur recup_config
* 80 -> Erreur d'ouverture du fichier base_descripteur_audio
* 86 -> Erreur récupération id du fichier avec le nom
* 90 -> Le fichier donné en param est plus long que tous les autres (impossible de le trouver dans un autre fichier)
* 91 -> Erreur, tableau vide (anormal)
*/
int comp_Doc_Audio(char*, int*, int*, int*, int*);

int get_secondes(char *, float, float*);

//Comparaison fichier image
#define CHEMIN_DESC_IMAGE "./src/c/moteurKamaka/descripteur/descripteur_image/base_descripteur_image"
#define CHEMIN_LIEN_IMAGE "./src/c/moteurKamaka/descripteur/descripteur_image/liste_base_image"
/**
* Fonction a appeler pour comparer un fichier image aux autres. Va ensuite effectuer le tri / remplissage dans le fichier résultat
* Paramètre :
* char * nomFic IN : nom du fichier à comparer.
* Valeurs de retour :
* 0 -> La fonction n'a rencontré aucun problème.
* 64 -> Erreur recup_config
* 80 -> Erreur d'ouverture du fichier base_descripteur_image
* 86 -> Erreur récupération id du fichier avec le nom
*/
int comparaison_fichier_image(char*);
/**
* Compare le fichier donnée en param aux autres descripteurs image. Insère les résultats dans deux tableaux
* Paramètres :
* char * nomFicParam IN : nom du fichier à comparer.
* int * resIdDesc IN/OUT : tableau des id résultats
* int * resDiffDesc IN/OUT : tableau des différents entre les images
* int * nbRes IN/OUT : nombre de résultats dans les deux tableaux
* Valeurs de retour :
* 0 -> La fonction n'a rencontré aucun problème.
* 64 -> Erreur recup_config
* 80 -> Erreur d'ouverture du fichier base_descripteur_image
* 86 -> Erreur récupération id du fichier avec le nom
*/
int comp_Doc_Image(char*, int*, int*, int*);


//Comparaison fichier texte
#define CHEMIN_DESC_TEXTE "./src/c/moteurKamaka/descripteur/descripteur_texte/base_descripteur_texte"
#define CHEMIN_LIEN_TEXTE "./src/c/moteurKamaka/descripteur/descripteur_texte/liste_base_texte"
/**
* Fonction a appeler pour comparer un fichier texte aux autres. Va ensuite effectuer le tri / remplissage dans le fichier résultat
* Paramètre :
* char * nomFic IN : nom du fichier à comparer.
* Valeurs de retour :
* 0 -> La fonction n'a rencontré aucun problème.
* 64 -> Erreur recup_config
* 80 -> Erreur d'ouverture du fichier base_descripteur_texte
* 86 -> Erreur récupération id du fichier avec le nom
*/
int comparaison_fichier_texte(char*);
/**
* Compare le fichier donnée en param aux autres descripteurs texte. Insère les résultats dans deux tableaux
* Paramètres :
* char * nomFicParam IN : nom du fichier à comparer.
* int * resIdFicc IN/OUT : tableau des id résultats
* int * resNbMotCommun IN/OUT : tableau du nombre de mots qu'on deux fichiers en commun (l'un étant celui passé en paramètre)
* int * nbElem IN/OUT : nombre de résultats dans les deux tableaux
* Valeurs de retour :
* 0 -> La fonction n'a rencontré aucun problème.
* 64 -> Erreur recup_config
* 80 -> Erreur d'ouverture du fichier base_descripteur_audio
* 86 -> Erreur récupération id du fichier avec le nom
*/
int comp_Doc_Texte(char*, int*, int*, int*);

//Comparaison critere
/**
* Fonction a appeler pour comparer un critère (mot clé, dominance de couleur) aux descripteurs qui correspondent (texte ou image).. Va ensuite effectuer le tri / remplissage dans le fichier résultat
* Paramètres :
* int type IN : type de comparaison / 0 = texte / 1 = image RGB / 2 = image N&B
* char * aTrouver IN : mot clé ou dominance de couleur à trouver.
* Valeurs de retour :
* 0 -> La fonction n'a rencontré aucun problème.
* 64 -> Erreur recup_config
* 80 -> Erreur d'ouverture du fichier base_descripteur_texte
* 81 -> Erreur de type (paramètre <0 ou >2)
*/
int comparaison_critere(int, char*);
/**
* Compare le critère donné (mot clé, dominance de couleur) aux descripteurs qui correspondent. Insère les résultats dans les tableaux correspondant
* Paramètres :
* int type IN : type de comparaison / 0 = texte / 1 = image RGB / 2 = image N&B
* char * aTrouver IN : mot clé ou dominance de couleur à trouver.
* int * ids IN/OUT : taleau des id résultats
* int * occs IN/OUT : tableau avec les valeurs qui nous interessent (occurrence ou différence)
* int * nbRes IN/OUT : nombre de résultat dans les tableaux
* Valeurs de retour :
* 0 -> La fonction n'a rencontré aucun problème.
* 64 -> Erreur recup_config
* 80 -> Erreur d'ouverture du fichier base_descripteur_texte
* 81 -> Erreur de type (paramètre <0 ou >2)
*/
int comp_Cri_Globale(int, char*, int*, int*, int*);


#endif
