/****************************************************************************
* Comp_Doc_Image.c : Fonctions permettant la comparaison par fichier image.
* Développeur : Matéo Gatel
*****************************************************************************/

#include "../../fcn_comp_globale.h"
#include "../../../indexation/indexation_utils.h"
#include "../../../fonctions.h"

int comp_Doc_Image(char *nomFicParam, int *resIdDesc, int *resRessemblanceDesc, int *nbRes)
{
	//Initialisation des variables
	int idFic, idDesc, composante, nbToken;
	FILE *ficDesc = NULL, *ficDesc2 = NULL;
	char descImg[TAILLE_UN_DESC], autreDesc[TAILLE_UN_DESC];
	char *stop, *suite, *suite2, *suite3;
	int valPixDesc[TAILLE_RESULTAT], occPixDesc[TAILLE_RESULTAT], nbPixDesc = 0;
	int idAutreDesc, composanteAutreDesc, nbTokenAutreDesc;
	int valAutreDesc[TAILLE_RESULTAT], occAutreDesc[TAILLE_RESULTAT], nbElemAutreDesc = 0;
	int nbCouleurs_config, nbCouleurs, totRessemblanceDesc = 0, diffPix, exec = 0;

	//Récupération du paramètre dans le fichier admin.config
	exec = recupere_config("nbr_bits_quantification", &nbCouleurs_config);
	if (exec != 0)
	{
		return exec;
	}

	//Récupération du l'id du fichier grâce à son nom
	int *existe = malloc(sizeof(int));
	char *str_id = malloc(sizeof(char[TAILLE_RESULTAT]));
	exec = get_id(11, existe, str_id, nomFicParam);
	if (exec)
	{
		return exec;
	}

	if (!*existe)
	{
		return 86;
	}
	idFic = atoi(str_id);

	//Ouverture du fichier qui contient les descripteurs image
	ficDesc = fopen(CHEMIN_DESC_IMAGE, "r");
	if (ficDesc == NULL)
	{
		return 80; // Erreur ouverture fichier
	}
	while (fgets(descImg, TAILLE_UN_DESC, ficDesc) != NULL)
	{
		//Récupération de l'id du descripteur
		stop = strchr(descImg, '|');
		suite = stop + 1;
		*stop = '\0';
		idDesc = atoi(descImg);
		//S'il est le même que celui passé en paramètre
		if (idDesc == idFic)
		{
			//On récupère la composante (rgb ou n&b) et le nombre de tokens
			stop = strchr(suite, '|');
			suite2 = stop + 1;
			*stop = '\0';
			composante = atoi(suite);

			stop = strchr(suite2, '|');
			suite3 = stop + 1;
			*stop = '\0';
			nbToken = atoi(suite2);

			//On récupère les tokens et leurs occurrences dans deux tableaux différents
			for (int i = 0; i < nbToken; i++)
			{
				char *la_suite = malloc(sizeof(char[TAILLE_UN_DESC]));
				char *suite4;

				if (i == 0)
				{
					strncpy(la_suite, suite3, strlen(suite3));
				}
				else
				{
					strncpy(la_suite, suite4, strlen(suite4));
				}
				stop = strchr(la_suite, ':');
				char *suite5 = stop + 1;
				*stop = '\0';
				valPixDesc[nbPixDesc] = atoi(la_suite);
				if (i < nbToken - 1)
				{
					stop = strchr(suite5, '|');
					char *suite6 = stop + 1;
					*stop = '\0';
					occPixDesc[nbPixDesc] = atoi(suite5);
					suite4 = suite6;
				}
				else
				{
					occPixDesc[nbPixDesc] = atoi(suite5);
				}

				nbPixDesc++;
			}
		}
	}
	fclose(ficDesc);

	//On réouvre le fichier
	ficDesc2 = fopen(CHEMIN_DESC_IMAGE, "r");
	if (ficDesc2 == NULL)
	{
		return 80; //Erreur ouverture fichier
	}
	while (fgets(autreDesc, TAILLE_UN_DESC, ficDesc2) != NULL)
	{
		stop = strchr(autreDesc, '|');
		suite = stop + 1;
		*stop = '\0';
		idAutreDesc = atoi(autreDesc);
		//Pour, cette fois-ci, parcourir tous les descripteurs sauf celui passé en paramètre
		if (idAutreDesc != idFic)
		{
			stop = strchr(suite, '|');
			suite2 = stop + 1;
			*stop = '\0';
			composanteAutreDesc = atoi(suite);

			//Si les images sont du même type (RGB ou N&B)
			if (composante == composanteAutreDesc)
			{

				stop = strchr(suite2, '|');
				suite3 = stop + 1;
				*stop = '\0';
				nbTokenAutreDesc = atoi(suite2);
				nbElemAutreDesc = 0;
				//On récupère les tokens et leurs occurrences dans deux tableaux ici aussi
				for (int i = 0; i < nbTokenAutreDesc; i++)
				{
					char *la_suite = malloc(sizeof(char[TAILLE_UN_DESC]));
					char *suite4;

					if (i == 0)
					{
						strncpy(la_suite, suite3, strlen(suite3));
					}
					else
					{
						strncpy(la_suite, suite4, strlen(suite4));
					}
					stop = strchr(la_suite, ':');
					char *suite5 = stop + 1;
					*stop = '\0';
					valAutreDesc[nbElemAutreDesc] = atoi(la_suite);
					if (i < nbTokenAutreDesc - 1)
					{
						stop = strchr(suite5, '|');
						char *suite6 = stop + 1;
						*stop = '\0';
						occAutreDesc[nbElemAutreDesc] = atoi(suite5);
						suite4 = suite6;
					}
					else
					{
						occAutreDesc[nbElemAutreDesc] = atoi(suite5);
					}
					nbElemAutreDesc++;
				}
				//Calcul du nombre de couleurs (varie en fonction de N)
				nbCouleurs = 1;
				for (int i = 0; i < composante * nbCouleurs_config; i++)
				{
					nbCouleurs *= 2;
				}
				totRessemblanceDesc = 0;
				//Calcul de la ressemblance entre deux images
				for (int j = 0; j < nbCouleurs; j++)
				{
					if (occPixDesc[j] < occAutreDesc[j])
					{
						totRessemblanceDesc += occPixDesc[j];
					}
					else
					{
						totRessemblanceDesc += occAutreDesc[j];
					}
				}

				//On ajoute le résultat dans nos tableaux
				resIdDesc[*nbRes] = idAutreDesc;
				resRessemblanceDesc[*nbRes] = totRessemblanceDesc;
				(*nbRes)++;
			}
		}
	}
	fclose(ficDesc2);
	return 0;
}

int comparaison_fichier_image(char *nomFic)
{
	int resIdDesc[TAILLE_RESULTAT];
	int resRessemblanceDesc[TAILLE_RESULTAT];
	int nbRes = 0;
	int exec = 0;
	//Exécution de la comparaison qui va ajouter les résultats dans les tableaux
	exec = comp_Doc_Image(nomFic, resIdDesc, resRessemblanceDesc, &nbRes);
	if (exec != 0)
	{
		return exec;
	}
	//Exécution du tri et remplissage dans le fichier de résultat
	exec = tri_Et_Remplissage(resIdDesc, resRessemblanceDesc, &nbRes, 0);
	return exec;
}
