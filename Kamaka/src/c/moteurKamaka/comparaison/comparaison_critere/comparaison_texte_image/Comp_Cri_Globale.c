/****************************************************************************
* Comp_Cri_Globale.c : Fonctions permettant la comparaison par critère
* image (RBG + N&B) ainsi que texte (par mot clé).
* Développeur : Matéo Gatel
*****************************************************************************/

#include "../../fcn_comp_globale.h"

int comp_Cri_Globale(int type, char *aTrouver, int *ids, int *occs, int *nbRes)
{
	//Déclaration & initialisation des variables
	char chemin[TAILLE_RESULTAT];
	char desc[TAILLE_UN_DESC]; //String d'un descripteur
	int id;   //ID du fichier
	int composante;	//SI IMAGE 3 = RGB / 1 = N&B
	int seuil; //SI TEXTE seuil récupération dans descripteur
	int nbToken;
	int nbTokenTxt;	//SI TEXTE nombre de token dans le texte entier
	char token[TAILLE_UN_DESC]; //SI IMAGE : valeur couleur / SI TEXTE : mot
	int occurrence;	//Occurence du token
	char *stop, *suite, *suite2, *suite3, *suite4;
	*nbRes = 0;
	int typeImage = 1;

	//Si c'est un texte, chemin vers descripteur texte, sinon image
	if (type == 0)
	{
		strcpy(chemin, CHEMIN_DESC_TEXTE);
	}
	else if (type == 1 || type == 2)
	{
		strcpy(chemin, CHEMIN_DESC_IMAGE);
		if (type == 1)
		{
			typeImage = 3;
		}
	}
	else
	{
		return 81; //Erreur type
	}

	//Ouverture du fichier qui contient les descripteurs
	FILE *descripteurs = NULL;
	descripteurs = fopen(chemin, "r");
	if (descripteurs == NULL)
	{
		return 80; //Erreur ouverture fichier
	}

	//Parcours du fichier
	while (fgets(desc, TAILLE_UN_DESC, descripteurs) != NULL)
	{
		//Pour chaque descripteur
		//On récupère ses premiers paramètres

		//On récupère le pointeur où apparait le premier |
		stop = strchr(desc, '|');
		//Puisqu'on va couper ce qui précède, on garde dans une variable la suite de la chaine
		suite = stop + 1;
		//On coupe la chaine, du coup desc vaut l'id
		*stop = '\0';
		id = atoi(desc);
		//On remet la suite à desc

		stop = strchr(suite, '|');
		suite2 = stop + 1;
		*stop = '\0';
		if (type == 1 || type == 2)
		{
			composante = atoi(suite);
		}
		else
		{
			seuil = atoi(suite);
		}

		stop = strchr(suite2, '|');
		suite3 = stop + 1;
		*stop = '\0';
		nbToken = atoi(suite2);
		if (type == 0)
		{
			stop = strchr(suite3, '|');
			if (stop != NULL)
			{
				suite4 = stop + 1;
				*stop = '\0';
				nbTokenTxt = atoi(desc);
			}
		}
		else
		{
			suite4 = suite3;
		}
		if (stop != NULL)
		{

			//Parcours des tokens
			for (int i = 0; i < nbToken; i++)
			{
				char *la_suite = malloc(sizeof(char[1000]));
				char *suite5;

				if (i == 0)
				{
					strncpy(la_suite, suite4, strlen(suite4));
				}
				else
				{
					strncpy(la_suite, suite5, strlen(suite5));
				}

				stop = strchr(la_suite, ':');
				char *suite6 = stop + 1;
				*stop = '\0';
				strcpy(token, la_suite);

				if (i < nbToken - 1)
				{
					stop = strchr(suite6, '|');
					suite5 = stop + 1;
					*stop = '\0';
					occurrence = atoi(suite6);
				}
				else
				{
					occurrence = atoi(suite6);
				}

				//S'il correspond au parametre a trouver, on l'ajoute dans nos valeurs de résultat
				if (type == 0)
				{
					if (strcmp(token, aTrouver) == 0)
					{
						ids[*nbRes] = id;
						occs[*nbRes] = occurrence;
						(*nbRes)++;
					}
				}
				else
				{
					if (strcmp(token, aTrouver) == 0 && composante == typeImage)
					{
						ids[*nbRes] = id;
						occs[*nbRes] = occurrence;
						(*nbRes)++;
					}
				}
			}
		}
	}

	fclose(descripteurs);
	return 0;
}

int comparaison_critere(int type, char *aTrouver)
{
	int idDesc[TAILLE_RESULTAT];
	int occDesc[TAILLE_RESULTAT];
	int nbRes;
	int exec = 0;
	//Exécution de la comparaison et ajout des résultats dans les tableaux qui correspondent
	exec += comp_Cri_Globale(type, aTrouver, idDesc, occDesc, &nbRes);

	if (exec != 0)
	{
		return exec;
	}

	//Tri et remplissage des résultats dans le fichier qui correspond
	exec += tri_Et_Remplissage(idDesc, occDesc, &nbRes, 0);
	return exec;
}
