/***********************************************************************************
* indexation_texte.c :
* Fonctions permettant le nettoyage, le filtrage et l'indexation des fichiers textes
* Développeur : Thibault DELPY
***********************************************************************************/

#include "indexation_texte.h"

int xml_to_clean(char *nomFichier)
{
	// Concaténation pour obtenir le fichier xml à ouvrir
	char nomFichierXML[TAILLE_MAX] = "./src/c/moteurKamaka/DATA/textes/";
	strcat(nomFichierXML, nomFichier);
	strcat(nomFichierXML, ".xml");

	// Concaténation pour obtenir le fichier clean à écrire
	char nomFichierClean[TAILLE_MAX] = "./src/c/moteurKamaka/DATA/textes/clean/";
	strcat(nomFichierClean, nomFichier);
	strcat(nomFichierClean, ".clean");

	FILE *ficxml = NULL;
	FILE *ficclean = NULL;

	// Ouverture du fichier xml en mode lecture
	ficxml = fopen(nomFichierXML, "r");

	if (ficxml == NULL)
		// Ouverture du fichier XML impossible
		return 21;
	else
	{
		// Ouverture du fichier clean en mode écriture
		ficclean = fopen(nomFichierClean, "w");
		if (ficclean == NULL)
			// Ecriture du fichier clean impossible
			return 22;
		else
		{
			// Début du traitement
			char str[1024] = "";
			char str2[1024] = "";
			const char *separateurs = " ,;:%!€?.()/	­<>-\"";
			const char *apostrophe = "'";
			char *ptr_c = NULL;
			// Tant que la ligne du fichier n'est pas nulle
			while (fgets(str, 1024, ficxml) != NULL)
			{

				// On ignore les lignes des balises inutiles
				if (strstr(str, "<?") == NULL && strstr(str, "<article>") == NULL && strstr(str, "<date>") == NULL && strstr(str, "<auteur>") == NULL && strstr(str, "<texte>") == NULL && strstr(str, "</texte>") == NULL && strstr(str, "</article>") == NULL)
				{

					// On supprime les balises
					ptr_c = strchr(str, '>');
					if (ptr_c != NULL)
						strcpy(str2, ptr_c + 1);
					ptr_c = strchr(str2, '<');
					if (ptr_c != NULL)
						*(ptr_c) = '\0';
					strcpy(str, str2);

					// On supprime la ponctuation
					strcpy(str2, "");
					ptr_c = strtok(str, separateurs);
					while (ptr_c != NULL)
					{
						strcat(str2, ptr_c);
						strcat(str2, " ");
						ptr_c = strtok(NULL, separateurs);
					}
					strcpy(str, str2);

					// On sépare en deux les mots avec apostrophe
					strcpy(str2, "");
					ptr_c = strtok(str, apostrophe);
					while (ptr_c != NULL)
					{
						strcat(str2, ptr_c);
						strcat(str2, "' ");
						ptr_c = strtok(NULL, apostrophe);
					}
					ptr_c = strrchr(str2, '\'');
					if (ptr_c != NULL)
						*(ptr_c) = '\0';
					strcpy(str, str2);

					// On met en minuscule le texte
					for (int i = 0; str[i]; i++)
					{
						str[i] = tolower(str[i]);
					}

					// On écrit la ligne dans le fichier clean
					fputs(str, ficclean);
					//printf("%s", str);
				}
			}
		}
	}

	// Fermeture des fichiers utilisés
	fclose(ficxml);
	fclose(ficclean);

	// Le nettoyage s'est bien déroulé
	return 0;
}

int clean_to_tok(char *nomFichier)
{
	// Concaténation pour obtenir le fichier clean à ouvrir
	char nomFichierClean[TAILLE_MAX] = "./src/c/moteurKamaka/DATA/textes/clean/";
	strcat(nomFichierClean, nomFichier);
	strcat(nomFichierClean, ".clean");

	// Concaténation pour obtenir le fichier tok à écrire
	char nomFichierTok[TAILLE_MAX] = "./src/c/moteurKamaka/DATA/textes/tok/";
	strcat(nomFichierTok, nomFichier);
	strcat(nomFichierTok, ".tok");

	FILE *ficclean = NULL;
	FILE *fictok = NULL;
	FILE *ficstopwords = NULL;

	// Ouverture du fichier clean en mode lecture
	ficclean = fopen(nomFichierClean, "r");

	if (ficclean == NULL)
		// Ouverture du fichier clean impossible
		return 21;

	// Ouverture du fichier stopwords en mode lecture
	ficstopwords = fopen("./src/c/moteurKamaka/DATA/textes/stopwords.txt", "r");

	if (ficstopwords == NULL)
		// Ouverture du fichier stopwords impossible
		return 22;

	// Ouverture du fichier tok en mode écriture
	fictok = fopen(nomFichierTok, "w");
	if (fictok == NULL)
		// Ecriture du fichier tok impossible
		return 23;

	// Début du traitement
	char str[TAILLE_MAX] = "";
	char str2[TAILLE_MAX] = "";
	int stop = 0;

	fscanf(ficclean, "%s", str);
	// Tant qu'il reste des mots non traités dans le fichier clean
	while (!feof(ficclean))
	{
		// On vérifie s'il est présent dans les stopwords
		while (!feof(ficstopwords))
		{
			fscanf(ficstopwords, "%s", str2);
			if (strcmp(str, str2) == 0)
			{
				stop = 1;
				break;
			}
		}
		// S'il n'est pas présent, on l'ajoute au fichier tok
		if (stop == 0)
		{
			fprintf(fictok, "%s ", str);
			//printf("%s ", str); commande de test
		}
		rewind(ficstopwords);
		stop = 0;
		// On récupère le mot suivant
		fscanf(ficclean, "%s", str);
	}

	// Fermeture des fichiers
	fclose(ficclean);
	fclose(ficstopwords);
	fclose(fictok);

	// Le filtrage s'est bien déroulé
	return 0;
}

int indexation_texte(char *nomFichier, char *id)
{
	int existe = 0;
	int seuil, exec = 0;
	exec = recupere_config("seuil_mot_config", &seuil);
	if (exec != 0)
	{
		return exec;
	}
	char descripteur[1024] = "";
	int occurences[2048];
	char str[TAILLE_MAX] = "";
	char *mots[2048];
	int estPresent = 0;
	mots[0] = NULL;
	int i = 0;
	char seuilStr[TAILLE_MAX];
	sprintf(seuilStr, "%d", seuil);
	int nbNombresSignificatifs = 0;
	int nbTokens = 0;
	char nbTokensStr[TAILLE_MAX];
	char nbNombresSignificatifsStr[TAILLE_MAX];
	char *stop;

	// Concaténation pour obtenir le fichier tok à lire
	char nomFichierTok[TAILLE_MAX] = "./src/c/moteurKamaka/DATA/textes/tok/";
	strcat(nomFichierTok, nomFichier);
	strcat(nomFichierTok, ".tok");

	// Concaténation pour avoir le nom du fichier avec extension xml
	char nomFichierXML[TAILLE_MAX] = "";
	strcat(nomFichierXML, nomFichier);
	strcat(nomFichierXML, ".xml");

	// Ouverture du fichier tok en mode lecture
	FILE *fictok = NULL;
	fictok = fopen(nomFichierTok, "r");

	if (fictok == NULL)
		// Lecture du fichier tok impossible
		return 21;

	fscanf(fictok, "%s", str);
	// Tant qu'il reste des mots non traités dans le fichier tok
	while (!feof(fictok))
	{
		nbTokens = nbTokens + 1;
		// On vérifie s'il est présent dans la liste des mots déjà rencontrés
		for (i = 0; mots[i] != NULL; i++)
		{
			//printf("\n%s : %s\n", str, mots[i]);
			if (strcmp(mots[i], str) == 0)
			{
				occurences[i] = occurences[i] + 1;
				estPresent = 1;
				break;
			}
		}
		// S'il n'est pas présent, on l'ajoute à mots
		if (estPresent == 0)
		{
			mots[i + 1] = NULL;
			mots[i] = malloc(sizeof(char[TAILLE_MAX]));
			strcpy(mots[i], str);
			occurences[i] = 1;
		}
		estPresent = 0;
		// On récupère le mot suivant
		fscanf(fictok, "%s", str);
	}

	// Ecriture du descripteur
	strcat(descripteur, id);
	strcat(descripteur, "|");
	strcat(descripteur, seuilStr);
	strcat(descripteur, "|");

	for (i = 0; mots[i] != NULL; i++)
	{
		if (occurences[i] >= seuil)
		{
			nbNombresSignificatifs = nbNombresSignificatifs + 1;
		}
	}

	sprintf(nbNombresSignificatifsStr, "%d", nbNombresSignificatifs);
	strcat(descripteur, nbNombresSignificatifsStr);
	strcat(descripteur, "|");

	sprintf(nbTokensStr, "%d", nbTokens);
	strcat(descripteur, nbTokensStr);
	strcat(descripteur, "|");

	for (i = 0; mots[i] != NULL; i++)
	{
		//printf("\n%d\n", occurences[i]);
		if (occurences[i] >= seuil)
		{
			strcat(descripteur, mots[i]);
			strcat(descripteur, ":");
			sprintf(nbNombresSignificatifsStr, "%d", occurences[i]);
			strcat(descripteur, nbNombresSignificatifsStr);
			strcat(descripteur, "|");
		}
	}
	stop = strrchr(descripteur, '|');
	*stop = '\0';

	//printf("\n\n%s\n\n", descripteur); commande de test

	exec = ecriture_fichiers(0, &existe, id, nomFichierXML, descripteur);
	if (exec)
	{
		return exec;
	}

	// Fermeture des fichiers
	fclose(fictok);

	// L'ajout du descripteur s'est bien déroulé
	return 0;
}

int multi_indexation_texte()
{

	int valRetour;
	char nomFichier[TAILLE_MAX];
	char nbTokensString[TAILLE_MAX];

	// Récupération de la liste des fichiers à indexer
	char **noms = malloc(sizeof(char[TAILLE_MAX_FIC][TAILLE_MAX_FIC]));
	valRetour = get_fichiers(0, noms, "xml");
	if (valRetour)
	{
		for (int i = 0; noms[i] != NULL; i++)
		{
			free(noms[i]);
		}
		free(noms);
		return valRetour;
	}

	for (int i = 0; noms[i] != NULL; i++)
	{
		char *stop = strchr(noms[i], '\n');
		*stop = '\0';

		// On vérifie si le fichier est déjà indexé ou non
		int *existe = malloc(sizeof(int));
		char *str_id = malloc(sizeof(char[100]));
		valRetour = get_id(0, existe, str_id, noms[i]);
		if (valRetour)
		{
			for (int i = 0; noms[i] != NULL; i++)
			{
				free(noms[i]);
			}
			free(noms);
			free(existe);
			free(str_id);
			return valRetour;
		}

		// S'il n'est pas indexé on l'indexe
		if (*existe == 0)
		{
			// On enlève l'extension du nom du fichier
			stop = strrchr(noms[i], '.');
			*stop = '\0';
			// Phase de nettoyage
			valRetour = xml_to_clean(noms[i]);
			if (valRetour)
			{
				for (int i = 0; noms[i] != NULL; i++)
				{
					free(noms[i]);
				}
				free(noms);
				free(existe);
				free(str_id);
				return valRetour;
			}

			// Phase de filtrage
			valRetour = clean_to_tok(noms[i]);
			if (valRetour) {
				for (int i = 0; noms[i] != NULL; i++)
				{
					free(noms[i]);
				}
				free(noms);
				free(existe);
				free(str_id);
				return valRetour;
			}
			// Phase d'indexation
			valRetour = indexation_texte(noms[i], str_id);
			if (valRetour)
			{
				for (int i = 0; noms[i] != NULL; i++)
				{
					free(noms[i]);
				}
				free(noms);
				free(existe);
				free(str_id);
				return valRetour;
			}
		}
	}
	for (int i = 0; noms[i] != NULL; i++)
	{
		free(noms[i]);
	}
	free(noms);

	return 0;
}
