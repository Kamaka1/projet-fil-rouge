/****************************************************************************
* indexation_audio.c : Fonctions permettant l'indexation d'un audio
* ou bien tous les audio d'un coup.
* Développeur : Rémi Laborie
*****************************************************************************/

#include "indexation_audio.h"

#include <stdlib.h>
#include <string.h>

int indexation_audio_all()
{

    int n, m, erreur;
    char ** noms = malloc(sizeof(char[TAILLE_MAX_FIC][TAILLE_MAX_FIC]));
    char * stop;

    /**************************************************************
     *  Récupération descripteur variables dans le fichier config *
     **************************************************************/

/* Code avant la généralisation dans fonctions.c, poura faire l'objet d'une optimisation de la fonction en permettant de récupérer plusieurs valeurs à la fois
    // Création d'un fichier temporaire pour faire le lien entre le fichier de config et le programme
    system("touch tmp");
    FILE *f = fopen("tmp", "r");

    if (f == NULL)
    {
        return 41;
    }

    // Commande . (source) pour récupérer les variables du fichier config (sur unix) et les mettre dans le fichier tmp

    char unixCommande[1000];
    // Test pour savoir si il faut utiliser la commade source ou .
    if (system("source 2> /dev/null"))
    {
        strcpy(unixCommande, ". ");
    }
    else
    {
        strcpy(unixCommande, "source ");
    }
    strcat(unixCommande, "\
                ./admin.config ; \
                echo $nbr_points_config > tmp ; \
                echo $nbr_intervalles_config >> tmp ; \
    ");
    system(unixCommande);

    // Récupération descripteur variables depuis le fichier tmp dans le programme C
    char chaine_n[TAILLE_TAB] = "";
    fgets(chaine_n, TAILLE_TAB, f);

    char chaine_m[TAILLE_TAB] = "";
    fgets(chaine_m, TAILLE_TAB, f);

    // Transformation descripteur variables dans le bon type
    int n = atoi(chaine_n);
    int m = atoi(chaine_m);

    // Fermeture et suppression du fichier tmp
    fclose(f);
    system("rm tmp");
*/
    erreur = recupere_config("nbr_points_config", &n);
    if (erreur) {
        return erreur;
    }
    erreur = recupere_config("nbr_intervalles_config", &m);
    if (erreur) {
        return erreur;
    }

    /**************************************************************
     * Appel de la fonction d'indexation pour chaque fichier .bin *
     **************************************************************/

    // Récupérations de tous les fichiers à indexer
    erreur = get_fichiers(2, noms, "bin");
    if (erreur) {
        return erreur;
    }

    for (int i = 0; noms[i] != NULL; i++)
    {
        // On enlève le retour à la ligne, qui gène dans la comparaison
        stop = strchr(noms[i], '\n');
        *stop = '\0';

        // On indexe le fichier (Si déjà indéxé, rien ne se passera)
        erreur = indexation_audio(noms[i], n, m);
        if (erreur) {
            return erreur;
        }
    }

    // Libération de la mémoire
    for (int i = 0; noms[i] != NULL; i++)
    {
        free(noms[i]);
    }
    free(noms);
    return 0;
}

int indexation_audio(char *path, int n, int m)
{
    // Valeur de retour
    int erreur;

    // Variables pour get_id
    int *existe = malloc(sizeof(int));
    char *str_id = malloc(sizeof(char[TAILLE_TAB]));

    // Seules les valeurs comprises dans cet intervalle seront prises en compte
    int intervalle = INTERVALLE_MAX - INTERVALLE_MIN;

    // Descripteur sans id, n et m
    char descripteur_tmp[DESCRIPTEUR_TAILLE_MAX];

    // Variables pour créer le descripteur_tmp
    long nb_lu; // Nombre de valeurs réellements lu dans le fichier
    int valeurs[m]; // Valeurs pour chaque intervalle
    int k = 0; // Nombre de fenêtres total dans le descripteur
    double tabbin[n]; // Valeurs récupérées dans le fichier
    double nombre; // Permet de stocker un nombre de tabbin dans cette variable pour le traiter
    char str_valeur[TAILLE_TAB]; // Nombre transformé en chaine de caractère
    
    // Variable pour créer le descripteur
    char string_k[1000];
    char string_m[3];
    char descripteur[TAILLE_TAB];

    // Ouverture du fichier à indexer
    FILE *ficbin = NULL;
    ficbin = fopen(path, "rb");

    if (ficbin == NULL)
    {
        // Erreur sur l'ouverture du fichier
        return 42;
    }

    // Récupération des informations du fichier (existe et id)
    erreur = get_id(2, existe, str_id, path);
    if (erreur) {
        return erreur;
    }

    // Si le fichier est déjà indéxé, passer au fichier suivant
    if (*existe)
    {
        free(existe);
        free(str_id);
        return 0;
    }

    

    /**************************************************************
     *               Création du descripteur_tmp                  *
     **************************************************************/

    // Début du descripteur_tmp
    strcpy(descripteur_tmp, "");
    // Création du descripteur_tmp, boucle qui va comparer n nombres à la fois, le nombre de points dans une fenêtre
    do
    {
        // Initialisation des valeurs à 0
        for (int i = 0; i < m; i++)
        {
            valeurs[i] = 0;
        }
        // Récupération des n valeurs suivantes
        nb_lu = fread(tabbin, sizeof(double), n, ficbin);

        for (int i = 0; i < nb_lu; i++)
        {
            // Pour chaque valeur récupérées,
            nombre = tabbin[i];
            // Si la valeur est dans l'intervale MAX,
            if (nombre > INTERVALLE_MIN && nombre < INTERVALLE_MAX)
            {
                // Calculer dans quel petit intervalle on doit la mettre
                // nombre - INTERVALLE_MIN pour avoir le premier interval à 0
                // * m / intervalle pour avoir le bon numéro d'intervalle
                valeurs[(int)((nombre - INTERVALLE_MIN) * m / intervalle)]++;
            }
        }

        // Écriture de la valeur de chaque petit intervalle dans le descripteur
        if (k != 0)
        {
            strcat(descripteur_tmp, "|");
        }
        for (int i = 0; i < m; i++)
        {
            sprintf(str_valeur, "%d", valeurs[i]);
            strcat(descripteur_tmp, str_valeur);
            if (i != m - 1)
                strcat(descripteur_tmp, ":");
        }
        k++;

        // On sort de la boucle quand il n'y a plus de valeurs à lire
    } while (nb_lu == n);

    // Fermeture du fichier
    fclose(ficbin);

    

    /**************************************************************
     *                 Création du descripteur                    *
     **************************************************************/
    
    // Transformation du nombre de fenêtre en chaine de caractères
    sprintf(string_k, "%d", k);

    // Transformation du nombre d'intervales en chaine de caractères
    sprintf(string_m, "%d", m);

    // Début du descripteur
    strcpy(descripteur, str_id);
    strcat(descripteur, "|");
    strcat(descripteur, string_k);
    strcat(descripteur, "|");
    strcat(descripteur, string_m);
    strcat(descripteur, "|");
    strcat(descripteur, descripteur_tmp);

    /**************************************************************
     *                 Écriture dans les fichiers                 *
     **************************************************************/

    erreur = ecriture_fichiers(2, existe, str_id, path, descripteur);
    if (erreur) {
        erreur;
    }

    // Libération de la mémoire
    free(existe);
    free(str_id);
    return 0;
}
