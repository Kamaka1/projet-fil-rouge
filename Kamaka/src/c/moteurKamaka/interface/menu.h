/****************************************************************************
* menu.h : Contient les prototypes des fonctions d'affichage des différents écrans
* Développeur : RABINIAUX Bastien
*****************************************************************************/


#ifndef DEF_MENU
#define DEF_MENU
#include "../fonctions.h"

/*
Fonction pour afficher le menu principal (écran numéro 0)
Le choix de menu va de 0 à 2
*/
void affiche_menu();

/*
Fonction pour afficher le mode administrateur (écran numéro 1)
Le choix de menu va de 0 à 8

Peut afficher une erreur si la récupération des configurations se passe mal
*/
void affiche_admin();

/*
Fonction pour afficher le mode utilisateur (écran numéro 2)
Le choix de menu va de 0 à 4

Peut afficher une erreur si la récupération d'une configuration se passe mal
*/
void affiche_user();

/*
Fonction qui affiche le menu de la recherche de fichier texte (écran numéro 3)
Le choix de menu va de 0 à 3
*/
void affiche_recherche_texte();

/*
Fonction qui affiche le menu de la recherche de fichier image (écran numéro 4)
Le choix de menu va de 0 à 4
*/
void affiche_recherche_image();

/*
Fonction qui affiche le menu de la recherche de fichier audio (écran numéro 5)
Le choix de menu va de 0 à 2
*/
void affiche_recherche_audio();

/*
Fonction qui affiche le menu pour générer les descripteurs (écran numéro 6)
Le choix de menu va de 0 à 5
*/
void affiche_genere_descripteurs();

/*
Fonction qui affiche le menu pour consulter les descripteurs (écran numéro 7)
Le choix de menu va de 0 à 4
*/
void affiche_consulte_descripteurs();



#endif

