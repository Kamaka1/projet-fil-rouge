/****************************************************************************
* tst_menu.c : Contient le codage de l'application générale
* Développeur : RABINIAUX Bastien
*****************************************************************************/


#include "menu.h"
#include "../indexation/indexation_audio/indexation_audio.h"
#include "../indexation/indexation_texte/indexation_texte.h"
#include "../indexation/indexation_image/indexation_image.h"
#include "../comparaison/fcn_comp_globale.h"
#include "../indexation/indexation_utils.h"

int main()
{
	int exec;

	printf("Lancement de l'application, veuillez patienter...\n");

	// Les indexations se font, un message d'erreur est affiché si il se passe un problème

	exec = indexation_audio_all();
	if (exec)
	{
		affiche_exec(exec);
	}
	exec = multi_indexation_image();
	if (exec)
	{
		affiche_exec(exec);
	}
	exec = multi_indexation_texte();
	if (exec)
	{
		affiche_exec(exec);
	}

	int choix = -1, num_menu = 0, erreur = 0, retour = -1, type = -1;
	char c;

	// La boucle se fait tant que l'utilisateur ne décide pas de quitter l'application
	while (choix != 0)

	{
		//Si le numéro de menu est celui du menu principal
		if (num_menu == 0)
		{
			affiche_menu();
			erreur = 0;
			while (num_menu == 0)
			{
				choix = choix_mode("le mode désiré");
				switch (choix)
				{
				case 0: // Quitter l'application
					exit(0);
					break;
				case 1: // Aller en mode admin
					retour = saisie_mdp();
					if (retour == 0)
					{
						num_menu = 1;
					}
					else
					{
						exit(0);
					}
					break;
				case 2: // Aller en mode utilisateur
					num_menu = 2;
					break;
				default: // Affiche l'erreur (remonte au menu précédent si 3 fois)
					num_menu = affiche_erreur(&erreur, num_menu);
					break;
				}
			}
		}

		//Si le numéro de menu est celui du menu administrateur
		if (num_menu == 1)
		{
			affiche_admin();
			erreur = 0;
			while (num_menu == 1)
			{
				choix = choix_mode("le paramètre désiré");
				switch (choix)
				{
				case 0: // Quitter l'application
					exit(0);
				/*
				case 1:
					retour = modif_config("seuil_config", 0, 100, "le seuil de similarité");
					break;
				*/
				case 1: // Seuil d'occurence d'un mot
					retour = modif_config("seuil_mot_config", 2, 15, "le nombre d'occurences que doit avoir un mot");
					if (retour == 0)
					{
						system("> ./src/c/moteurKamaka/descripteur/descripteur_texte/base_descripteur_texte");
						system("> ./src/c/moteurKamaka/descripteur/descripteur_texte/liste_base_texte");
						printf("Indexation en cours, veuillez patienter...\n");
						exec = multi_indexation_texte();
						if (exec)
						{
							affiche_exec(exec);
						}
					}
					break;
				case 2: // Nombre de fichiers affichés
					retour = modif_config("nbr_fichiers_config", 1, 30, "le nombre de fichier affichés");
					break;
				case 3: // Nombre d'intervalles dans l'histogramme
					retour = modif_config("nbr_intervalles_config", 30, 50, "le nombre d'intervalles");
					if (retour == 0)
					{
						system("> ./src/c/moteurKamaka/descripteur/descripteur_audio/base_descripteur_audio");
						system("> ./src/c/moteurKamaka/descripteur/descripteur_audio/liste_base_audio");
						printf("Indexation en cours, veuillez patienter...\n");
						exec = indexation_audio_all();
						if (exec)
						{
							affiche_exec(exec);
						}
					}
					break;
				case 4: // Nombre de points dans une fenêtre
					retour = modif_config("nbr_points_config", 2048, 8192, "le nombre de points");
					if (retour == 0)
					{
						system("> ./src/c/moteurKamaka/descripteur/descripteur_audio/base_descripteur_audio");
						system("> ./src/c/moteurKamaka/descripteur/descripteur_audio/liste_base_audio");
						printf("Indexation en cours, veuillez patienter...\n");
						exec = indexation_audio_all();
						if (exec)
						{
							affiche_exec(exec);
						}
					}
					break;
				case 5: // Nombre de bits de quantification
					retour = modif_config("nbr_bits_quantification", 2, 3, "le nombre de bits de quantification");
					if (retour == 0)
					{
						system("> ./src/c/moteurKamaka/descripteur/descripteur_image/base_descripteur_image");
						system("> ./src/c/moteurKamaka/descripteur/descripteur_image/liste_base_image");
						printf("Indexation en cours, veuillez patienter...\n");
						exec = multi_indexation_image();
						if (exec)
						{
							affiche_exec(exec);
						}
					}
					break;
				case 6: // Vers menu pour générer les descripteurs
					num_menu = 6;
					break;
				case 7: // Vers menu pour consulter les descripteurs
					num_menu = 7;
					break;
				case 8: // Remonte au menu précédent
					num_menu = remonte_menu(num_menu);
					break;
				default: // Affiche l'erreur (remonte au menu précédent si 3 fois)
					num_menu = affiche_erreur(&erreur, num_menu);
					break;
				}
				if (choix == 1 || choix == 2 || choix == 3 || choix == 4 || choix == 5)
				{
					if (retour == 0) // Permet d'actualiser l'affichage après un changement de configuration
					{
						erreur = 0;
						affiche_admin();
					}
					else
					{
						num_menu = remonte_menu(num_menu); // Permet de conserver la remontée de menu après mauvaise saisie
					}
				}
			}
		}

		//Si le numéro de menu est celui du menu utilisateur
		if (num_menu == 2)
		{
			affiche_user();
			erreur = 0;
			while (num_menu == 2)
			{
				choix = choix_mode("le type de fichier à rechercher");
				switch (choix)
				{
				case 0: // Quitter l'application
					exit(0);
				case 1: // Menu de recherche de fichier texte
					num_menu = 3;
					break;
				case 2: // Menu de recherche de fichier image
					num_menu = 4;
					break;
				case 3: // Menu de recherche de fichier audio
					num_menu = 5;
					break;
				case 4: // Remonte au menu précédent
					num_menu = remonte_menu(num_menu);
					break;
				default: // Affiche l'erreur (remonte au menu précédent si 3 fois)
					num_menu = affiche_erreur(&erreur, num_menu);
					break;
				}
			}
		}

		//Si le numéro de menu est celui de la recherche de fichier texte
		if (num_menu == 3)
		{
			type = 0;
			affiche_recherche_texte();
			erreur = 0;
			while (num_menu == 3)
			{
				choix = choix_mode("le type de comparaison");
				switch (choix)
				{
				case 0: // Quitter l'application
					exit(0);
				case 1: // Comparaison par mot clé
					printf("\t\tSaisissez un mot clé : ");
					char *aTrouver = malloc(sizeof(char[100]));
					scanf("%s", aTrouver);
					while ((c = getchar()) != '\n' && c != EOF) // On vide le buffer
					{
					}
					exec = comparaison_critere(type, aTrouver);
					affichage_resultat(exec, type, "occurrence");
					free(aTrouver);
					// Partie pour actualiser l'affichage (efface les résultats)
					demande_actualisation();
					erreur = 0;
					affiche_recherche_texte();
					break;
				case 2: // Comparaison par fichier
					printf("Corpus de fichiers textes :\n");
					system("cat ./src/c/moteurKamaka/descripteur/descripteur_texte/liste_base_texte");
					printf("Veuillez saisir un id : ");
					int id;
					scanf("%d", &id);
					while ((c = getchar()) != '\n' && c != EOF) // On vide le buffer
					{
					}
					int *existe = malloc(sizeof(int));
					char *path = malloc(sizeof(char[100]));
					char str_id[TAILLE_TAB];
					sprintf(str_id, "%d", id);
					exec = get_nom(type, existe, str_id, path); // On récupère le chemin du fichier avec l'id correspondant
					if (exec)
					{
						affiche_exec(exec);
					}
					exec = comparaison_fichier_texte(path);
					affichage_resultat(exec, type, "mots-clés en commun");
					free(existe);
					free(path);
					// Partie pour actualiser l'affichage (efface les résultats)
					demande_actualisation();
					erreur = 0;
					affiche_recherche_texte();
					break;
				case 3: // Remonte au menu précédent
					num_menu = remonte_menu(num_menu);
					break;
				default: // Affiche l'erreur (remonte au menu précédent si 3 fois)
					num_menu = affiche_erreur(&erreur, num_menu);
					break;
				}
			}
		}

		//Si le numéro de menu est celui de la recherche de fichier image
		if (num_menu == 4)
		{
			type = 1;
			affiche_recherche_image();
			erreur = 0;
			while (num_menu == 4)
			{
				choix = choix_mode("le type de comparaison");
				switch (choix)
				{
				case 0: // Quitter l'application
					exit(0);
				case 1: // Comparaison par couleur donnée
					printf("Liste des couleurs disponibles : Rouge, vert, bleu\n");
					printf("Saisissez une couleur (respectivement 0, 1, 2) : ");
					int couleur;
					int pixR, pixG, pixB;
					int nbr_bits, res_traitement;
					char str_traitement[TAILLE_TAB];
					scanf("%d", &couleur);
					while ((c = getchar()) != '\n' && c != EOF) // On vide le buffer
					{
					}
					retour = liste_couleur(couleur, &pixR, &pixG, &pixB);
					if (retour != 66)
					{
						exec = recupere_config("nbr_bits_quantification", &nbr_bits);
						if (exec != 0)
						{
							affiche_exec(exec);
						}
						res_traitement = traitement_couleur(pixR, pixG, pixB, nbr_bits);
						sprintf(str_traitement, "%d", res_traitement);
						exec = comparaison_critere(1, str_traitement);
						affichage_resultat(exec, type, "occurrence");
					}
					else
					{
						printf("Mauvais chiffre !\n");
					}
					// Partie pour actualiser l'affichage (efface les résultats)
					demande_actualisation();
					erreur = 0;
					affiche_recherche_image();
					break;
				case 2: // Comparaison par "couleur" (noir/blanc) donnée
					printf("Liste des niveaux de gris disponibles : Noir, gris foncé, gris clair, blanc\n");
					printf("Saisissez un niveau (respectivement 0, 1, 2, 3) : ");
					int gris;
					char str_gris[TAILLE_TAB];
					scanf("%d", &gris);
					while ((c = getchar()) != '\n' && c != EOF) // On vide le buffer
					{
					}
					if (gris == 0 || gris == 1 || gris == 2 || gris == 3)
					{
						sprintf(str_gris, "%d", gris);
						printf("Avant comparaison\n");
						exec = comparaison_critere(2, str_gris);
						affichage_resultat(exec, type, "occurrence");
					}
					else
					{
						printf("Mauvais chiffre !\n");
					}
					// Partie pour actualiser l'affichage (efface les résultats)
					demande_actualisation();
					erreur = 0;
					affiche_recherche_image();
					break;
				case 3: // Comparaison par fichier
					printf("Corpus de fichiers images :\n");
					system("cat ./src/c/moteurKamaka/descripteur/descripteur_image/liste_base_image");
					printf("Veuillez saisir un id : ");
					int id;
					scanf("%d", &id);
					while ((c = getchar()) != '\n' && c != EOF) // On vide le buffer
					{
					}
					int *existe = malloc(sizeof(int));
					char *path = malloc(sizeof(char[100]));
					char str_id[TAILLE_TAB];
					sprintf(str_id, "%d", id);
					exec = get_nom(type, existe, str_id, path); // On récupère le chemin du fichier avec l'id correspondant
					if (exec)
					{
						affiche_exec(exec);
					}
					exec = comparaison_fichier_image(path);
					affichage_resultat(exec, type, "ressemblance");
					free(existe);
					free(path);
					// Partie pour actualiser l'affichage (efface les résultats)
					demande_actualisation();
					erreur = 0;
					affiche_recherche_image();
					break;
				case 4: // Remonte au menu précédent
					num_menu = remonte_menu(num_menu);
					break;
				default: // Affiche l'erreur (remonte au menu précédent si 3 fois)
					num_menu = affiche_erreur(&erreur, num_menu);
					break;
				}
			}
		}

		//Si le numéro de menu est celui de la recherche de fichier audio
		if (num_menu == 5)
		{
			affiche_recherche_audio();
			erreur = 0;
			type = 2;
			while (num_menu == 5)
			{
				choix = choix_mode("le type de comparaison");
				switch (choix)
				{
				case 0: // Quitter l'application
					exit(0);
				case 1: // Comparaison par fichier
					printf("Corpus de fichiers audios :\n");
					system("cat ./src/c/moteurKamaka/descripteur/descripteur_audio/liste_base_audio");
					printf("Veuillez saisir un id : ");
					int id;
					scanf("%d", &id);
					while ((c = getchar()) != '\n' && c != EOF) // On vide le buffer
					{
					}
					int *existe = malloc(sizeof(int));
					char *path = malloc(sizeof(char[100]));
					char str_id[TAILLE_TAB];
					sprintf(str_id, "%d", id);
					exec = get_nom(type, existe, str_id, path); // On récupère le chemin du fichier avec l'id correspondant
					if (exec)
					{
						affiche_exec(exec);
					}
					printf("%s\n", path);
					printf("début comparaison\n");
					exec = comparaison_fichier_audio(path);
					printf("fin comparaison\n");
					affichage_resultat(exec, type, "temps en seconde au bout duquel l'extrait est présent");
					free(existe);
					free(path);
					// Partie pour actualiser l'affichage (efface les résultats)
					demande_actualisation();
					erreur = 0;
					affiche_recherche_audio();
					break;
				case 2: // Remonte au menu précédent
					num_menu = remonte_menu(num_menu);
					break;
				default: // Affiche l'erreur (remonte au menu précédent si 3 fois)
					num_menu = affiche_erreur(&erreur, num_menu);
					break;
				}
			}
		}

		//Si le numéro de menu est celui pour générer les descripteurs
		if (num_menu == 6)
		{
			affiche_genere_descripteurs();
			erreur = 0;
			while (num_menu == 6)
			{
				choix = choix_mode("les descripteurs à générer");
				switch (choix)
				{
				case 0: // Quitter l'application
					exit(0);
				case 1: // Génération de tous les descripteurs
					system("> ./src/c/moteurKamaka/descripteur/descripteur_texte/base_descripteur_texte");
					system("> ./src/c/moteurKamaka/descripteur/descripteur_texte/liste_base_texte");
					printf("Indexation texte en cours, veuillez patienter...\n");
					exec = multi_indexation_texte();
					if (exec)
					{
						affiche_exec(exec);
					}
					system("> ./src/c/moteurKamaka/descripteur/descripteur_image/base_descripteur_image");
					system("> ./src/c/moteurKamaka/descripteur/descripteur_image/liste_base_image");
					printf("Indexation image en cours, veuillez patienter...\n");
					exec = multi_indexation_image();
					if (exec)
					{
						affiche_exec(exec);
					}
					system("> ./src/c/moteurKamaka/descripteur/descripteur_audio/base_descripteur_audio");
					system("> ./src/c/moteurKamaka/descripteur/descripteur_audio/liste_base_audio");
					printf("Indexation audio en cours, veuillez patienter...\n");
					exec = indexation_audio_all();
					if (exec)
					{
						affiche_exec(exec);
					}
					break;
				case 2: // Génération des descripteurs texte
					system("> ./src/c/moteurKamaka/descripteur/descripteur_texte/base_descripteur_texte");
					system("> ./src/c/moteurKamaka/descripteur/descripteur_texte/liste_base_texte");
					printf("Indexation texte en cours, veuillez patienter...\n");
					exec = multi_indexation_texte();
					if (exec)
					{
						affiche_exec(exec);
					}
					break;
				case 3: // Génération des descripteurs image
					system("> ./src/c/moteurKamaka/descripteur/descripteur_image/base_descripteur_image");
					system("> ./src/c/moteurKamaka/descripteur/descripteur_image/liste_base_image");
					printf("Indexation image en cours, veuillez patienter...\n");
					exec = multi_indexation_image();
					if (exec)
					{
						affiche_exec(exec);
					}
					break;
				case 4: // Génération des descripteurs audio
					system("> ./src/c/moteurKamaka/descripteur/descripteur_audio/base_descripteur_audio");
					system("> ./src/c/moteurKamaka/descripteur/descripteur_audio/liste_base_audio");
					printf("Indexation audio en cours, veuillez patienter...\n");
					exec = indexation_audio_all();
					if (exec)
					{
						affiche_exec(exec);
					}
					break;
				case 5:
					num_menu = remonte_menu(num_menu);
					break;
				default: // Affiche l'erreur (remonte au menu précédent si 3 fois)
					num_menu = affiche_erreur(&erreur, num_menu);
					break;
				}
			}
		}

		//Si le numéro de menu est celui pour consulter les descripteurs
		if (num_menu == 7)
		{
			affiche_consulte_descripteurs();
			erreur = 0;
			while (num_menu == 7)
			{
				choix = choix_mode("les descripteurs à consulter");
				switch (choix)
				{
				case 0: // Quitter l'application
					exit(0);
				case 1: // Consultation des descripteurs texte
					system("cat ./src/c/moteurKamaka/descripteur/descripteur_texte/base_descripteur_texte");
					break;
				case 2: // Consultation des descripteurs image
					system("cat ./src/c/moteurKamaka/descripteur/descripteur_image/base_descripteur_image");
					break;
				case 3: // Consultation des descripteurs audio
					system("cat ./src/c/moteurKamaka/descripteur/descripteur_audio/base_descripteur_audio");
					break;
				case 4: // Remonte au menu précédent
					num_menu = remonte_menu(num_menu);
					break;
				default: // Affiche l'erreur (remonte au menu précédent si 3 fois)
					num_menu = affiche_erreur(&erreur, num_menu);
					break;
				}
				// Partie pour actualiser l'affichage (efface les résultats)
				if (choix == 1 || choix == 2 || choix == 3)
				{
					demande_actualisation();
					erreur = 0;
					affiche_consulte_descripteurs();
				}
			}
		}
	}

	return 0;
}
