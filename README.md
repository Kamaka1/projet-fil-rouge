# Kamaka

## Compiler et lancer l'application
* Installer le nécessaire
    * make
    * gcc
    * dos2unix
    * java (version 11 ou plus)
    * ffmpeg
* Ouvrir le dossier Kamaka dans un terminal
* Lancer la commande __./gradlew jar__
    * Si la commande ne fonctionne pas, faire __dos2unix gradlew__ avant
* Lancer la commande __java -jar build/libs/Kamaka.jar__
